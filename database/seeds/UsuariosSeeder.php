<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'rol_id' => 1
        ]);
/*

        DB::table('users')->insert([
            'email' => 'empleado@gmail.com',
            'password' => bcrypt('123456'),
            'rol_id' => 2
        ]);

        DB::table('users')->insert([
            'email' => 'entrenador@gmail.com',
            'password' => bcrypt('123456'),
            'rol_id' => 3
        ]);

        DB::table('users')->insert([
            'email' => 'empresa@gmail.com',
            'password' => bcrypt('123456'),
            'rol_id' => 4
        ]);

        DB::table('users')->insert([
            'email' => 'estudiante@gmail.com',
            'password' => bcrypt('123456'),
            'rol_id' => 5
        ]);*/

    }
}
