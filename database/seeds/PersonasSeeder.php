<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('personas')->insert([
            'tipo_documento' => 'CC',
            'documento' => '123456789',
            'nombres' => 'Administrador',
            'apellidos' => 'Sistema',
            'telefono' => '123123',
            'fecha_nacimiento' => '2019-02-04',
            'sexo' => 'Masculino',
            'ocupacion' => 'calle falsa 123',
            'city_id' => 1,
            'user_id' => 1,
        ]);

/*        DB::table('personas')->insert([
            'tipo_documento' => 'CC',
            'documento' => '987654321',
            'nombres' => 'Empleado',
            'apellidos' => 'Sistema',
            'telefono' => '123123',
            'fecha_nacimiento' => '2019-02-04',
            'sexo' => 'Masculino',
            'ocupacion' => 'calle falsa 123',
            'city_id' => 1,
            'user_id' => 2,
        ]);

        DB::table('personas')->insert([
            'tipo_documento' => 'CC',
            'documento' => '1239876541',
            'nombres' => 'Entrenador',
            'apellidos' => 'Sistema',
            'telefono' => '123123',
            'fecha_nacimiento' => '2019-02-04',
            'sexo' => 'Masculino',
            'ocupacion' => 'calle falsa 123',
            'city_id' => 1,
            'user_id' => 3,
        ]);

        DB::table('personas')->insert([
            'tipo_documento' => 'CC',
            'documento' => '123987654',
            'nombres' => 'Estudiante',
            'apellidos' => 'Sistema',
            'telefono' => '123123',
            'fecha_nacimiento' => '2019-02-04',
            'sexo' => 'Masculino',
            'ocupacion' => 'calle falsa 123',
            'city_id' => 1,
            'user_id' => 5,
        ]);*/

    }
}
