<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('rols')->insert([
            'id'=> 1,
            'nombre' => 'Administrador'
        ]);

        DB::table('rols')->insert([
            'id'=> 2,
            'nombre' => 'Empleado'
        ]);

        DB::table('rols')->insert([
            'id'=> 3,
            'nombre' => 'Entrenador'
        ]);

        DB::table('rols')->insert([
            'id'=> 4,
            'nombre' => 'Empresa'
        ]);

        DB::table('rols')->insert([
            'id'=> 5,
            'nombre' => 'Estudiante'
        ]);



    }
}
