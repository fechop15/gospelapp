<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('imagen')->nullable();
            $table->longText('contenido');
            $table->date('fecha');
            $table->boolean('estado')->default(true);
            $table->string('tipo');//predica//evento//otros

            //quien lo Registro
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('noticias');

    }
}
