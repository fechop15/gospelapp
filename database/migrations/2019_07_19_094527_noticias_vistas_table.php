<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoticiasVistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('noticias_vistas', function (Blueprint $table) {

            $table->boolean('like')->nullable();
            $table->integer('noticia_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->primary(['noticia_id', 'user_id']);

            $table->foreign('noticia_id')
                ->references('id')
                ->on('noticias');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('noticias_vistas');

    }
}
