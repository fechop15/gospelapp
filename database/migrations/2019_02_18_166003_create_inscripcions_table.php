<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscripcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcions', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('estado')->default(true);
            $table->boolean('descargable')->default(true);

            $table->boolean('pago')->default(false);
            $table->boolean('documentado')->default(false);

            $table->boolean('nota_teorica')->nullable();
            $table->boolean('nota_practica')->nullable();

            $table->date('fecha_certificacion')->nullable();
            $table->date('fecha_reporte_ministerio')->nullable();
            $table->integer('asistencia_validada')->default(0);

            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')->references('id')->on('cursos');

            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id')->on('personas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcions');
    }
}
