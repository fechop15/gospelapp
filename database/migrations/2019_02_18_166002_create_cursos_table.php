<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('nivel');
            $table->string('supervisor');
            $table->string('ciudad');
            $table->string('dias');

            $table->integer('intensidad');
            $table->integer('cupos');

            $table->date('fecha_inicio_inscripciones');
            $table->date('fecha_fin_inscripciones');
            $table->date('fecha_inicio_curso');
            $table->date('fecha_fin_curso');

            $table->boolean('estado')->default(true);

            $table->integer('entrenador_id')->unsigned();
            $table->foreign('entrenador_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
