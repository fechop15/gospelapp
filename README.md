<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Comandos Laravel

Crear seeder
###### php artisan make:seeder UsersTableSeeder

Crear modelo con migracion
###### php artisan make:model --migration Productos

actualizar base de datos y seeders
###### php artisan migrate:fresh --seed

crear controlador
###### php artisan make:controller BodegasController



Ajax Blanquito

            $.ajax({
                url: '/add-cuenta',
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    mesa_id: idMesa,
                },

            }).done(function (response) {
                console.log(response);

                //return response;
            }).fail(function (error) {

                console.log(error)

            });


swal peticion blanquito

            swal({
                title: 'Agregar Producto',
                html:

                    '<hr>' +
                    '<div style=" margin-top: 10px">' +
                    '<select class="js-example-responsive"  style="width: 100%; margin-top: 10px" name="state">\n' +
                    '  <option value="" selected="" disabled></option>\n' +
                    option +
                    '</select>' +
                    '</div>' +


                    '<div class="form-group" style="margin-top: 20px">' +
                    '<label class="label-on-left" style="float: left">Cantidad</label>' +
                    '<input id="cantidad" type="number" min="1" class="form-control" />' +
                    '</div>' +


                    '<div class="form-group">' +
                    '<label class="label-on-left" style="float: left">Nota adicional</label>' +
                    ' <textarea name="descripcion" id="descripcion" cols="15" rows="3"\n' +
                    '                                      class="form-control"></textarea>' +
                    '</div>'


                ,
                confirmButtonText: 'Registrar',
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        swal.showLoading()
                        if ($('#nombre-usuario').val() === "" || $('#select-rol').val() === null || $('#correo').val() === "" || $('#password').val() === "") {
                            swal.showValidationError(
                                'Complete el Formulario.'
                            )
                            swal.disableLoading()
                        } else {


                            $.ajax({
                                url: '/guardar-usuario',
                                type: 'POST',
                                data: {
                                    _token: CSRF_TOKEN,
                                    name: $('#nombre-usuario').val(),
                                    email: $('#correo').val(),
                                    password: $('#password').val(),
                                    rol_id: $('#select-rol').val()
                                },

                            }).done(function (response) {
                                console.log(response);
                                resolve([
                                    $('#nombre-usuario').val()
                                ])
                                //return response;
                            }).fail(function (error) {

                                    console.log(error)


                                    var obj = error.responseJSON.errors;

                                    Object.entries(obj).forEach(([key, value]) => {


                                        swal.showValidationError(
                                            value[0]
                                        )
                                        ;

                                    });


                                    swal.disableLoading()
                                }
                            );
                        }
                    })
                }

                ,
                onOpen: function () {
                    $('#nombre-usuario').focus()

                    $('.js-example-responsive').select2({
                        placeholder: 'Selecciona un producto',
                        allowClear: true
                    });

                    var number = document.getElementById('cantidad');


                    number.onkeydown = function (e) {
                        if (!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8)) {
                            return false;
                        }
                    }
                }
            }).then(function (result) {
                swal(
                    'Felicidades!',
                    'El usuario ' + result[0] + ' se ha registrado correctamente.',
                    'success'
                ).then((result) => {
                    location.reload();
                })
            }).catch(swal.noop)
