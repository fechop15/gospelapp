<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-3">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title" id="exampleModalCenterTitle">Registro Preinscripcion Eliminar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Realmente desea eliminar esta preinscripcion?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="eliminarPrematricula()">Eliminar</button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>
@endsection