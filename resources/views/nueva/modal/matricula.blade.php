<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Inscripcion a curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="guardarUsuario">

                    <div class="row">
                        <div class="col-md-12">
                            <label>Estudiante</label>
                            <select class="col-sm-12" id="select-persona" style="width: 100%">
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label>Curso</label>
                            <select class="col-sm-12" id="select-curso" style="width: 100%">
                            </select>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group fill">
                                <label>Entrego Documentacion</label>
                                <select class="form-control" id="documentado">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group fill">
                                <label>Pago Inscripcion?</label>
                                <select class="form-control" id="pago">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="error" style="display: none">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary btn-guardar m-2" type="button">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Guardando...</span>
                    <span class="btn-text">Guardar</span>
                </button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-actualizar">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Matricula</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center" id="cargaActualizar">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>

                <form id="actualizarUsuario">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group fill">
                                <label>Nombre Completo</label>
                                <input type="text" id="Anombre" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group fill">
                                <label>Documento</label>
                                <input type="text" id="Adocumento" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group fill">
                                <label>Telefono</label>
                                <input type="text" id="Atelefono" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group fill">
                                <label>Correo</label>
                                <input type="text" id="Acorreo" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group fill">
                                <label>Curso</label>
                                <input type="text" id="Acurso" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Fecha Inicio</label>
                                <input type="date" id="AfechaInicio" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Fecha Fin</label>
                                <input type="date" id="AfechaFin" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Entrego Documentacion</label>
                                <select class="form-control" id="Adocumentado">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Pago Inscripcion?</label>
                                <select class="form-control" id="Apago">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Estado</label>
                                <select class="form-control" id="estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert" id="Aerror" style="display: none">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary btn-actualizar m-2" type="button">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Actualizando...</span>
                    <span class="btn-text">Actualizar</span>
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>
    </script>

@endsection