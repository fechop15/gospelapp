<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-pass">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cambio de contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-password">

                    <div class="form-group">
                        <label class="col-form-label">Contraseña nueva:</label>
                        <input type="text" class="form-control" id="pass">
                    </div>

                    <div class="form-group">
                        <div class="alert alert-danger" id="Perror" style="display: none">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="cambiarPass()">Cambiar</button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>

@endsection