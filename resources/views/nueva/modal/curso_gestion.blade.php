
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-calificacion">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro de notas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="calificacion">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group fill">
                                <label>Nota Teorica (40%)</label>
                                <input type="number" class="form-control" id="nota_teorica"
                                       placeholder="ingrese la nota teorica de 0 a 100" max="100"
                                       autocomplete="off"
                                       onkeyup="calcularNota()">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group fill">
                                <label>Nota Practica (60%)</label>
                                <input type="number" class="form-control" id="nota_practica"
                                       placeholder="ingrese la nota practica de 0 a 100" max="100" autocomplete="off"
                                       onkeyup="calcularNota()">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label>El estudiante debe tener una calificacion promedio superior al 70%
                                para aprobar el curso y generar su certificado</label>
                        </div>

                        <div class="col-md-12">
                            <div class="alert alert-danger" id="Cerror" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <h4 class="modal-title" style="color: gray;">Nota Final:<strong id="notaFinal">0</strong>%</h4>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="calificar()">Calificar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-asistencia">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Confirmacion de asistencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="asistencia">
                    <div class="row">
                        <div class="col-md-12">
                            <label>
                                Al Confirmar esta asistencia esta indicando que el estudiante asistio las veces requeridas para
                                aprobar este curso
                            </label>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group fill">
                                <label>Confirmar asistencia</label>
                                <select class="form-control" id="asistio">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label>
                                OJO. una vez confirmado esta asistencia no se podra cambiar.
                            </label>
                        </div>

                        <div class="col-md-12">
                            <div class="alert alert-danger" id="Aerror" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="confirmarAsistencia()">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-certificado">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Generacion de certificado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="certificado">
                    <div class="row">
                        <label>
                        Realmente Desea Generar el Certificado.. <br>
                        Este solo puede ser descargado por el estudiante si se encuentra a paz y salvo y
                        ha cumplido los requisitos requeridos por la entidad
                        </label>
                        <div class="col-md-12">
                            <div class="alert alert-danger" id="Ceerror" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="certificar()">Generar Certificado</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-reportar">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Confirmacion de Reporte Al Ministerio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="certificado">
                    <div class="row">
                        <label class="form-control-label">
                            Realmente desea reportar este certificado al ministerio.. <br>
                        </label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger" id="Merror" style="display: none">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="reportar()">Reportar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-actualizar">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Matricula</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center" id="cargaActualizar">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>

                <form id="actualizarUsuario">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group fill">
                                <label>Nombre Completo</label>
                                <input type="text" id="Anombre" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group fill">
                                <label>Documento</label>
                                <input type="text" id="Adocumento" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group fill">
                                <label>Telefono</label>
                                <input type="text" id="Atelefono" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group fill">
                                <label>Correo</label>
                                <input type="text" id="Acorreo" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group fill">
                                <label>Curso</label>
                                <input type="text" id="Acurso" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Fecha Inicio</label>
                                <input type="date" id="AfechaInicio" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Fecha Fin</label>
                                <input type="date" id="AfechaFin" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Entrego Documentacion</label>
                                <select class="form-control" id="Adocumentado">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Pago Inscripcion?</label>
                                <select class="form-control" id="Apago">
                                    <option value="0">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group fill">
                                <label>Estado</label>
                                <select class="form-control" id="estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert" id="Aerror" style="display: none">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary btn-actualizar m-2" type="button">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Actualizando...</span>
                    <span class="btn-text">Actualizar</span>
                </button>
            </div>
        </div>
    </div>
</div>
