@extends('nueva.layout.Dashboard')
@section('page')
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Pre Inscripciones</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                                class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Aspirantes a Cursos</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">
                    <div class="card animated fadeIn" id="vistaPreInscripciones">
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-hover" id="tablaPreinscripciones" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Identificacion</th>
                                        <th>Curso</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card animated fadeIn" id="vistaPreInscripcionPersona1" style="display: none">
                        <div class="card-header">
                            <h5>Pre Inscripcion</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarMatricula">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombres</label>
                                            <input type="text" id="nombres" class="form-control" placeholder="Nombres">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Apellidos</label>
                                            <input type="text" id="apellidos" class="form-control"
                                                   placeholder="Apellidos">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Documento</label>
                                            <input type="number" id="documento" class="form-control"
                                                   placeholder="Documento">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ocupacion</label>
                                            <input type="text" id="ocupacion" class="form-control"
                                                   placeholder="Ocupacion">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Telefono</label>
                                            <input type="number" id="telefono" class="form-control"
                                                   placeholder="Telefono">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Nacimiento</label>
                                            <input type="date" id="fecha_nacimiento" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Sexo</label>
                                            <select class="form-control" id="sexo">
                                                <option value="Masculino">Masculino</option>
                                                <option value="Femenino">Femenino</option>
                                                <option value="Otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Correo</label>
                                            <input type="email" id="email" class="form-control" placeholder="Correo">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Contraseña</label>
                                            <input type="text" id="password" class="form-control"
                                                   placeholder="Contraseña">
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Departamento</label>
                                            <select class="col-sm-12" id="select-municipio" name="state" style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ciudad</label>
                                            <select class="col-sm-12" id="select-ciudad" name="city" style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Entrego Documentacion</label>
                                            <select class="form-control" id="documentado">
                                                <option value="0">No</option>
                                                <option value="1">Si</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Pago Inscripcion?</label>
                                            <select class="form-control" id="pago">
                                                <option value="0">No</option>
                                                <option value="1">Si</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Empresa</label>
                                        <select class="col-sm-12" id="select-empresa" name="empresa" style="width: 100%">
                                        </select>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-success" role="success" id="cursoInteresado">

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Curso</label>
                                        <select class="col-sm-12" id="select-curso" style="width: 100%">
                                        </select>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                        </div>
                                    </div>


                                </div>
                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            onclick="guardar()" >Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card animated fadeIn" id="vistaPreInscripcionPersona2" style="display: none">
                        <div class="card-header">
                            <h5>Pre Inscripcion</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarMatriculaR">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Nombres</label>
                                            <input type="text" id="Rnombres" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Apellidos</label>
                                            <input type="text" id="Rapellidos" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Documento</label>
                                            <input type="number" id="Rdocumento" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Entrego Documentacion</label>
                                            <select class="form-control" id="Rdocumentado">
                                                <option value="0">No</option>
                                                <option value="1">Si</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Pago Inscripcion?</label>
                                            <select class="form-control" id="Rpago">
                                                <option value="0">No</option>
                                                <option value="1">Si</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Empresa</label>
                                        <select class="col-sm-12" id="Rselect-empresa" name="empresa" style="width: 100%">
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Curso</label>
                                        <select class="col-sm-12" id="Rselect-curso" style="width: 100%">
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="alert alert-success" id="RcursoInteresado">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="Rerror" style="display:none">

                                        </div>
                                    </div>
                                </div>
                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            onclick="guardar()" >Guardar
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    <!-- [ Main Content ] end   @ include('nueva.modal.pre_inscripcion') -->
    @include('nueva.modal.pre_inscripcion')

@endsection

@section('js')

    <script>
        var PREMATRICULA = 0;
        var EMPRESA = null;
        var PERSONA = 0;
        var STATES = [];
        var CURSOS = [];
        var EMPRESAS = [{
            id: 0,
            text: 'Ninguna'
        }];
        $('#select-ciudad').select2();
        $('#select-municipio').select2();

        $('#select-curso').select2();
        $('#Rselect-curso').select2();

        $('#select-empresa').select2();
        $('#Rselect-empresa').select2();

        var TABLA = $('#tablaPreinscripciones').DataTable({
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            "ajax": {
                "url": "/get-preinscripcion",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombres: data.msg[item].nombre,
                            Telefono: data.msg[item].telefono,
                            Identificacion: data.msg[item].documento,
                            Correo: data.msg[item].email,
                            Empresa: data.msg[item].nombre_empresa,
                            curso: data.msg[item].curso,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            order: [[ 0, "desc" ]],
            columns: [
                {data: "Id"},
                {data: "Nombres"},
                {data: "Telefono"},
                {data: "Identificacion"},
                {data: "curso"},
                {data: "Opciones"}
            ]
        });

        $('.datatable tbody td').each(function(index){
            $this = $(this);
            var titleVal = $this.text();
            if (typeof titleVal === "string" && titleVal !== '') {
                $this.attr('title', titleVal);
            }
        });

        $.get(
            "/get-empresas",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id_empresa,
                    text: data.msg[item].nombre
                };
                EMPRESAS.push(itemSelect2)
            }

            $('#select-empresa').select2({
                minimumResultsForSearch: 5,
                allowClear: true,
                placeholder: "Bucar Empresa",
                data: EMPRESAS
            });

            $('#Rselect-empresa').select2({
                minimumResultsForSearch: 5,
                allowClear: true,
                placeholder: "Bucar Empresa",
                data: EMPRESAS
            });

            $("#select-empresa").val(0).trigger('change');
            $("#Rselect-empresa").val(0).trigger('change');

        });

        $.get(
            "/getStates",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                STATES.push(itemSelect2)
            }
            $('#select-municipio').select2({
                data: STATES
            });
        });

        $('#select-municipio').on('change', function (e) {
            var cities = [];
            $("#select-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#select-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    allowClear: true,
                    data: cities
                });
            });
        });

        function matriculas() {
            $.post(
                "/get-cursos-matricula", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].matriculados + "/" + data.msg[item].cupos +
                            " - " + data.msg[item].nombre + " " + data.msg[item].nivel+ " - " +
                            data.msg[item].fecha_inicio_curso+ " a "+ data.msg[item].fecha_fin_curso,
                    };
                    CURSOS.push(itemSelect2)
                }
                $('#select-curso').select2({
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Cursos Disponibles",
                    data: CURSOS
                });
                $('#Rselect-curso').select2({
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Cursos Disponibles",
                    data: CURSOS
                });
                $("#select-curso").val("").trigger('change');
                $("#Rselect-curso").val("").trigger('change');
            });
        }

        function eliminar(id) {
            PREMATRICULA = id;
                swal({
                    title: "Estas seguro?",
                    text: "Realmente desea eliminar esta preinscripcion?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.post(
                                "/terminar-preinscripcion", {id: PREMATRICULA, _token: $('meta[name="csrf-token"]').attr('content')}
                            ).done(function (data) {
                                TABLA.ajax.reload();
                                EMPRESA=null;
                                notify('Pre matricula eliminada con exito','success');
                            });
                        } else {

                        }
                    });

            //$('#modal-3').modal();
        }

        function opciones(id) {
            var opciones = '' +
                '<button type="button" class="btn btn-sm btn-primary btn-matricular" ' +
                '           data-toggle="tooltip" data-placement="top" title="Matricular" data-original-title="Edit">' +
                '           <i class="feather icon-edit"></i>' +
                ' </button>' +
                '<button type="button" class="btn btn-danger btn-sm eliminar" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="eliminar(' + id + ')">\n' +
                '           <i class="feather icon-trash-2"></i>\n' +
                ' </button>';
            return opciones;
        }

        TABLA.on('click', '.btn-matricular', function () {
            $('#vistaPreInscripcionPersona1').show();
            $('#vistaPreInscripcionPersona2').hide();
            $('#vistaPreInscripciones').hide();
            cargar(true,'#guardarMatricula');
            cargar(true,'#guardarMatriculaR');

            $('#guardarMatricula')[0].reset();
            $('#guardarMatriculaR')[0].reset();

            matriculas();
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            PREMATRICULA=data.Id;
            console.log(data);
            $.ajax({
                    url: '/buscar-preinscripcion',
                    type: 'POST',
                    data: {
                        id: data.Id,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);
                if (response.msg.persona == null) {
                    $('#nombres').val(response.msg.nombres);
                    $('#apellidos').val(response.msg.apellidos);
                    $('#documento').val(response.msg.documento);

                    $('#ocupacion').val(response.msg.ocupacion);
                    $('#telefono').val(response.msg.telefono);

                    $('#email').val(response.msg.email);
                    $('#cursoInteresado').html("Esta interesado en :" + response.msg.curso);
                    $("#select-empresa").val(response.msg.empresa==null?0:response.msg.empresa.id).trigger('change');

                    cargar(false,'#guardarMatricula');
                    cargar(false,'#guardarMatriculaR');

                    $('#vistaPreInscripcionPersona2').hide();
                    $('#vistaPreInscripcionPersona1').show();

                    PERSONA = 0;
                } else {
                    $('#Rnombres').val(response.msg.nombres);
                    $('#Rapellidos').val(response.msg.apellidos);
                    $('#Rdocumento').val(response.msg.documento);
                    $('#RcursoInteresado').html("Esta interesado en :" + response.msg.curso);
                    $("#Rselect-empresa").val(response.msg.empresa==null?0:response.msg.empresa.id).trigger('change');

                    cargar(false,'#guardarMatricula');
                    cargar(false,'#guardarMatriculaR');

                    $('#vistaPreInscripcionPersona2').show();
                    $('#vistaPreInscripcionPersona1').hide();
                    PERSONA = response.msg.persona.id;
                }
                //EMPRESA=response.msg.empresa.id;

                return response;

            }).fail(function (error) {

                console.log(error);

            });
        });

        function matricular(id) {
            matriculas();
            PREMATRICULA = id;
            $.ajax({
                    url: '/buscar-preinscripcion',
                    type: 'POST',
                    data: {
                        id: PREMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);
                if (response.msg.persona == null) {
                    $('#nombres').val(response.msg.nombres);
                    $('#apellidos').val(response.msg.apellidos);
                    $('#documento').val(response.msg.documento);

                    $('#ocupacion').val(response.msg.ocupacion);
                    $('#telefono').val(response.msg.telefono);

                    $('#email').val(response.msg.email);
                    $('#cursoInteresado').html("Esta interesado en :" + response.msg.curso);
                    $("#select-empresa").val(response.msg.empresa==null?0:response.msg.empresa.id).trigger('change');
                    $('#modal-1').modal();
                    PERSONA = 0;
                } else {
                    $('#Rnombres').val(response.msg.nombres);
                    $('#Rapellidos').val(response.msg.apellidos);
                    $('#Rdocumento').val(response.msg.documento);
                    $('#RcursoInteresado').html("Esta interesado en :" + response.msg.curso);
                    $("#Rselect-empresa").val(response.msg.empresa==null?0:response.msg.empresa.id).trigger('change');
                    $('#modal-2').modal();
                    PERSONA = response.msg.persona.id;
                }
                //EMPRESA=response.msg.empresa.id;

                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }

        function guardar() {
            $('#Rerror').hide();
            $('#error').hide();
            if (PERSONA == 0) {
                cargar(true,'#guardarMatricula');
                crearUsuario();
            } else {
                cargar(true,'#guardarMatriculaR');
                crearInscripcion(PERSONA, $('#Rdocumentado').val(), $('#Rpago').val(), $('#Rselect-curso').val(),$('#Rselect-empresa').val());
            }

        }

        function crearUsuario() {

            $.ajax({
                url: '/crear-usuarios',
                type: 'POST',
                data: {
                    nombres: $('#nombres').val().toUpperCase(),
                    apellidos: $('#apellidos').val().toUpperCase(),
                    tipo_documento: "CC",
                    documento: $('#documento').val(),
                    ocupacion: $('#ocupacion').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    fecha_nacimiento: $('#fecha_nacimiento').val(),
                    sexo: $('#sexo').val(),
                    city_id: $('#select-ciudad').val(),
                    empresa_id: $('#select-empresa').val(),
                    rol_id: 5,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                    cargar(false,'#guardarMatricula');
                    cargar(false,'#guardarMatriculaR');
                } else {
                    crearInscripcion(response.msg.persona.id, $('#documentado').val(), $('#pago').val(), $('#select-curso').val(),$('#select-empresa').val());
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false,'#guardarMatricula');
                cargar(false,'#guardarMatriculaR');
            });

        }

        function crearInscripcion(persona_id, documentado, pago, curso,empresa) {
            //        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
            //        'fecha_inicio_curso','fecha_fin_curso','estado','entrenador_id'
            $.ajax({
                url: '/crear-inscripcion',
                type: 'POST',
                data: {
                    documentado: documentado,
                    pago: pago,
                    persona_id: persona_id,
                    curso_id: curso,
                    empresa_id: empresa,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                    $("#Rerror").html(response.msg);
                    $("#Rerror").show();
                    cargar(false,'#guardarMatricula');
                    cargar(false,'#guardarMatriculaR');
                } else {
                    terminarPrematricula();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    $("#Rerror").html(value[0]);
                    $("#Rerror").show();
                });
            });

        }

        function terminarPrematricula() {
            $.post(
                "/terminar-preinscripcion", {id: PREMATRICULA, _token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                TABLA.ajax.reload();
                $("#guardarMatricula")[0].reset();
                $("#guardarMatriculaR")[0].reset();
                $("#error").hide();
                $("#Rerror").hide();
                $('#modal-3').modal('hide');
                EMPRESA=null
                cargar(false,'#guardarMatricula');
                cargar(false,'#guardarMatriculaR');
                notify('Estudiante Matriculado con exito','success');
            });

        }

        function eliminarPrematricula() {
            $.post(
                "/terminar-preinscripcion", {id: PREMATRICULA, _token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                TABLA.ajax.reload();
                $('#modal-3').modal('hide');
                EMPRESA=null;
                notify('PreMatricula Eliminada con exito','success');
            });

        }

        function cancelar() {
            $('#vistaPreInscripcionPersona1').hide();
            $('#vistaPreInscripcionPersona2').hide();
            $('#vistaPreInscripciones').show();
        }

        function cargar(state,button){
            var d = $(button);
            if (state){
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else{
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

        function notify(message, type) {
                $.notify({
                    message: message,
                    icon: 'feather icon-bell',
                }, {
                    type: type,
                    allow_dismiss: false,
                    label: 'Cancel',
                    className: 'btn-xs btn-inverse',
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    delay: 4000,
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    },
                    offset: {
                        x: 30,
                        y: 30
                    }
                });
            }

    </script>


@endsection