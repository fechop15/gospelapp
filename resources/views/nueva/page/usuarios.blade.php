@extends('nueva.layout.Dashboard')
@section('page')

    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Empleados</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                                class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Empleados Del Sistema</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">

                    <div class="card user-profile-list animated fadeIn" id="vistaEmpleados">
                        <div class="card-header">
                            <h5>Empleados Del Sistema</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary" id="nuevoEmpleado">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Registrar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-hover" id="tablaEmpleados" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Cedula</th>
                                        <th>Rol</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaRegistroEmpleado" style="display: none">
                        <div class="card-header">
                            <h5>Registro De Empleado</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarUsuario">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombres</label>
                                            <input type="text" id="nombres" class="form-control" placeholder="Nombres">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Apellidos</label>
                                            <input type="text" id="apellidos" class="form-control"
                                                   placeholder="Apellidos">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Telefono</label>
                                            <input type="number" id="telefono" class="form-control"
                                                   placeholder="Telefono">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ocupacion</label>
                                            <input type="text" id="ocupacion" class="form-control"
                                                   placeholder="Ocupacion">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Tipo Documento</label>
                                            <select class="form-control" id="tipo_documento">
                                                <option value="CC">CC</option>
                                                <option value="TI">TI</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Documento</label>
                                            <input type="number" id="documento" class="form-control"
                                                   placeholder="Documento">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Fecha Nacimiento</label>
                                            <input type="date" id="fecha_nacimiento" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Correo</label>
                                            <input type="email" id="email" class="form-control" placeholder="Correo">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Contraseña</label>
                                            <input type="text" id="password" class="form-control"
                                                   placeholder="Contraseña">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Sexo</label>
                                            <select class="form-control" id="sexo">
                                                <option value="Masculino">Masculino</option>
                                                <option value="Femenino">Femenino</option>
                                                <option value="Otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Grupo Ganguineo</label>
                                            <select class="form-control" id="tipo_sangre">
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Departamento</label>
                                            <select class="col-sm-12" id="select-municipio" name="state"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ciudad</label>
                                            <select class="col-sm-12" id="select-ciudad" name="city"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Rol</label>
                                            <select class="form-control" id="rol">
                                                <option value="2">Empleado</option>
                                                <option value="3">Entrenador</option>
                                                <option value="5">Estudiante</option>
                                                <option value="1">Administrador</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaActualizarEmpleado" style="display: none">
                        <div class="card-header">
                            <h5>Actualizar De Empleado</h5>
                        </div>
                        <div class="card-body">
                            <form id="actualizarUsuario">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombres</label>
                                            <input type="text" id="Anombres" class="form-control" placeholder="Nombres">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Apellidos</label>
                                            <input type="text" id="Aapellidos" class="form-control"
                                                   placeholder="Apellidos">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Telefono</label>
                                            <input type="number" id="Atelefono" class="form-control"
                                                   placeholder="Telefono">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ocupacion</label>
                                            <input type="text" id="Aocupacion" class="form-control"
                                                   placeholder="Ocupacion">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Tipo Documento</label>
                                            <select class="form-control" id="Atipo_documento">
                                                <option value="CC">CC</option>
                                                <option value="TI">TI</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Documento</label>
                                            <input type="number" id="Adocumento" class="form-control"
                                                   placeholder="Documento">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Fecha Nacimiento</label>
                                            <input type="date" id="Afecha_nacimiento" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Correo</label>
                                            <input type="email" id="Aemail" class="form-control" placeholder="Correo">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Sexo</label>
                                            <select class="form-control" id="Asexo">
                                                <option value="Masculino">Masculino</option>
                                                <option value="Femenino">Femenino</option>
                                                <option value="Otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Grupo Ganguineo</label>
                                            <select class="form-control" id="Atipo_sangre">
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Departamento</label>
                                            <select class="col-sm-12" id="Aselect-municipio" name="state"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ciudad</label>
                                            <select class="col-sm-12" id="Aselect-ciudad" name="city"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Rol</label>
                                            <select class="form-control" id="Arol">
                                                <option value="2">Empleado</option>
                                                <option value="3">Entrenador</option>
                                                <option value="5">Estudiante</option>
                                                <option value="1">Administrador</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group fill">
                                            <label>Estado</label>
                                            <select class="form-control" id="estado">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    <!-- [ Main Content ] end -->

    @include('nueva.modal.cambioPassword')
@endsection
@section('js')

    <script>
        var IDUSUARIO = 0;
        var STATES = [];

        $('#select-ciudad').select2();

        $('#select-municipio').select2();

        var TABLA = $('#tablaEmpleados').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            "ajax": {
                "url": "/get-usuarios",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>');

                        var estadoYOpciones = estado + '<div class="overlay-edit">' + opciones(data.msg[item].id) + '</div>';

                        var usuario = '<div class="d-inline-block align-middle">' +
                            '<img src="/images/perfil/'+data.msg[item].imagen +'" alt="user image" class="img-radius align-top m-r-15" style="width:40px;">' +
                            '<div class="d-inline-block">' +
                            '<h6 class="m-b-0">' + data.msg[item].nombre + '</h6>' +
                            '<p class="m-b-0">' + data.msg[item].email + '</p>' +
                            '</div>' +
                            '</div>';
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombres: usuario,
                            Cedula: data.msg[item].cedula,
                            Correo: data.msg[item].email,
                            Rol: data.msg[item].rol,
                            Telefono: data.msg[item].telefono,
                            Estado: estadoYOpciones,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombres"},
                {data: "Cedula"},
                {data: "Rol"},
                {data: "Telefono"},
                {data: "Estado"},
            ]
        });

        $.get(
            "/getStates",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                STATES.push(itemSelect2)
            }
            $('#select-municipio').select2({
                data: STATES
            });
            $('#Aselect-municipio').select2({
                data: STATES
            });

        });

        $('#select-municipio').on('change', function (e) {
            var cities = [];
            $("#select-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#select-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    allowClear: true,
                    data: cities
                });

            });
        });

        $('#Aselect-municipio').on('change', function (e) {
            var cities = [];
            $("#Aselect-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#Aselect-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#Aselect-ciudad').select2({
                    allowClear: true,
                    data: cities
                });

            });
        });

        $('#nuevoEmpleado').on('click', function () {
            $('#vistaEmpleados').hide();
            $('#vistaRegistroEmpleado').show();
        });

        function cancelar() {
            $('#vistaEmpleados').show();
            $('#vistaRegistroEmpleado').hide();
            $('#vistaActualizarEmpleado').hide();
        }

        function opciones() {
            return '' +
                '<button type="button" class="btn btn-icon btn-warning actualizar">' +
                '        <i class="feather icon-edit"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-icon btn-info perfil">' +
                '        <i class="feather icon-eye"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-icon btn-success password">' +
                '        <i class="fas fa-key"></i>' +
                '</button>';
        }

        $('#guardar').on('click', function () {
            cargar(true, this);
            $.ajax({
                url: '/crear-usuarios',
                type: 'POST',
                data: {
                    nombres: $('#nombres').val().toUpperCase(),
                    apellidos: $('#apellidos').val().toUpperCase(),
                    tipo_documento: $('#tipo_documento').val(),
                    documento: $('#documento').val(),
                    ocupacion: $('#ocupacion').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    fecha_nacimiento: $('#fecha_nacimiento').val(),
                    sexo: $('#sexo').val(),
                    tipo_sangre: $('#tipo_sangre').val(),
                    city_id: $('#select-ciudad').val(),
                    rol_id: $('#rol').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {

                    TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                    notify('Empleado regustrado con exito', 'success');
                    cancelar();
                }
                cargar(false, this);

                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false, '#guardarUsuario');
                });

            });
        });

        $('#actualizar').on('click', function () {
            cargar(true, this);
            $("#Aerror").hide();
            console.log(IDUSUARIO);
            $.ajax({
                    url: '/actualizar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        nombres: $('#Anombres').val().toUpperCase(),
                        apellidos: $('#Aapellidos').val().toUpperCase(),
                        tipo_documento: $('#Atipo_documento').val(),
                        documento: $('#Adocumento').val(),
                        ocupacion: $('#Aocupacion').val().toUpperCase(),
                        telefono: $('#Atelefono').val(),
                        email: $('#Aemail').val(),
                        fecha_nacimiento: $('#Afecha_nacimiento').val(),
                        sexo: $('#Asexo').val(),
                        tipo_sangre: $('#Atipo_sangre').val().toUpperCase(),
                        city_id: $('#Aselect-ciudad').val(),
                        rol_id: $('#Arol').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Empleado actualizado con exito', 'success');
                    cancelar();
                    $("#actualizarUsuario")[0].reset();
                    $("#Aerror").hide();
                }
                cargar(false, this);
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();
                });
                cargar(false, this);

            });
        });

        function cambiarPass() {
            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    id: IDUSUARIO,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else {
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

        TABLA.on('click', '.actualizar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDUSUARIO = data.Id;
            $("#actualizarUsuario")[0].reset();
            $('#vistaEmpleados').hide();
            $('#vistaActualizarEmpleado').show();
            cargar(true, '#actualizarUsuario');

            $.ajax({
                    url: '/buscar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombres').val(response.msg.persona.nombres);
                $('#Aapellidos').val(response.msg.persona.apellidos);
                $('#Atipo_documento').val(response.msg.persona.tipo_documento);
                $('#Adocumento').val(response.msg.persona.documento);
                $('#Aocupacion').val(response.msg.persona.ocupacion);
                $('#Atelefono').val(response.msg.persona.telefono);
                $('#Aemail').val(response.msg.email);
                $('#Apassword').val(response.msg.password);
                $('#Afecha_nacimiento').val(response.msg.persona.fecha_nacimiento);
                $('#Asexo').val(response.msg.persona.sexo);
                $('#Atipo_sangre').val(response.msg.persona.tipo_sangre);
                $("#Aselect-municipio").val(response.msg.municipio).trigger('change');
                $('#Arol').val(response.msg.rol_id);
                $('#estado').val(response.msg.estado);
                $("#Aselect-empresa").val(response.msg.persona.empresa_id == null ? 0 : response.msg.persona.empresa_id).trigger('change');
                $('#estado').val(response.msg.estado);

                $.get(
                    "/getCities", {'state': response.msg.municipio}
                ).done(function (data) {
                    console.log(data);
                    var cities = [];
                    for (var item in data.msg) {
                        var itemSelect2 = {
                            id: data.msg[item].id,
                            text: data.msg[item].name
                        };
                        cities.push(itemSelect2)
                    }
                    $('#Aselect-ciudad').select2({
                        allowClear: true,
                        data: cities
                    });
                    $("#Aselect-ciudad").val(response.msg.persona.city_id).trigger('change');
                    cargar(false, '#actualizarUsuario');
                });

                //$('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);

            });
        });

        TABLA.on('click', '.perfil', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            window.location.href = "/dashboard/perfil/" + data.Id;
            //alert("Perfil");
        });

        TABLA.on('click', '.password', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDUSUARIO = data.Id;
            $('#modal-pass').modal();
            $("#form-password")[0].reset();
            $("#Perror").hide();
        });

    </script>


@endsection