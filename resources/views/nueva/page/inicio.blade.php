@extends('nueva.layout.Dashboard')
@section('page')
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Dashboard</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Informacion General</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-lg-7 col-md-12">
                    <!-- support-section start -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card support-bar overflow-hidden">
                                <div class="card-body pb-0">
                                    <h2 class="m-0" id="TotalEventos">0</h2>
                                    <span class="text-c-blue">Evento</span>
                                    <p class="mb-3 mt-3">Numero total de visitas de los eventos.</p>
                                </div>
                                <div id="support-chart"></div>
                                <div class="card-footer bg-primary text-white">
                                    <div class="row text-center">
                                        <div class="col">
                                            <h4 class="m-0 text-white" id="TotalEventosLeidos">0</h4>
                                            <span>Leidas</span>
                                        </div>
                                        <div class="col">
                                            <h4 class="m-0 text-white" id="TotalEventosLikes">0</h4>
                                            <span>Likes</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card support-bar overflow-hidden">
                                <div class="card-body pb-0">
                                    <h2 class="m-0" id="TotalPredicas">0</h2>
                                    <span class="text-c-green">Predicas</span>
                                    <p class="mb-3 mt-3">Numero total de visitas de las predicas.</p>
                                </div>
                                <div id="support-chart1"></div>
                                <div class="card-footer bg-success text-white">
                                    <div class="row text-center">
                                        <div class="col">
                                            <h4 class="m-0 text-white" id="TotalPredicasLeidos">
                                               0
                                            </h4>
                                            <span>Leidas</span>
                                        </div>
                                        <div class="col">
                                            <h4 class="m-0 text-white" id="TotalPredicasLikes">0</h4>
                                            <span>Likes</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- support-section end -->
                </div>
                <div class="col-lg-5 col-md-12">
                    <!-- page statustic card start -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-yellow">{{$usuarios->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Usuarios</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-users f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-yellow">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">En El Sistema</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-green">{{$usuarios->where('rol_id','=','5')->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Estudiantes</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-user-graduate f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-green">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">Registrados</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-red">{{$cursos->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Cursos</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-book f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-red">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">Creados</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-blue">{{$certificados->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Certificados</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-certificate f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-blue">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">Generados</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- page statustic card end -->
                </div>
                <!-- prject ,team member start -->
                <div class="col-xl-7 col-md-12">
                    <div class="card table-card">
                        <div class="card-header">
                            <h5>Usuarios</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Ultimo Ingreso</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($usuarios->sortByDesc('last_login')->take(5) as $usuario)
                                        @if($usuario->last_login!=null)

                                            <tr>
                                                <td>
                                                    <div class="d-inline-block align-middle">
                                                        <img src="/images/perfil/{{$usuario->imagen}}" alt="user image"
                                                             class="img-radius wid-40 hei-40 align-top m-r-15">
                                                        <div class="d-inline-block">
                                                @if($usuario->persona!=null)
                                                                <h6>{{$usuario->persona->nombres}} {{$usuario->persona->apellidos}}</h6>
                                                @else
                                                                <h6>{{$usuario->empresa->nombre}}</h6>
                                                @endif
                                                    <p class="text-muted m-b-0">Rol: {{$usuario->rol->nombre}}</p>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($usuario->last_login)->diffForHumans()}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12">
                    <div class="card user-card2">
                        <div class="card-body text-center">
                            <h6 class="m-b-15">Bienvenido</h6>
                            <img class="img-radius img-fluid wid-150" src="/images/perfil/{{auth()->user()->imagen}}" alt="User image">
                            <h6 class="m-b-10 m-t-10">{{auth()->user()->persona->nombres}} {{auth()->user()->persona->apellidos}}</h6>
                            <a href="{{route('perfil')}}" class="text-c-green b-b-success">{{auth()->user()->rol->nombre}}</a>
                            <div class="row justify-content-center m-t-10 b-t-default m-l-0 m-r-0">
                                <div class="col m-t-15 b-r-default">
                                    <h6 class="text-muted">Cursos</h6>
                                    <h6>0</h6>
                                </div>
                                <div class="col m-t-15">
                                    <h6 class="text-muted">Registro</h6>
                                    <h6>{{auth()->user()->created_at}}</h6>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-success btn-block" href="{{route('perfil')}}">Ver mi perfil</a>
                    </div>
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    <!-- @ include('nueva.modal.inicio') -->
@endsection
@section('js')

    <!-- custom-chart js -->
    <script>
      /*  $(document).ready(function () {
            checkCookie();
        });



        $(document).ready(function() {
            setTimeout(function() {
               // floatchart()
            }, 100);
        });
         */

        var eventos=[];
        var eventosGrafica=[];
        var predicas=[];
        var predicasGrafica=[];
        $.get('/get-noticias').done(function (response) {
            //console.log(response);
            var totalLikesP=0;
            var totalVistosP=0;
            var totalLikesE=0;
            var totalVistosE=0;
            response.noticias.forEach(function (noticia) {
                if (noticia.tipo==='Predica'){
                    predicas.push(noticia);
                    predicasGrafica.push(noticia.vistos);
                    totalVistosP+=noticia.vistos;
                    totalLikesP+=noticia.like;
                } else {
                    eventos.push(noticia);
                    eventosGrafica.push(noticia.vistos);
                    totalVistosE+=noticia.vistos;
                    totalLikesE+=noticia.like;
                }
            });
            $('#TotalEventos').html(eventos.length);
            $('#TotalEventosLeidos').html(totalVistosE);
            $('#TotalEventosLikes').html(totalLikesE);

            $('#TotalPredicas').html(predicas.length);
            $('#TotalPredicasLeidos').html(totalVistosP);
            $('#TotalPredicasLikes').html(totalLikesP);
            floatchart();
        });
/*
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function checkCookie() {
            var ticks = getCookie("modelopen");
            if (ticks != "") {
                ticks++;
                setCookie("modelopen", ticks, 1);
                if (ticks == "2" || ticks == "1" || ticks == "0") {
                    $('#exampleModalCenter').modal();
                }
            } else {
                // user = prompt("Please enter your name:", "");
                $('#exampleModalCenter').modal();
                ticks = 1;
                setCookie("modelopen", ticks, 1);
            }
        }

 */
        function floatchart() {
            // [ support-chart ] start
            $(function() {
                var options1 = {
                    chart: {
                        type: 'area',
                        height: 80,
                        sparkline: {
                            enabled: true
                        }
                    },
                    colors: ["#4680ff"],
                    stroke: {
                        curve: 'smooth',
                        width: 2,
                    },
                    series: [{
                        data: eventosGrafica
                    }],
                    tooltip: {
                        fixed: {
                            enabled: false
                        },
                        x: {
                            show: false
                        },
                        y: {
                            title: {
                                formatter: function(seriesName) {
                                    return 'Ticket '
                                }
                            }
                        },
                        marker: {
                            show: false
                        }
                    }
                }
                new ApexCharts(document.querySelector("#support-chart"), options1).render();
            });
            // [ support-chart ] end
            // [ support-chart1 ] start
            $(function() {
                var options1 = {
                    chart: {
                        type: 'area',
                        height: 80,
                        sparkline: {
                            enabled: true
                        }
                    },
                    colors: ["#9ccc65"],
                    stroke: {
                        curve: 'smooth',
                        width: 2,
                    },
                    series: [{
                        data: predicasGrafica
                    }],
                    tooltip: {
                        fixed: {
                            enabled: false
                        },
                        x: {
                            show: false
                        },
                        y: {
                            title: {
                                formatter: function(seriesName) {
                                    return 'Ticket '
                                }
                            }
                        },
                        marker: {
                            show: false
                        }
                    }
                }
                new ApexCharts(document.querySelector("#support-chart1"), options1).render();
            });
            // [ support-chart1 ] end
        }
    </script>
@endsection