@extends('nueva.layout.Dashboard')
@section('page')
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Empresas</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                                class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Empresas Registrados</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">
                    <div class="card user-profile-list animated fadeIn" id="vistaEmpresas">
                        <div class="card-header">
                            <h5>Gestion de Empresas</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary" id="nuevaEmpresa">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Registrar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-hover" id="tablaEmpresas" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Sector</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaRegistroEmpresa" style="display: none">
                        <div class="card-header">
                            <h5>Registro De Empresa</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarEmpresa">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombre Empresa</label>
                                            <input type="text" id="empresa" class="form-control"
                                                   placeholder="Nombre empresa">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nit</label>
                                            <input type="text" id="nit" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Contacto</label>
                                            <input type="text" id="contacto" class="form-control"
                                                   placeholder="Nombre contacto">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Telefono</label>
                                            <input type="number" id="telefono" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Correo</label>
                                            <input type="email" id="email" class="form-control" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Contraseña</label>
                                            <input type="text" id="password" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Sector</label>
                                            <select class="form-control" id="sector">
                                                <option value="Electrico">Electrico</option>
                                                <option value="Telecomunicaciones">Telecomunicaciones</option>
                                                <option value="Hidrocarburos">Hidrocarburos</option>
                                                <option value="Construccion">Construccion</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaActualizarEmpresa" style="display: none">
                        <div class="card-header">
                            <h5>Actualizar Empresa</h5>
                        </div>
                        <div class="card-body">
                            <form id="actualizarEmpresa">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombre Empresa</label>
                                            <input type="text" id="Aempresa" class="form-control"
                                                   placeholder="Nombre empresa">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nit</label>
                                            <input type="text" id="Anit" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Contacto</label>
                                            <input type="text" id="Acontacto" class="form-control"
                                                   placeholder="Nombre contacto">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Telefono</label>
                                            <input type="number" id="Atelefono" class="form-control" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Correo</label>
                                            <input type="email" id="Aemail" class="form-control" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Sector</label>
                                            <select class="form-control" id="Asector">
                                                <option value="Electrico">Electrico</option>
                                                <option value="Telecomunicaciones">Telecomunicaciones</option>
                                                <option value="Hidrocarburos">Hidrocarburos</option>
                                                <option value="Construccion">Construccion</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group fill">
                                            <label>Estado</label>
                                            <select class="form-control" id="estado">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    <!-- [ Main Content ] end -->

    @include('nueva.modal.cambioPassword')
@endsection
@section('js')

    <script>
        var IDEMPRESA = 0;
        var STATES = [];

        $('#select-ciudad').select2();

        $('#select-municipio').select2();

        var TABLA = $('#tablaEmpresas').DataTable({
            "ajax": {
                "url": "/get-empresas",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>');
                        var estadoYOpciones = estado + '<div class="overlay-edit">' + opciones(data.msg[item].estado) + '</div>';
                        var usuario = '<div class="d-inline-block align-middle">' +
                            '<img src="/images/perfil/' + data.msg[item].usuario.imagen + '" alt="user image" class="img-radius align-top m-r-15" style="width:40px;">' +
                            '<div class="d-inline-block">' +
                            '<h6 class="m-b-0">' + data.msg[item].nombre + '</h6>' +
                            '<p class="m-b-0">' + data.msg[item].contacto + '</p>' +
                            '<p class="m-b-0">' + data.msg[item].email + '</p>' +
                            '</div>' +
                            '</div>';
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: usuario,
                            Sector: data.msg[item].sector,
                            Contacto: data.msg[item].contacto,
                            Telefono: data.msg[item].telefono,
                            Correo: data.msg[item].email,
                            Estado: estadoYOpciones
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Sector"},
                {data: "Telefono"},
                {data: "Estado"},
            ]
        });

        function opciones(estado) {
            var opciones = '';

            opciones += '' +
                '<button type="button" class="btn btn-icon btn-warning actualizar">' +
                '        <i class="feather icon-edit"></i>' +
                '</button>';

            if (estado) {
                opciones += '' +
                    '<button type="button" class="btn btn-icon btn-danger verificar">' +
                    '        <i class="feather icon-x"></i>' +
                    '</button>';
            } else {
                opciones += '' +
                    '<button type="button" class="btn btn-icon btn-success verificar">' +
                    '        <i class="feather icon-check"></i>' +
                    '</button>';
            }

            opciones += '' +
                '<button type="button" class="btn btn-icon btn-info perfil">' +
                '        <i class="feather icon-eye"></i>' +
                '</button>';
            opciones += '' +
                '<button type="button" class="btn btn-icon btn-success password">' +
                '        <i class="fas fa-key"></i>' +
                '</button>';

            return opciones;
        }

        $('#nuevaEmpresa').on('click', function () {
            $('#vistaEmpresas').hide();
            $('#vistaRegistroEmpresa').show();
            $('#vistaActualizarEmpresa').hide();
        });

        $('#guardar').on('click', function () {
            cargar(true, this);
            $.ajax({
                url: '/crear-empresa',
                type: 'POST',
                data: {
                    nombre: $('#empresa').val().toUpperCase(),
                    nit: $('#nit').val().toUpperCase(),
                    contacto: $('#contacto').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    sector: $('#sector').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Empresa regustrada con exito', 'success');
                    cancelar();
                    $("#guardarEmpresa")[0].reset();
                    $("#error").hide();
                }
                cargar(false, '#guardarEmpresa');

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false, '#guardarEmpresa');

            });

        });

        $('#actualizar').on('click', function () {
            cargar(true, this);
            $("#Aerror").hide();
            $.ajax({
                    url: '/actualizar-empresa',
                    type: 'POST',
                    data: {
                        id: IDEMPRESA,
                        nombre: $('#Aempresa').val().toUpperCase(),
                        nit: $('#Anit').val().toUpperCase(),
                        contacto: $('#Acontacto').val().toUpperCase(),
                        telefono: $('#Atelefono').val(),
                        email: $('#Aemail').val(),
                        sector: $('#Asector').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    //$('#modal-actualizar').modal('hide');
                    notify('Empresa actualizada con exito', 'success');
                    cancelar();
                    $('#actualizarEmpresa')[0].reset();
                    $("#Aerror").hide();
                }
                cargar(false, '#actualizarEmpresa');

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
                cargar(false, '#actualizarEmpresa');
            });
        });

        function cambiarPass() {
            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    id: IDEMPRESA,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cancelar() {
            $('#vistaEmpresas').show();
            $('#vistaRegistroEmpresa').hide();
            $('#vistaActualizarEmpresa').hide();
        }

        TABLA.on('click', '.actualizar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDEMPRESA = data.Id;
            $("#actualizarEmpresa")[0].reset();
            $('#vistaEmpresas').hide();
            $('#vistaActualizarEmpresa').show();
            cargar(true, '#actualizarEmpresa');

            $.ajax({
                    url: '/buscar-empresa',
                    type: 'POST',
                    data: {
                        id: IDEMPRESA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Aempresa').val(response.msg.nombre);
                $('#Anit').val(response.msg.nit);
                $('#Acontacto').val(response.msg.contacto);
                $('#Atelefono').val(response.msg.telefono);
                $('#Aemail').val(response.msg.email);
                $('#Asector').val(response.msg.sector);
                $('#estado').val(response.msg.estado);

                cargar(false, '#actualizarEmpresa');

                return response;

            }).fail(function (error) {

                console.log(error);

            });

        });

        TABLA.on('click', '.perfil', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            location.href = "/dashboard/perfil/" + data.Id;
            //alert("Perfil");
        });

        TABLA.on('click', '.verificar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();

            $.ajax({
                    url: '/verificar-empresa',
                    type: 'POST',
                    data: {
                        id: data.Id,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    //error
                } else {
                    TABLA.ajax.reload();
                    if(response.msg.estado==1){
                        notify('Empresa verificada con exito', 'success');
                    }else{
                        notify('Empresa desabilitada con exito', 'success');
                    }


                }
                //return response;
            }).fail(function (error) {

                console.log(error);

            });

        });

        TABLA.on('click', '.password', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDEMPRESA = data.Id;
            $('#modal-pass').modal();
            $("#form-password")[0].reset();
            $("#Perror").hide();

        });

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else {
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

    </script>


@endsection