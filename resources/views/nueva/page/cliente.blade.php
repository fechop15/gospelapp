@extends('nueva.layout.Dashboard')
@section('page')

    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>{{$estudiante->persona->nombres}} {{$estudiante->persona->apellidos}}</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('estudiantes')}}">Estudiantes</a></li>
                                <li class="breadcrumb-item"><a href="#!">Perfil</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-pills bg-white" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active text-uppercase" id="user1-tab" data-toggle="tab"
                                       href="#user1" role="tab" aria-controls="user1"
                                       aria-selected="true">Informacion</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user2-tab" data-toggle="tab" href="#user2"
                                       role="tab" aria-controls="user2" aria-selected="false">Mis Cursos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user3-tab" data-toggle="tab" href="#user3"
                                       role="tab" aria-controls="user3" aria-selected="false">Cuenta</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user4-tab" data-toggle="tab" href="#user4"
                                       role="tab" aria-controls="user4" aria-selected="false">Cursos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <!-- [ user card1 ] start -->
                        <div class="tab-pane fade show active" id="user1" role="tabpanel" aria-labelledby="user1-tab">
                            <div class="row mb-n4">
                                <div class="col-md-12">
                                    <div class="card bg-light">
                                        <div class="card-header">
                                            <h5>Informacion Personal</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button"
                                                            class="btn btn-icon btn-light"
                                                            id="editar-btn">
                                                        <i class="feather icon-edit-2"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="view-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="general-info">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-xl-6">
                                                                    <table class="table m-0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th scope="row">Nombres</th>
                                                                            <td>{{$estudiante->persona->nombres}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Apellidos</th>
                                                                            <td>{{$estudiante->persona->apellidos}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Identificacion</th>
                                                                            <td>{{$estudiante->persona->documento}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Telefono</th>
                                                                            <td>{{$estudiante->persona->telefono}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Fecha Nacimiento</th>
                                                                            <td>{{$estudiante->persona->fecha_nacimiento}}</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <th scope="row">Empresa</th>
                                                                            <td>{{ $estudiante->persona->empresa==null?'Ninguna':$estudiante->persona->empresa->nombre}}</td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->

                                                                <div class="col-lg-12 col-xl-6">
                                                                    <table class="table">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th scope="row">Ciudad</th>
                                                                            <td>{{$estudiante->persona->city->name}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Correo</th>
                                                                            <td>{{$estudiante->email}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Ocupacion</th>
                                                                            <td> {{$estudiante->persona->ocupacion}}</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <th scope="row">Sexo</th>
                                                                            <td>{{$estudiante->persona->sexo}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Grupo Sanguineo</th>
                                                                            <td>{{ $estudiante->persona->tipo_sangre}}</td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of general info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <div class="edit-info" style="display: none;">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="general-info">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <form id="guardarUsuario">

                                                                        <div class="row">

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Nombres</label>
                                                                                    <input type="text" id="nombres"
                                                                                           value="{{$estudiante->persona->nombres}}"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Apellidos</label>
                                                                                    <input type="text" id="apellidos"
                                                                                           value="{{$estudiante->persona->apellidos}}"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Telefono</label>
                                                                                    <input type="number" id="telefono"
                                                                                           value="{{$estudiante->persona->telefono}}"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Tipo Documento</label>
                                                                                    <select class="form-control"
                                                                                            id="tipo_documento">
                                                                                        <option value="CC">CC</option>
                                                                                        <option value="TI">TI</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Documento</label>
                                                                                    <input type="number" id="documento"
                                                                                           class="form-control"
                                                                                           value="{{$estudiante->persona->documento}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Ocupacion</label>
                                                                                    <input type="text" id="ocupacion"
                                                                                           class="form-control"
                                                                                           value="{{$estudiante->persona->ocupacion}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Fecha Nacimiento</label>
                                                                                    <input type="date"
                                                                                           id="fecha_nacimiento"
                                                                                           class="form-control"
                                                                                           value="{{$estudiante->persona->fecha_nacimiento}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Correo</label>
                                                                                    <input type="email" id="email"
                                                                                           class="form-control"
                                                                                           value="{{$estudiante->email}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Grupo Ganguineo</label>
                                                                                    <select class="form-control"
                                                                                            id="tipo_sangre">
                                                                                        <option value="O+">O+</option>
                                                                                        <option value="O-">O-</option>
                                                                                        <option value="A+">A+</option>
                                                                                        <option value="A-">A-</option>
                                                                                        <option value="B+">B+</option>
                                                                                        <option value="B-">B-</option>
                                                                                        <option value="AB+">AB+</option>
                                                                                        <option value="AB-">AB-</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Sexo</label>
                                                                                    <select class="form-control" id="sexo">
                                                                                        <option value="Masculino">Masculino</option>
                                                                                        <option value="Femenino">Femenino</option>
                                                                                        <option value="Otro">Otro</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Departamento</label>
                                                                                    <select class="col-sm-12" id="select-municipio" name="state"
                                                                                            style="width: 100%">
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group fill">
                                                                                    <label>Ciudad</label>
                                                                                    <select class="col-sm-12" id="select-ciudad" name="city"
                                                                                            style="width: 100%">
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </form>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->


                                                                <!-- end of table col-lg-6 -->
                                                            </div>
                                                            <!-- end of row -->
                                                            <div class="text-center">
                                                                <button type="button" class="btn btn-primary" onclick="actualizarUsuario()">Actualizar</button>
                                                                <button type="button" class="btn btn-default" id="edit-cancel">Cancelar</button>
                                                            </div>
                                                        </div>
                                                        <!-- end of edit info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [ user card1 ] end -->
                        <!-- varient [ 2 ][ cover shape ] card Start -->
                        <div class="tab-pane fade" id="user2" role="tabpanel" aria-labelledby="user2-tab">
                            <div class="row mb-n4">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-header-text">Cursos</h5>

                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>

                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Curso</th>
                                                        <th class="text-center txt-primary">Fecha</th>
                                                        <th class="text-center txt-primary">Dias</th>
                                                        <th class="text-center txt-primary">Pago</th>
                                                        <th class="text-center txt-primary">Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                    @foreach($estudiante->persona->matriculas->reverse() as $matricula)
                                                        @if($matricula->estado)
                                                            <tr>
                                                                <td>{{$matricula->curso->nombre}} {{$matricula->curso->nivel}}</td>
                                                                <td>{{$matricula->curso->fecha_inicio_curso}}
                                                                    a {{$matricula->curso->fecha_fin_curso}}</td>
                                                                <td>{{$matricula->curso->dias}}</td>

                                                                <td class="text-center">
                                                                    @if($matricula->pago)
                                                                        <span class="badge badge-success">Pago</span>
                                                                    @else
                                                                        <span class="badge badge-warning">Pendiente</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">
                                                                    @if($matricula->pago==1 && $matricula->documentado==1 &&
                                                                        $matricula->fecha_certificacion!=null)
                                                                        @if($matricula->descargable)
                                                                            <button type="button"
                                                                                    class="btn btn-success btn-sm"
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="Descargar Certificado"
                                                                                    data-original-title="Edit"
                                                                                    onclick="descargar('{{$matricula->id}}')">
                                                                                <i class="feather icon-download"></i>
                                                                            </button>
                                                                        @else
                                                                            <span class="badge badge-success"
                                                                                  data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Su empresa de trabajo no le permite descargar el certificado"
                                                                            >Terminado</span>
                                                                        @endif

                                                                    @else
                                                                        <span class="badge badge-info">En Proceso ..</span>
                                                                    @endif
                                                                    @if(auth()->user()->rol_id<=3)
                                                                        <button type="button"
                                                                                class="btn btn-warning btn-sm"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Ver Curso"
                                                                                data-original-title="Edit"
                                                                                onclick="verCurso('{{$matricula->curso->id}}')">
                                                                            <i class="feather icon-eye"></i>
                                                                        </button>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- varient [ 2 ][ cover shape ] card end -->
                        <!-- varient [ footer color ] card Start -->
                        <div class="tab-pane fade user-card" id="user3" role="tabpanel" aria-labelledby="user3-tab">
                            <div class="row mb-n4">
                                @if(auth()->user()->id==$estudiante->id)
                                    <div class="col-md-6">
                                        <div class="card bg-light">
                                            <div class="card-body">
                                                <h5 class="card-title">Cambio de Contraseña</h5>
                                                <form id="form-password">

                                                    <div class="form-group">
                                                        <label class="col-form-label">Contraseña Actual:</label>
                                                        <input type="text" class="form-control" id="passOld">

                                                        <label class="col-form-label">Contraseña nueva:</label>
                                                        <input type="text" class="form-control" id="pass">

                                                        <label class="col-form-label">Contraseña nueva
                                                            (Confirmacion):</label>
                                                        <input type="text" class="form-control" id="pass2">
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="alert alert-danger" id="Perror"
                                                             style="display: none">
                                                        </div>
                                                    </div>

                                                </form>
                                                <button type="button"
                                                        class="btn btn-primary waves-effect waves-light"
                                                        onclick="cambiarPass()">Confirmar
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="card bg-light">
                                            <div class="card-body">
                                                <h6>Imagen de perfil</h6>
                                                <div class="change-profile text-center">
                                                    <div class="dropdown w-auto d-inline-block">
                                                        <a class="dropdown-toggle" data-toggle="dropdown"
                                                           aria-haspopup="true" aria-expanded="false">
                                                            <div class="profile-dp">
                                                                <div class="position-relative d-inline-block">
                                                                    <img class="img-radius img-fluid wid-100"
                                                                         src="/images/perfil/{{auth()->user()->imagen}}"
                                                                         alt="User image">
                                                                </div>
                                                                <div class="overlay">
                                                                    <span style="left: 0px">Cambiar</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" id="editar"><i
                                                                        class="feather icon-upload-cloud mr-2"></i>Cambiar</a>
                                                            <a class="dropdown-item" id="eliminar"><i
                                                                        class="feather icon-trash-2 mr-2"></i>Quitar</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <form id="subirImagen">
                                                    <input type="file" id="avatar" name="avatar"
                                                           style="display: none" accept="image/*">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- varient [ footer color ] card end -->
                        <!-- varient [ Profile ] card Start -->
                        <div class="tab-pane fade" id="user4" role="tabpanel" aria-labelledby="user4-tab">
                            <div class="row mb-n4">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Cursos Disponibles</h5>
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablaCursos"
                                                       width="100%">
                                                    <thead>

                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Id</th>
                                                        <th class="text-center txt-primary">Curso</th>
                                                        <th class="text-center txt-primary">Cupo</th>
                                                        <th class="text-center txt-primary">Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="text-center">

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- varient [ Profile ] card End -->
                    </div>
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

@endsection

@section('js')

    <script>
        var IDCLIENTE = '{!! $estudiante->id !!}';
        let CLIENTE = {!! $estudiante->persona!!};
        var IDNOTA = 0;
        var STATES = [];
        var CURSOS = [];
        //$(document).ready(function () {
        // Instrucciones a ejecutar al terminar la carga
        $("#error").hide();
        console.log(CLIENTE);

        cargarMunicipios();

        cargarDatos();
        //matriculas();
        var TABLA = $('#tablaCursos').DataTable({
            "ajax": {
                "url": "/get-cursos-matricula",
                "type": "POST",
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content')
                },
                "dataSrc": function (data) {
                    console.log(data);
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Curso: data.msg[item].nombre + " " + data.msg[item].nivel + " - " +
                                data.msg[item].fecha_inicio_curso + " a " + data.msg[item].fecha_fin_curso,
                            Cupos: data.msg[item].matriculados + "/" + data.msg[item].cupos,
                            Opciones: opciones(data.msg[item].id, data.msg[item].nombre + " " + data.msg[item].nivel)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            autoWidth: true,
            order: [[0, "desc"]],
            columns: [
                {data: "Id"},
                {data: "Curso"},
                {data: "Cupos"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            },
        });

        function opciones(id, nombreCurso) {
            var opciones = '';

            opciones += '<button type="button" class="btn btn-sm btn-primary Inscribir" ' +
                '           data-toggle="tooltip" data-placement="top" title="Inscribir" data-original-title="Inscribir"' +
                '           onclick="guardar(`' + nombreCurso + '`)">' +
                '           <i class="feather icon-check"></i>' +
                ' </button>';

            return opciones;
        }

        function cargarDatos() {
            $('#tipo_documento').val(CLIENTE.tipo_documento);
            $('#sexo').val(CLIENTE.sexo);
            $('#tipo_sangre').val(CLIENTE.tipo_sangre);
            $('#select-municipio').val(CLIENTE.city.state_id).trigger('change');
            $('#select-ciudad').select2();


            $.get(
                "/getCities", {'state': CLIENTE.city.state_id}
            ).done(function (data) {
                var cities = [];
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    //dropdownParent: $("#modal-actualizar"),
                    allowClear: true,
                    data: cities
                });
                $("#select-ciudad").val(CLIENTE.city_id).trigger('change');
            });

        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargarMunicipios() {

            $.ajax({
                url: '/getStates',
                type: 'GET',
                async: false,
            }).done(function (data) {
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    STATES.push(itemSelect2)
                }
                $('#select-municipio').select2({
                    //dropdownParent: $("#modal-1"),
                    data: STATES
                });

                //return response;
            }).fail(function (error) {

                console.log(error)

            });


        }

        $('#select-municipio').on('change', function (e) {

            var cities = [];
            $("#select-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#select-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    //dropdownParent: $("#modal-1"),
                    allowClear: true,
                    data: cities
                });

            });
        });

        $("#editar").click(function () {
            $("#avatar").click();
        });

        $("#eliminar").click(function () {

            $.post(
                "/quitar-avatar", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#error").html(data.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + data.imagen);
                    notify(response.msg, 'success');

                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        $('#avatar').change(function () {
            var data = new FormData();
            jQuery.each(jQuery('#avatar')[0].files, function (i, file) {
                data.append('avatar', file);
            });
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: '/cambiar-avatar',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + response.imagen);
                    notify(response.msg, 'success');

                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        function actualizarUsuario() {

            $.ajax({
                    url: '/actualizar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDCLIENTE,
                        nombres: $('#nombres').val().toUpperCase(),
                        apellidos: $('#apellidos').val().toUpperCase(),
                        tipo_documento: $('#tipo_documento').val(),
                        documento: $('#documento').val(),
                        ocupacion: $('#ocupacion').val().toUpperCase(),
                        telefono: $('#telefono').val(),
                        email: $('#email').val(),
                        fecha_nacimiento: $('#fecha_nacimiento').val(),
                        sexo: $('#sexo').val(),
                        tipo_sangre: $('#tipo_sangre').val(),
                        city_id: $('#select-ciudad').val(),
                        empresa_id: $('#select-empresa').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    notify('Usuario Actualizado Correctamente', 'info')
                    location.reload();

                    $("#error").hide();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();

                });
            });
        }

        function descargar(id) {
            window.open("/descargar_certificado/" + id);
        }

        function verCurso(id) {
            location.href = "/dashboard/curso/" + id;
        }

        function cambiarPass() {
            if ($("#pass").val() != $("#pass2").val()) {
                $("#Perror").html("Las Contraseña No Son Iguales");
                $("#Perror").show();
                return;
            }
            $("#Perror").hide();

            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    passOld: $("#passOld").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function guardar(curso) {
            $.ajax({
                url: '/crear-preinscripcion',
                type: 'POST',
                data: {
                    nombres: CLIENTE.nombres,
                    apellidos: CLIENTE.apellidos,
                    tipo_documento: CLIENTE.tipo_documento,
                    documento: CLIENTE.documento,
                    cargo: CLIENTE.ocupacion,
                    telefono: CLIENTE.telefono,
                    email: '{{$estudiante->email}}',
                    curso: curso,
                    nombre_empresa: 'ninguna',
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    notify(response.msg, 'error');

                    //$("#error").html(response.msg);
                    //$("#error").show();
                } else {
                    notify('Inscripcion completada', 'success');
                    TABLA.ajax.reload();
                    //$("#error").hide();
                }
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    notify(value[0], 'error');

                    //$("#error").html(value[0]);
                    //$("#error").show();
                });

            });

        }

        $('#editar-btn').on('click', function () {
            $('.view-info').hide();
            $('#editar-btn').hide();
            $('.edit-info').show();
        });

        $('#edit-cancel').on('click', function () {
            $('.view-info').show();
            $('.edit-info').hide();
            $('#editar-btn').show();

        });


    </script>


@endsection
