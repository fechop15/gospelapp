<nav class="pcoded-navbar menu-light menupos-fixed">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div ">

            <div class="">
                <div class="main-menu-header">
                    <img class="img-radius" src="/images/perfil/{{auth()->user()->imagen}}" alt="User-Profile-Image">
                    <div class="user-details">
                        <div id="more-details" style="font-size:10px;">
                            @if(auth()->user()->empresa==null)
                                {{ auth()->user()->persona->nombres }}
                                {{ auth()->user()->persona->apellidos }}
                            @else
                                {{ auth()->user()->empresa->nombre }}
                            @endif
                            <i class="fa fa-caret-down"></i></div>
                    </div>
                </div>
                <div class="collapse" id="nav-user-link">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="{{route('perfil')}}" data-toggle="tooltip"
                                                        title="Ver Perfil"><i class="feather icon-user"></i></a></li>
{{--                        <li class="list-inline-item"><a href="email_inbox.html"><i class="feather icon-mail"--}}
{{--                                                                                   data-toggle="tooltip"--}}
{{--                                                                                   title="Messages"></i>--}}
{{--                                <small class="badge badge-pill badge-primary">5</small>--}}
{{--                            </a></li>--}}
                        <li class="list-inline-item"><a href="{{route('logout')}}" data-toggle="tooltip" title="Salir"
                                                        class="text-danger"><i class="feather icon-power"></i></a></li>
                    </ul>
                </div>
            </div>

            <ul class="nav pcoded-inner-navbar ">
                <li class="nav-item pcoded-menu-caption">
                    <label>Navegacion</label>
                </li>
                @if(auth()->user()->rol_id >=4 )
                    <li class="nav-item {{ (request()->is('perfil/*')) ? 'active' : '' }}">
                        <a href="{{route('perfil')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-aperture"></i>
                            </span>
                            <span class="pcoded-mtext">Mi Perfil</span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->rol_id == 1 || auth()->user()->rol_id == 2 )
                    <li class="nav-item {{ (request()->is('dashboard/')) ? 'active' : '' }}">
                        <a href="{{route('dashboard')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-home"></i>
                            </span>
                            <span class="pcoded-mtext">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('noticias/*')) ? 'active' : '' }}">
                        <a href="{{route('noticias')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-clipboard"></i>
                            </span>
                            <span class="pcoded-mtext">Noticias</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('pre_inscripciones/*')) ? 'active' : '' }}">
                        <a href="{{route('pre_inscripciones')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-edit-2"></i>
                            </span>
                            <span class="pcoded-mtext">Pre Inscripciones</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('matriculas/*')) ? 'active' : '' }}">
                        <a href="{{route('matriculas')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-calendar"></i>
                            </span>
                            <span class="pcoded-mtext">Matriculas</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('estudiantes/*')) ? 'active' : '' }}">
                        <a href="{{route('estudiantes')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-users"></i>
                            </span>
                            <span class="pcoded-mtext">Estudiantes</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('empresas/*')) ? 'active' : '' }}">
                        <a href="{{route('empresas')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-briefcase"></i>
                            </span>
                            <span class="pcoded-mtext">Empresas</span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->rol_id == 1 || auth()->user()->rol_id == 2 || auth()->user()->rol_id == 3 )
                    <li class="nav-item {{ (request()->is('cursos/*')) || (request()->is('infoCurso/*')) ? 'active' : '' }}">
                        <a href="{{route('cursos')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-book"></i>
                            </span>
                            <span class="pcoded-mtext">Cursos</span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->rol_id == 1)
                    <li class="nav-item pcoded-menu-caption">
                        <label>Administrador</label>
                    </li>
                    <li class="nav-item {{ (request()->is('empleados/*')) ? 'active' : '' }}">
                        <a href="{{route('empleados')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-user"></i>
                            </span>
                            <span class="pcoded-mtext">Empleados</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('certificados/*')) ? 'active' : '' }}">
                        <a href="{{route('certificados')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-file-text"></i>
                            </span>
                            <span class="pcoded-mtext">Certificados</span>
                        </a>
                    </li>
                @endif
            </ul>

{{--            <div class="card text-center">--}}
{{--                <div class="card-block">--}}
{{--                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
{{--                    <i class="feather icon-sunset f-40"></i>--}}
{{--                    <h6 class="mt-3">Help?</h6>--}}
{{--                    <p>Please contact us on our email for need any support</p>--}}
{{--                    <a href="#!" target="_blank" class="btn btn-primary btn-sm text-white m-0">Support</a>--}}
{{--                </div>--}}
{{--            </div>--}}

        </div>
    </div>
</nav>


