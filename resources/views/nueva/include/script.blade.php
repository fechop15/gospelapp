<script src="/assets/js/vendor-all.min.js"></script>
<script src="/assets/js/plugins/bootstrap.min.js"></script>
<script src="/assets/js/ripple.js"></script>
<script src="/assets/js/pcoded.min.js"></script>

<!-- Apex Chart <script src="/assets/js/menu-setting.min.js"></script>-->
<script src="/assets/js/plugins/apexcharts.min.js"></script>
<script src="/assets/js/plugins/bootstrap-notify.min.js"></script>

<!-- datatable Js -->
<script src="/assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="/assets/js/plugins/dataTables.bootstrap4.min.js"></script>
<script src="/assets/js/plugins/dataTables.fixedColumns.min.js"></script>

<!-- sweet alert Js -->
<script src="/assets/js/plugins/sweetalert.min.js"></script>

<!-- select2 Js -->
<script src="/assets/js/plugins/select2.full.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
