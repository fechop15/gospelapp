<header class="navbar pcoded-header navbar-expand-lg navbar-light header-purple">

    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="#!" class="b-brand">
            <!-- ========   change your logo hear   ============ -->
            <img src="/images/logo360.png" alt="" class="logo" style="width: 150px;">
            <img src="/images/logo360.png" alt="" class="logo-thumb" style="width: 150px;">
        </a>
        <a href="#!" class="mob-toggler">
            <i class="feather icon-more-vertical"></i>
        </a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="#!" class="pop-search"><i class="feather icon-search"></i></a>
                <div class="search-bar">
                    <input type="text" class="form-control border-0 shadow-none" placeholder="Search hear">
                    <button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                    <div class="dropdown-menu dropdown-menu-right notification">
                        <div class="noti-head">
                            <h6 class="d-inline-block m-b-0">Notificaciones</h6>
                            <div class="float-right">
                                <a href="#!" class="m-r-10">Marcar leidos</a>
                                <a href="#!">Limpiar todo</a>
                            </div>
                        </div>
                        <div class="noti-footer">
                            <a href="#!">Ver todo</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown drp-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="feather icon-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-notification">
                        <div class="pro-head">
                            <img src="/images/perfil/{{auth()->user()->imagen}}" class="img-radius" alt="User-Profile-Image">
                            <span style="font-size:10px;">
                                 @if(auth()->user()->empresa==null)
                                    {{ auth()->user()->persona->nombres }}
                                    {{ auth()->user()->persona->apellidos }}
                                @else
                                    {{ auth()->user()->empresa->nombre }}

                                @endif
                            </span>
                            <a href="{{route('logout')}}" class="dud-logout" title="Logout">
                                <i class="feather icon-log-out"></i>
                            </a>
                        </div>
                        <ul class="pro-body">
                            <li><a href="{{route('perfil')}}" class="dropdown-item"><i class="feather icon-user"></i> Perfil</a></li>
{{--                            <li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>--}}
                            <li><a href="{{route('logout')}}" class="dropdown-item"><i class="feather icon-lock"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</header>
