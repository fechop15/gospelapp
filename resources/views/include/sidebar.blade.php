<aside class="main-sidebar hidden-print ">
    <section class="sidebar" id="sidebar-scroll">

        <div class="user-panel">
            <div class="f-left image"><img src="/images/perfil/{{auth()->user()->imagen}}" alt="User Image"
                                           class="img-circle"></div>
            <div class="f-left info">
                <p>
                    @if(auth()->user()->empresa==null)
                        {{ auth()->user()->persona->nombres }}
                    @else
                        {{ auth()->user()->empresa->nombre }}

                    @endif
                </p>
                <p class="designation">
                    {{ auth()->user()->rol->nombre }}
                    <i class="icofont icofont-caret-down m-l-5"></i>
                </p>

            </div>
        </div>
        <!-- sidebar profile Menu-->
        <ul class="nav sidebar-menu extra-profile-list">
            <li>
                <a class="waves-effect waves-dark" href="{{route('perfil')}}">
                    <i class="icon-user"></i>
                    <span class="menu-text">Ver Perfil</span>
                    <span class="selected"></span>
                </a>
            </li>
            {{--            <li>
                            <a class="waves-effect waves-dark" href="javascript:void(0)">
                                <i class="icon-settings"></i>
                                <span class="menu-text">Settings</span>
                                <span class="selected"></span>
                            </a>
                        </li>--}}
            <li>
                <a class="waves-effect waves-dark" href="{{route('logout')}}">
                    <i class="icon-logout"></i>
                    <span class="menu-text">Logout</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">

            <li class="nav-level">Navegacion</li>
            @if(auth()->user()->rol_id >=4 )
                <li class="{{ request()->route()->getName() === 'perfil' || request()->route()->getName() === 'perfil'? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('perfil')}}">
                        <i class="icon-people"></i><span> Mi Perfil</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->rol_id == 1 || auth()->user()->rol_id == 2 )
                <li class="{{ request()->route()->getName() === 'dashboard' ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('dashboard')}}">
                        <i class="icon-speedometer"></i><span> Dashboard</span>
                    </a>
                </li>
                <li class="{{ request()->route()->getName() === 'noticias' ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('noticias')}}">
                        <i class="icon-speedometer"></i><span> Noticias</span>
                    </a>
                </li>
                <li class="{{ request()->route()->getName() === 'pre_inscripciones' ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('pre_inscripciones')}}">
                        <i class="icofont icofont-pencil"></i><span> Pre Inscripciones </span>
                    </a>
                </li>
                <li class="{{ request()->route()->getName() === 'matriculas' ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('matriculas')}}">
                        <i class="icon-calendar"></i><span> Matriculas </span>
                    </a>
                </li>

                <li class="{{ request()->route()->getName() === 'estudiantes'|| request()->route()->getName() === 'perfilCliente' && isset($estudiante) ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('estudiantes')}}">
                        <i class="icon-people"></i><span> Estudiantes</span>
                    </a>
                </li>

                <li class="{{ request()->route()->getName() === 'empresas'|| request()->route()->getName() === 'perfilCliente' && isset($empresa) ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('empresas')}}">
                        <i class="icofont icofont-business-man"></i><span> Empresas</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->rol_id == 1 || auth()->user()->rol_id == 2 || auth()->user()->rol_id == 3 )
                <li class="{{ request()->route()->getName() === 'cursos' || request()->route()->getName() === 'infoCurso'? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('cursos')}}">
                        <i class="icofont icofont-university"></i><span> Cursos</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->rol_id == 1)
                <li class="nav-level">Administrador</li>
                <li class="{{ request()->route()->getName() === 'empleados'? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('empleados')}}">
                        <i class="icon-people"></i><span> Empleados</span>
                    </a>
                </li>
                <li class="{{ request()->route()->getName() === 'certificados'? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('certificados')}}">
                        <i class="icofont icofont-certificate"></i><span> Certificados</span>
                    </a>
                </li>

                {{--                <li class="{{ request()->route()->getName() === 'blank' ? ' active' : '' }} treeview">
                                    <a class="waves-effect waves-dark" href="{{route('blank')}}">
                                        <i class="icon-paper-plane"></i><span> Blank</span>
                                    </a>
                                </li>--}}

            @endif
        </ul>
    </section>
</aside>