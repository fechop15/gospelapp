<!-- Required Jqurey -->
<script src="/plugins/jquery/dist/jquery.min.js"></script>
<script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/plugins/tether/dist/js/tether.min.js"></script>

<!-- Required Fremwork -->
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- waves effects.js -->
<script src="/plugins/waves/waves.min.js"></script>

<!-- Scrollbar JS-->
<script src="/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="/plugins/jquery.nicescroll/jquery.nicescroll.min.js"></script>

<!--classic JS-->
<script src="/plugins/classie/classie.js"></script>

<!-- notification -->
<script src="/plugins/notification/js/bootstrap-growl.min.js"></script>

<!-- Date picker.js -->
<script src="/plugins/datepicker/js/moment-with-locales.min.js"></script>
<script src="/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Select 2 js -->
<script src="/plugins/select2/dist/js/select2.full.min.js"></script>

<!-- Max-Length js -->
<script src="/plugins/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>

<!-- Multi Select js -->
<script src="/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
<script src="/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="/plugins/multi-select/js/jquery.quicksearch.js"></script>

<!-- Tags js -->
<script src="/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>

<!-- Bootstrap Datepicker js -->
<script type="text/javascript" src="/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- bootstrap range picker -->
<script type="text/javascript" src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- color picker -->
<script type="text/javascript" src="/plugins/spectrum/spectrum.js"></script>
<script type="text/javascript" src="/plugins/jscolor/jscolor.js"></script>

<!-- highlite js -->
<script type="text/javascript" src="/plugins/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="/plugins/syntaxhighlighter/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="/plugins/syntaxhighlighter/scripts/shBrushXml.js"></script>
<script type="text/javascript">SyntaxHighlighter.all();</script>

<!-- datatable js -->
<script type="text/javascript" src="/plugins/datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/plugins/datatable/js/dataTables.bootstrap.js"></script>


<!-- custom js -->
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/pages/profile.js"></script>
<script type="text/javascript" src="/pages/elements.js"></script>
<script type="text/javascript" src="/js/menu.min.js"></script>
<script type="text/javascript" src="/pages/advance-form.js"></script>
<script src="/js/menu.min.js"></script>