<title>GOSPELAPP</title>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Favicon icon -->
<link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon">
<link rel="icon" href="/images/favicon.ico" type="image/x-icon">

<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<!-- Font Awesome -->
<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- iconfont -->
<link rel="stylesheet" type="text/css" href="/icon/icofont/css/icofont.css">

<!-- simple line icon -->
<link rel="stylesheet" type="text/css" href="/icon/simple-line-icons/css/simple-line-icons.css">

<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="/plugins/bootstrap/css/bootstrap.min.css">

<!-- Date Picker css -->
<link rel="stylesheet"
      href="/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"/>

<!-- Bootstrap Date-Picker css -->
<link rel="stylesheet" href="/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-daterangepicker/daterangepicker.css"/>

<!-- Select 2 css -->
<link rel="stylesheet" href="/plugins/select2/dist/css/select2.min.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/select2/css/s2-docs.css">

<!-- Multi Select css -->
<link rel="stylesheet" href="/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css"/>
<link rel="stylesheet" href="/plugins/multiselect/css/multi-select.css"/>

<!-- Color Picker css -->
<link rel="stylesheet" href="/plugins/spectrum/spectrum.css"/>

<!-- Tags css -->
<link rel="stylesheet" href="/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"/>

<!-- bash syntaxhighlighter css -->
<link type="text/css" rel="stylesheet" href="/plugins/syntaxhighlighter/styles/shCoreDjango.css"/>

<!-- datatable css -->
<link type="text/css" rel="stylesheet" href="/plugins/datatable/css/jquery.dataTables.css"/>

<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="/css/main.css">

<!-- Responsive.css-->
<link rel="stylesheet" type="text/css" href="/css/responsive.css">

<!--color css-->
<link rel="stylesheet" type="text/css" href="/css/color/color-6.css" id="color"/>

@if(request()->route()->getName() != '')

    <link rel="stylesheet" type="text/css" href="/icon/simple-line-icons/css/simple-line-icons.css">
    <link href="/css/svg-weather.css" rel="stylesheet">
    <script src="/plugins/charts/echarts/js/echarts-all.js"></script>

@endif

