<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro De Empleado</h5>
            </div>

            <div class="modal-body">
                <form id="guardarUsuario">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-form-label">Nombres:</label>
                                <input type="text" class="form-control" id="nombres">
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label">Apellidos:</label>
                                <input type="text" class="form-control" id="apellidos">
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label">Telefono:</label>
                                <input type="number" class="form-control" id="telefono">
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-form-label">Tipo Documento:</label>
                                <select class="form-control" id="tipo_documento">
                                    <option value="CC">CC</option>
                                    <option value="TI">TI</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label">Documento:</label>
                                <input type="number" class="form-control" id="documento">
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Ocupacion:</label>
                                <input type="text" class="form-control" id="ocupacion">
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label class="col-form-label">Correo:</label>
                                <input type="email" class="form-control" id="email">
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Contraseña:</label>
                                <input type="text" class="form-control" id="password">
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label">Fecha Nacimiento:</label>
                                <input type="date" class="form-control" id="fecha_nacimiento"
                                       style="padding-bottom: 13px;">
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label class="col-form-label">Sexo:</label>
                                <select class="form-control" id="sexo">
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </div>


                            <div class="col-md-4">
                                <label class="col-form-label">Grupo Sanguineo:</label>
                                <input type="text" class="form-control" id="tipo_sangre">
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label class="col-form-label">Departamento:</label>

                                <select class="select2-hidden-accessible" id="select-municipio"
                                        name="state" style="width: 100%">
                                </select>

                            </div>

                            <div class="col-md-4" style="padding-bottom: 13px;">
                                <label class="col-form-label">Ciudad:</label>

                                <select class="select2-hidden-accessible" id="select-ciudad"
                                        name="city" style="width: 100%;">
                                </select>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-form-label">Rol:</label>
                                <select class="form-control" id="rol">
                                    <option value="2">Empleado</option>
                                    <option value="3">Entrenador</option>
                                    <option value="5">Estudiante</option>
                                    <option value="1">Administrador</option>
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="alert alert-danger" id="error" style="display: none">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Empleado</h5>
            </div>

            <div class="modal-body">

                <form id="actualizarUsuario">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-form-label">Nombres:</label>
                                <input type="text" class="form-control" id="Anombres">
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label">Apellidos:</label>
                                <input type="text" class="form-control" id="Aapellidos">
                            </div>
                            <div class="col-md-4">
                                <label class="col-form-label">Telefono:</label>
                                <input type="number" class="form-control" id="Atelefono">
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label class="col-form-label">Tipo Documento:</label>
                                <select class="form-control" id="Atipo_documento">
                                    <option value="CC">CC</option>
                                    <option value="TI">TI</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Documento:</label>
                                <input type="number" class="form-control" id="Adocumento">
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Ocupacion:</label>
                                <input type="text" class="form-control" id="Aocupacion">
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label class="col-form-label">Correo:</label>
                                <input type="email" class="form-control" id="Aemail">
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Estado:</label>
                                <select class="form-control" id="estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Fecha Nacimiento:</label>
                                <input type="date" class="form-control" id="Afecha_nacimiento"
                                       style="padding-bottom: 13px;">
                            </div>

                        </div>

                        <div class="row">


                            <div class="col-md-4">
                                <label class="col-form-label">Sexo:</label>
                                <select class="form-control" id="Asexo">
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="col-form-label">Grupo Sanguineo:</label>
                                <input type="text" class="form-control" id="Atipo_sangre">
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-form-label">Departamento:</label>

                                <select class="select2-hidden-accessible" id="Aselect-municipio"
                                        name="state" style="width: 100%">
                                </select>

                            </div>

                            <div class="col-md-4" style="padding-bottom: 13px;">
                                <label class="col-form-label">Ciudad:</label>

                                <select class="select2-hidden-accessible" id="Aselect-ciudad"
                                        name="city" style="width: 100%">
                                </select>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-form-label">Rol:</label>
                                <select class="form-control" id="Arol">
                                    <option value="2">Empleado</option>
                                    <option value="3">Entrenador</option>
                                    <option value="5">Estudiante</option>
                                    <option value="1">Administrador</option>
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="alert alert-danger" id="Aerror" style="display: none">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Cambio de contraseña</h5>
            </div>

            <div class="modal-body">

                <form id="form-password">

                    <div class="form-group">
                        <label class="col-form-label">Contraseña nueva:</label>
                        <input type="text" class="form-control" id="pass">
                    </div>


                    <div class="form-group">
                        <div class="alert alert-danger" id="Perror" style="display: none">
                        </div>
                    </div>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="cambiarPass()">Cambiar
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>

@endsection