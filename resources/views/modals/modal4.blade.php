<div class="modal fade" id="modal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Nueva Nota</h5>
            </div>

            <div class="modal-body p-5">
                <label class="form-control-label">Titulo </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="titulo" placeholder="Titulo De la nota">
                </div>

                <label class="form-control-label">Descripcion </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="descripcion" placeholder="Descripcion">
                </div>

                <label id="error" class="has-error"></label>

            </div>

            <div class="modal-footer border-0">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Nueva Nota</h5>
            </div>

            <div class="modal-body p-5">
                <label class="form-control-label">Titulo </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="Atitulo" placeholder="Titulo De la nota">
                </div>

                <label class="form-control-label">Descripcion </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="Adescripcion" placeholder="Descripcion">
                </div>

                <label id="Aerror" class="has-error"></label>

            </div>

            <div class="modal-footer border-0">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>