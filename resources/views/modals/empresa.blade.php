<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro De Empresa</h5>
            </div>

            <div class="modal-body">
                <form id="guardarUsuario">

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Nombre Empresa:</label>
                            <input type="text" class="form-control" id="empresa">
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Nit:</label>
                            <input type="text" class="form-control" id="nit">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Contacto:</label>
                            <input type="text" class="form-control" id="contacto">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Telefono:</label>
                            <input type="number" class="form-control" id="telefono">
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Correo:</label>
                            <input type="email" class="form-control" id="email">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Contraseña:</label>
                            <input type="text" class="form-control" id="password">
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-form-label">Sector:</label>
                        <select class="form-control" id="sector">
                            <option value="Electrico">Electrico</option>
                            <option value="Telecomunicaciones">Telecomunicaciones</option>
                            <option value="Hidrocarburos">Hidrocarburos</option>
                            <option value="Construccion">Construccion</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-danger" id="error" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Empresa</h5>
            </div>

            <div class="modal-body">

                <form id="actualizarUsuario">

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Nombre Empresa:</label>
                            <input type="text" class="form-control" id="Aempresa">
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Nit:</label>
                            <input type="text" class="form-control" id="Anit">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Contacto:</label>
                            <input type="text" class="form-control" id="Acontacto">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Telefono:</label>
                            <input type="number" class="form-control" id="Atelefono">
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Correo:</label>
                            <input type="email" class="form-control" id="Aemail">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Estado:</label>
                            <select class="form-control" id="estado">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Sector:</label>
                        <select class="form-control" id="Asector">
                            <option value="Electrico">Electrico</option>
                            <option value="Telecomunicaciones">Telecomunicaciones</option>
                            <option value="Hidrocarburos">Hidrocarburos</option>
                            <option value="Construccion">Construccion</option>
                        </select>
                    </div>


                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Aerror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Cambio de contraseña</h5>
            </div>

            <div class="modal-body">

                <form id="form-password">

                    <div class="form-group">
                        <label class="col-form-label">Contraseña nueva:</label>
                        <input type="text" class="form-control" id="pass">
                    </div>


                    <div class="form-group">
                        <div class="alert alert-danger" id="Perror" style="display: none">
                        </div>
                    </div>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="cambiarPass()">Cambiar
                </button>
            </div>
        </div>
    </div>
</div>


@section('modaljs')
    <script>

    </script>

@endsection