<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registrar Preinscripcion</h5>
            </div>

            <div class="modal-body">
                <form class="md-float-material" id="form-preinscripcion">

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="col-form-label">Nombres:</label>
                            <input type="text" class="form-control" id="nombres" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Apellidos:</label>
                            <input type="text" class="form-control" id="apellidos" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Tipo Documento:</label>
                            <select class="form-control" id="tipo_documento">
                                <option value="CC">CC</option>
                                <option value="TI">TI</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Documento:</label>
                            <input type="number" class="form-control" id="documento" autocomplete="off">
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Cargo/Ocupacion:</label>
                            <input type="text" class="form-control" id="cargo" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Telefono:</label>
                            <input type="number" class="form-control" id="telefono" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Correo:</label>
                            <input type="email" class="form-control" id="email" autocomplete="off">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Curso:</label>
                            <select class="form-control" id="curso" autocomplete="off">
                                <option value="Trabajo En Altura Avanzado">Trabajo Seguro En Altura Avanzado</option>
                                <option value="Trabajo En Altura Reentrenamiento">Trabajo Seguro En Altura
                                    Reentrenamiento
                                </option>
                                <option value="Trabajo En Altura Basico Operativo">Trabajo Seguro En Altura Basico
                                    Operativo
                                </option>
                                <option value="Trabajo En Altura Basico Operativo">Trabajo Seguro En Altura Coordinador
                                </option>
                                <option value="Trabajo Seguro En Altura Jefe Area">Trabajo Seguro En Altura Jefe Area
                                </option>
                                <option value="Rescate Vertical">Rescate Vertical</option>
                                <option value="Otro">Otro</option>
                            </select>
                            <input type="text" placeholder="Cual??" class="form-control" id="cual" autocomplete="off"
                                   style="display: none">

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" id="error" style="display: none">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-footer"> -->
                    <!-- </div> -->
                </form>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light"
                        onclick="guardarPreinscripcion()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro Preinscripcion Eliminar</h5>
            </div>

            <div class="modal-body">
                <form id="guardarMatriculaR">

                    <div class="form-group">
                        <h3>Realmente desea eliminar esta preinscripcion?</h3>
                    </div>

                    <label class="has-error"></label>
                </form>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="terminarPrematricula()">
                    Eliminar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-descargable" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Descarga de Certificado</h5>
            </div>

            <div class="modal-body">
                <form id="guardarMatriculaR">

                    <div class="form-group">
                        <h3>Desea Permitir la descarga de este certificado?</h3>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <select class="form-control" id="descargable">
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>

                    <label class="has-error"></label>
                </form>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="SendDescargable()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>

@endsection