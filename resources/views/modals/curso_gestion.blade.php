<div class="modal fade" id="modal-calificacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">Registro de notas</h5>
            </div>

            <div class="modal-body">
                <form id="calificacion">
                    <label class="form-control-label">Nota Teorica (40%): </label>
                    <div class="input-group">
                    <span class="input-group-addon">
                        <i class="icofont icofont-user"></i>
                    </span>
                        <input type="number" class="form-control" id="nota_teorica"
                               placeholder="ingrese la nota teorica de 0 a 100" max="100" autocomplete="off"
                               onkeyup="calcularNota()">
                    </div>

                    <label class="form-control-label">Nota Practica (60%): </label>
                    <div class="input-group">
                    <span class="input-group-addon">
                        <i class="icofont icofont-user"></i>
                    </span>
                        <input type="number" class="form-control" id="nota_practica"
                               placeholder="ingrese la nota practica de 0 a 100" max="100" autocomplete="off"
                               onkeyup="calcularNota()">
                    </div>
                    <label class="form-control-label">El estudiante debe tener una calificacion promedio superior al 70%
                        para aprobar el curso y generar su certificado</label>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" id="Cerror" style="display: none">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <h4 class="modal-title text-center">Nota Final:<strong id="notaFinal">0</strong>%</h4>

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="calificar()">
                    Calificar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-asistencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Confirmacion de asistencia</h5>
            </div>

            <div class="modal-body">
                <form id="asistencia">
                    <label class="form-control-label">
                        Al Confirmar esta asistencia esta indicando que el estudiante asistio las veces requeridas para
                        aprobar este curso
                    </label>
                    <select id="asistio" class="form-control">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>

                    <label class="form-control-label">
                        OJO. una vez confirmado esta asistencia no se podra cambiar.
                    </label>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Aerror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="confirmarAsistencia()">
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-certificado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Confirmacion de asistencia</h5>
            </div>

            <div class="modal-body">
                <form id="certificado">
                    <label class="form-control-label">
                        Realmente Desea Generar el Certificado.. <br>
                        Este solo puede ser descargado por el estudiante si se encuentra a paz y salvo y
                        ha cumplido los requisitos requeridos por la entidad
                    </label>
                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Ceerror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="certificar()">
                    Generar Certificado
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-reportar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Confirmacion de Reporte Al Ministerio</h5>
            </div>

            <div class="modal-body">
                <form id="certificado">
                    <label class="form-control-label">
                        Realmente desea reportar este certificado al ministerio.. <br>
                    </label>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" id="Merror" style="display: none">
                            </div>
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="reportar()">
                    Reportar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Curso</h5>
            </div>

            <div class="modal-body">

                <form id="actualizarUsuario">
                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Nombre Completo:</label>
                            <input type="text" disabled class="form-control" id="Anombre">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Documento:</label>
                            <input type="text" disabled class="form-control" id="Adocumento">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Telefono:</label>
                            <input type="text" disabled class="form-control" id="Atelefono">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Correo:</label>
                            <input type="text" disabled class="form-control" id="Acorreo">
                        </div>

                    </div>

                    <label class="col-form-label">Fecha del curso</label>
                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Fecha Inicio:</label>
                            <input type="date" disabled class="form-control" id="AfechaInicio">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Fecha Fin:</label>
                            <input type="date" disabled class="form-control" id="AfechaFin">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Curso:</label>
                        <input type="text" disabled class="form-control" id="Acurso">
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Entrego Documentacion:</label>
                            <select class="form-control" id="Adocumentado">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Pago Inscripcion?:</label>
                            <select class="form-control" id="Apago">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Estado:</label>
                        <select class="form-control" id="estado">
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Aerror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>
