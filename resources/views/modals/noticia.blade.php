<div class="modal fade" id="modal-1" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro De Noticia</h5>
            </div>
            <form id="guardarUsuario">
                <div class="modal-body">

                    <input type="hidden" id="idNoticia">

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="col-form-label">Titulo:</label>
                                <input type="text" class="form-control" id="titulo">
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label">Tipo:</label>
                                <select class="form-control" id="tipoNoticia">
                                    <option value="Predica">Predica</option>
                                    <option value="Evento">Evento</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="col-form-label">Estado:</label>
                                <select class="form-control" id="estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group" id="editor">

                            <div class="col-md-12">
                             <textarea id='edit' style="margin-top: 30px;" placeholder="Contenido de la noticia">

                              </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="alert alert-danger" id="error" style="display: none">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary waves-effect waves-light" value="Guardar" id="guardar">
                    <input type="submit" class="btn btn-primary waves-effect waves-light" value="Actualizar" id="actualizar">
                </div>
            </form>
        </div>
    </div>
</div>
@section('modaljs')
    <script>

    </script>

@endsection