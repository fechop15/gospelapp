<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Inscripcion a curso</h5>
            </div>

            <div class="modal-body">
                <form id="guardarUsuario">

                    <div class="form-group">
                        <label class="col-form-label">Estudiante:</label>
                        <select class="form-control" id="select-persona" style="width: 100%">
                        </select>

                        <label class="col-form-label">Curso:</label>
                        <select class="form-control" id="select-curso" style="width: 100%">
                        </select>

                        <label class="col-form-label">Entrego Documentacion:</label>
                        <select class="form-control" id="documentado">
                            <option value="0">No</option>
                            <option value="1">Si</option>
                        </select>

                        <label class="col-form-label">Pago Inscripcion?:</label>
                        <select class="form-control" id="pago">
                            <option value="0">No</option>
                            <option value="1">Si</option>
                        </select>
                    </div>


                    <div class="col-md-12">
                        <div class="alert alert-danger" id="error" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Matricula</h5>
            </div>

            <div class="modal-body">

                <form id="actualizarUsuario">
                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Nombre Completo:</label>
                            <input type="text" disabled class="form-control" id="Anombre">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Documento:</label>
                            <input type="text" disabled class="form-control" id="Adocumento">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Telefono:</label>
                            <input type="text" disabled class="form-control" id="Atelefono">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Correo:</label>
                            <input type="text" disabled class="form-control" id="Acorreo">
                        </div>

                    </div>

                    <label class="col-form-label">Fecha del curso</label>
                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Fecha Inicio:</label>
                            <input type="date" disabled class="form-control" id="AfechaInicio">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Fecha Fin:</label>
                            <input type="date" disabled class="form-control" id="AfechaFin">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Curso:</label>
                        <input type="text" disabled class="form-control" id="Acurso">
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Entrego Documentacion:</label>
                            <select class="form-control" id="Adocumentado">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Pago Inscripcion?:</label>
                            <select class="form-control" id="Apago">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Estado:</label>
                        <select class="form-control" id="estado">
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Aerror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-informacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Informacion de matricula</h5>
            </div>

            <div class="modal-body">

                <form id="actualizarUsuario">

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Nombre Completo:</label>
                            <input type="text" disabled class="form-control" id="Inombre">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Documento:</label>
                            <input type="text" disabled class="form-control" id="Idocumento">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Telefono:</label>
                            <input type="text" disabled class="form-control" id="Itelefono">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Correo:</label>
                            <input type="text" disabled class="form-control" id="Icorreo">
                        </div>

                    </div>

                    <label class="col-form-label">Fecha del curso</label>
                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Fecha Inicio:</label>
                            <input type="date" disabled class="form-control" id="IfechaInicio">
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Fecha Fin:</label>
                            <input type="date" disabled class="form-control" id="IfechaFin">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Curso:</label>
                        <input type="text" disabled class="form-control" id="Icurso">
                        </select>
                    </div>

                    <div class="form-group">

                        <div class="col-md-6">
                            <label class="col-form-label">Entrego Documentacion:</label>
                            <select class="form-control" disabled id="Idocumentado">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                        <div class="col-md-6">
                            <label class="col-form-label">Pago Inscripcion?:</label>
                            <select class="form-control" disabled id="Ipago">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-form-label">Estado:</label>
                        <select class="form-control" disabled id="Iestado">
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Ierror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>

@endsection