<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro De Curso</h5>
            </div>

            <div class="modal-body">
                <form id="guardarUsuario">

                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="col-form-label">Nombre Curso:</label>
                            <input type="text" class="form-control" id="nombre">
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Nivel:</label>
                            <input type="text" class="form-control" id="nivel">
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Intensidad Horaria:</label>
                            <input type="number" class="form-control" id="intensidad">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Cupos:</label>
                            <input type="number" class="form-control" id="cupos">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Inicio Inscripcion:</label>
                            <input type="date" class="form-control" id="inscripcionFechaInicio"
                                   style="padding-bottom: 13px;">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Fin Inscripcion:</label>
                            <input type="date" class="form-control" id="inscripcionFechaFin"
                                   style="padding-bottom: 13px;">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Inicio curso:</label>
                            <input type="date" class="form-control" id="fechaInicio" style="padding-bottom: 13px;">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Fin curso:</label>
                            <input type="date" class="form-control" id="fechaFin" style="padding-bottom: 13px;">
                        </div>

                        <div class="col-md-4" style="padding-bottom: 13px;">
                            <label class="col-form-label">Entrenador:</label>
                            <select class="form-control" id="select-entrenador" style="width: 100%">
                            </select>
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Ciudad:</label>
                            <select class="form-control" id="ciudad">
                                <option value="Monteria">Montería</option>
                                <option value="Sincelejo">Sincelejo</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Dias</label>
                            <input type="text" class="form-control" id="dias" placeholder="1,2,3,4">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Supervisor:</label>
                            <input type="text" class="form-control" id="supervisor" placeholder="Nombre Supervisor">
                        </div>

                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-form-label">Certificado(PDF):</label>
                                <input type="file" class="form-control" id="certificado" name="certificado"
                                       accept="application/pdf">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="alert alert-danger" id="error" style="display: none">
                        </div>
                    </div>

                    <label class="has-error"></label>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" id="guardar" class="btn btn-primary waves-effect waves-light" onclick="guardar()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Curso</h5>
            </div>

            <div class="modal-body">

                <form id="actualizarUsuario">

                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="col-form-label">Nombre Curso:</label>
                            <input type="text" class="form-control" id="Anombre">
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Nivel:</label>
                            <input type="text" class="form-control" id="Anivel">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Intensidad Horaria:</label>
                            <input type="number" class="form-control" id="Aintensidad">
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Cupos:</label>
                            <input type="number" class="form-control" id="Acupos">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Inicio Inscripcion:</label>
                            <input type="date" class="form-control" id="AinscripcionFechaInicio"
                                   style="padding-bottom: 13px;">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Fin Inscripcion:</label>
                            <input type="date" class="form-control" id="AinscripcionFechaFin"
                                   style="padding-bottom: 13px;">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Inicio Curso:</label>
                            <input type="date" class="form-control" id="AfechaInicio" style="padding-bottom: 13px;">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Fecha Fin Curso:</label>
                            <input type="date" class="form-control" id="AfechaFin" style="padding-bottom: 13px;">
                        </div>
                        <div class="col-md-4" style="padding-bottom: 13px;">
                            <label class="col-form-label">Entrenador:</label>
                            <select class="form-control" id="Aselect-entrenador" style="width: 100%">
                            </select>
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Ciudad:</label>
                            <select class="form-control" id="Aciudad">
                                <option value="Monteria">Montería</option>
                                <option value="Sincelejo">Sincelejo</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Dias</label>
                            <input type="text" class="form-control" id="Adias" placeholder="1,2,3,4">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Supervisor:</label>
                            <input type="text" class="form-control" id="Asupervisor">
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">

                                <label class="col-form-label">Certificado(PDF):</label>
                                <input type="file" class="form-control" id="Acertificado" name="Acertificado"
                                       accept="application/pdf">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Estado:</label>
                                <select class="form-control" id="estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" id="Aerror" style="display: none">
                            </div>
                        </div>
                    </div>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>

@endsection