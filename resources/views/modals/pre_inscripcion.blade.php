<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro Preinscripcion</h5>
            </div>

            <div class="modal-body">
                <form id="guardarMatricula">

                    <div class="row">

                        <div class="col-md-4">
                            <label class="col-form-label">Nombres:</label>
                            <input type="text" class="form-control" id="nombres">
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Apellidos:</label>
                            <input type="text" class="form-control" id="apellidos">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Documento:</label>
                            <input type="number" class="form-control" id="documento">
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label class="col-form-label">Ocupacion:</label>
                            <input type="text" class="form-control" id="ocupacion">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Telefono:</label>
                            <input type="number" class="form-control" id="telefono">
                        </div>

                        <div class="col-md-4" style="padding-bottom: 5px;">
                            <label class="col-form-label">Fecha Nacimiento:</label>
                            <input type="date" class="form-control" id="fecha_nacimiento">
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label class="col-form-label">Sexo:</label>
                            <select class="form-control" id="sexo">
                                <option value="Masculino">Masculino</option>
                                <option value="Femenino">Femenino</option>
                                <option value="Otro">Otro</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Correo:</label>
                            <input type="email" class="form-control" id="email">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Contraseña:</label>
                            <input type="text" class="form-control" id="password">
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-3">
                            <label class="col-form-label">Departamento:</label>
                            <select class="select2-hidden-accessible" id="select-municipio"
                                    name="state" style="width: 100%">
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label class="col-form-label">Ciudad:</label>
                            <select class="select2-hidden-accessible" id="select-ciudad"
                                    name="city" style="width: 100%">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="col-form-label">Entrego Documentacion:</label>
                            <select class="form-control" id="documentado">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="col-form-label">Pago Inscripcion?:</label>
                            <select class="form-control" id="pago">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="col-form-label">Empresa:</label>
                            <select class="select2-hidden-accessible" id="select-empresa"
                                    name="empresa" style="width: 100%">
                            </select>
                        </div>
                    </div>

                    <label class="col-form-label" id="cursoInteresado"></label>

                    <div class="form-group">
                        <label class="col-form-label">Curso:</label>
                        <select class="form-control" id="select-curso" style="width: 100%">
                        </select>
                    </div>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="error" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro Preinscripcion</h5>
            </div>

            <div class="modal-body">
                <form id="guardarMatriculaR">

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Nombres:</label>
                            <input type="text" class="form-control" id="Rnombres">
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Apellidos:</label>
                            <input type="text" class="form-control" id="Rapellidos">
                        </div>

                        <div class="col-md-4">
                            <label class="col-form-label">Documento:</label>
                            <input type="number" class="form-control" id="Rdocumento">
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-4">
                            <label class="col-form-label">Entrego Documentacion:</label>
                            <select class="form-control" id="Rdocumentado">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="col-form-label">Pago Inscripcion?:</label>
                            <select class="form-control" id="Rpago">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>
                        <div class="col-md-4" style="padding-bottom: 13px;">
                            <label class="col-form-label">Empresa:</label>
                            <select class="select2-hidden-accessible" id="Rselect-empresa"
                                    name="empresa" style="width: 100%">
                            </select>
                        </div>
                    </div>

                    <label class="col-form-label" id="RcursoInteresado"></label>

                    <div class="form-group">
                        <label class="col-form-label">Curso:</label>
                        <select class="form-control" id="Rselect-curso" style="width: 100%">
                        </select>
                    </div>

                    <div class="col-md-12">
                        <div class="alert alert-danger" id="Rerror" style="display: none">
                        </div>
                    </div>
                    <label class="has-error"></label>
                </form>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro Preinscripcion Eliminar</h5>
            </div>

            <div class="modal-body">
                <form id="guardarMatriculaR">

                    <div class="form-group">
                        <h3>Realmente desea eliminar esta preinscripcion?</h3>
                    </div>

                    <label class="has-error"></label>
                </form>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="terminarPrematricula()">
                    Eliminar
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>

    </script>

@endsection