<!doctype html>
<html lang="en">
<head>
@include('include.head')
<body>
@yield('page')

@include('include.script_login')
@yield('js')

</body>
</html>
