@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>{{$estudiante->persona->nombres}} {{$estudiante->persona->apellidos}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('estudiantes')}}">Estudiantes</a>
                        </li>
                        <li class="breadcrumb-item">Perfil
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Header end -->
            <div class="row">
                <!-- start col-lg-9 -->
                <div class="col-xl-12 col-lg-12">
                    <!-- Nav tabs -->
                    <div class="tab-header">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Informacion
                                    Personal</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#project" role="tab">Mis Cursos</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#questions" role="tab">Cuenta</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#members" role="tab">Cursos</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Sobre Mi</h5>

                                    <button id="edit-btn" type="button"
                                            class="btn btn-primary waves-effect waves-light f-right">
                                        <i class="icofont icofont-edit"></i>
                                    </button>
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombres</th>
                                                                    <td>{{$estudiante->persona->nombres}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Apellidos</th>
                                                                    <td>{{$estudiante->persona->apellidos}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Identificacion</th>
                                                                    <td>{{$estudiante->persona->documento}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Telefono</th>
                                                                    <td>{{$estudiante->persona->telefono}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Fecha Nacimiento</th>
                                                                    <td>{{$estudiante->persona->fecha_nacimiento}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Empresa</th>
                                                                    <td>{{ $estudiante->persona->empresa==null?'Ninguna':$estudiante->persona->empresa->nombre}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Ciudad</th>
                                                                    <td>{{$estudiante->persona->city->name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Correo</th>
                                                                    <td>{{$estudiante->email}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Ocupacion</th>
                                                                    <td> {{$estudiante->persona->ocupacion}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Sexo</th>
                                                                    <td>{{$estudiante->persona->sexo}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Grupo Sanguineo</th>
                                                                    <td>{{ $estudiante->persona->tipo_sangre}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                    <div class="edit-info" style="">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <form id="guardarUsuario">
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Nombres:</label>
                                                                            <input type="text" class="form-control"
                                                                                   value="{{$estudiante->persona->nombres}}"
                                                                                   id="nombres">
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Apellidos:</label>
                                                                            <input type="text" class="form-control"
                                                                                   value="{{$estudiante->persona->apellidos}}"
                                                                                   id="apellidos">
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Telefono:</label>
                                                                            <input type="number" class="form-control"
                                                                                   value="{{$estudiante->persona->telefono}}"
                                                                                   id="telefono">
                                                                        </div>


                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Tipo
                                                                                Documento:</label>
                                                                            <select class="form-control"
                                                                                    id="tipo_documento">
                                                                                <option value="CC">CC</option>
                                                                                <option value="TI">TI</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Documento:</label>
                                                                            <input type="number" class="form-control"
                                                                                   disabled
                                                                                   value="{{$estudiante->persona->documento}}"
                                                                                   id="documento">
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Ocupacion:</label>
                                                                            <input type="text" class="form-control"
                                                                                   value="{{$estudiante->persona->ocupacion}}"
                                                                                   id="ocupacion">
                                                                        </div>

                                                                    </div>

                                                                    <div class=row>

                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Correo:</label>
                                                                            <input type="email" class="form-control"
                                                                                   value="{{$estudiante->email}}"
                                                                                   id="email">
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Fecha
                                                                                Nacimiento:</label>
                                                                            <input type="date" class="form-control"
                                                                                   id="fecha_nacimiento"
                                                                                   value="{{$estudiante->persona->fecha_nacimiento}}"
                                                                                   style="padding-bottom: 13px;">
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Grupo
                                                                                Sanguineo:</label>
                                                                            <input type="text" class="form-control"
                                                                                   value="{{$estudiante->persona->tipo_sangre}}"
                                                                                   id="tipo_sangre">
                                                                        </div>

                                                                    </div>

                                                                    <div class="row">

                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Sexo:</label>
                                                                            <select class="form-control" id="sexo">
                                                                                <option value="Masculino">Masculino
                                                                                </option>
                                                                                <option value="Femenino">Femenino
                                                                                </option>
                                                                                <option value="Otro">Otro</option>
                                                                            </select>
                                                                        </div>


                                                                        <div class="col-md-4">
                                                                            <label class="col-form-label">Departamento:</label>
                                                                            <select class="select2-hidden-accessible"
                                                                                    id="select-municipio"
                                                                                    name="state" style="width: 100%">
                                                                            </select>
                                                                        </div>

                                                                        <div class="col-md-4"
                                                                             style="padding-bottom: 13px;">
                                                                            <label class="col-form-label">Ciudad:</label>
                                                                            <select class="select2-hidden-accessible"
                                                                                    id="select-ciudad"
                                                                                    name="city" style="width: 100%">
                                                                            </select>
                                                                        </div>

                                                                    </div>


                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="alert alert-danger" id="error"
                                                                                 style="display: flex">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->


                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                    <div class="text-center">
                                                        <a href="#!"
                                                           onclick="actualizarUsuario()"
                                                           class="btn btn-primary waves-effect waves-light m-r-20">Actualizar</a>
                                                        <a href="#!" id="edit-cancel"
                                                           class="btn btn-default waves-effect">Cancelar</a>
                                                    </div>
                                                </div>
                                                <!-- end of edit info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->

                                    </div>

                                </div>
                                <!-- end of card-block -->

                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Cursos</h5>
                                </div>
                                <!-- end of card-header  -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="project-table">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>

                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Curso</th>
                                                        <th class="text-center txt-primary">Fecha</th>
                                                        <th class="text-center txt-primary">Dias</th>
                                                        <th class="text-center txt-primary">Pago</th>
                                                        <th class="text-center txt-primary">Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                    @foreach($estudiante->persona->matriculas->reverse() as $matricula)
                                                        @if($matricula->estado)
                                                            <tr>
                                                                <td>{{$matricula->curso->nombre}} {{$matricula->curso->nivel}}</td>
                                                                <td>{{$matricula->curso->fecha_inicio_curso}}
                                                                    a {{$matricula->curso->fecha_fin_curso}}</td>
                                                                <td>{{$matricula->curso->dias}}</td>

                                                                <td class="text-center">
                                                                    @if($matricula->pago)
                                                                        <span class="label label-success m-t-20">Pago</span>
                                                                    @else
                                                                        <span class="label label-warning m-t-20">Pendiente</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">
                                                                    @if($matricula->pago==1 && $matricula->documentado==1 &&
                                                                        $matricula->fecha_certificacion!=null)
                                                                        @if($matricula->descargable)
                                                                            <button type="button"
                                                                                    class="btn btn-success waves-effect waves-light"
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="Descargar Certificado"
                                                                                    data-original-title="Edit"
                                                                                    onclick="descargar('{{$matricula->id}}')">
                                                                                <i class="icofont icofont-download"></i>
                                                                            </button>
                                                                        @else
                                                                            <span class="label label-success m-t-20"
                                                                                  data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Su empresa de trabajo no le permite descargar el certificado"
                                                                            >Terminado</span>
                                                                        @endif

                                                                    @else
                                                                        <span class="label label-info m-t-20">En Proceso ..</span>
                                                                    @endif
                                                                    @if(auth()->user()->rol_id<=3)
                                                                        <button type="button"
                                                                                class="btn btn-warning waves-effect waves-light"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Ver Curso"
                                                                                data-original-title="Edit"
                                                                                onclick="verCurso('{{$matricula->curso->id}}')">
                                                                            <i class="icofont icofont-eye"></i>
                                                                        </button>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                            <!-- end of table responsive -->
                                        </div>
                                        <!-- end of project table -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">
                            @if(auth()->user()->id==$estudiante->id)
                                <section class="panels-wells">

                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Cuenta</h5>
                                        </div>
                                        <!-- end of card-header  -->
                                        <div class="card-block">

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h6 class="card-header-text">Cambio de Contraseña</h6>

                                                    <form id="form-password">

                                                        <div class="form-group">
                                                            <label class="col-form-label">Contraseña Actual:</label>
                                                            <input type="text" class="form-control" id="passOld">

                                                            <label class="col-form-label">Contraseña nueva:</label>
                                                            <input type="text" class="form-control" id="pass">

                                                            <label class="col-form-label">Contraseña nueva
                                                                (Confirmacion):</label>
                                                            <input type="text" class="form-control" id="pass2">
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="alert alert-danger" id="Perror"
                                                                 style="display: none">
                                                            </div>
                                                        </div>

                                                    </form>
                                                    <button type="button"
                                                            class="btn btn-primary waves-effect waves-light"
                                                            onclick="cambiarPass()">Confirmar
                                                    </button>


                                                    <!-- end of project table -->
                                                </div>
                                                <div class="col-sm-4">
                                                    <h6>IMAGEN DE PERFIL</h6>
                                                    <div class="card">
                                                        <div class="social-profile">
                                                            <img id="perfil" class="img-fluid width-100"
                                                                 src="/images/perfil/{{auth()->user()->imagen}}" alt="">
                                                            <div class="profile-hvr m-t-15">
                                                                <i class="icofont icofont-ui-edit p-r-10"
                                                                   id="editar"></i>
                                                                <i class="icofont icofont-ui-delete" id="eliminar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <form id="subirImagen">
                                                        <input type="file" id="avatar" name="avatar"
                                                               style="display: none" accept="image/*">
                                                    </form>
                                                    <!-- end of project table -->
                                                </div>
                                                <div class="col-sm-4"></div>
                                                <!-- end of col-lg-12 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>
                                    </div>

                                </section>
                            @endif
                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                            <section class="panels-wells">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text">Cursos Disponibles</h5>
                                    </div>
                                    <!-- end of card-header  -->
                                    <div class="card-block">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="project-table">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover" id="tablaCursos" width="100%">
                                                            <thead>

                                                            <tr>
                                                                <th class="text-center txt-primary pro-pic">Id</th>
                                                                <th class="text-center txt-primary">Curso</th>
                                                                <th class="text-center txt-primary">Cupo</th>
                                                                <th class="text-center txt-primary">Opciones</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="text-center">

                                                            </tbody>
                                                        </table>
                                                        <!-- end of table -->
                                                    </div>
                                                    <!-- end of table responsive -->
                                                </div>
                                                <!-- end of project table -->
                                            </div>

                                        </div>
                                        <!-- end of row -->
                                    </div>
                                </div>

                            </section>
                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>

        </div>
        <!-- Container-fluid ends -->
    </div>

@endsection

@section('js')

    <script>
        var IDCLIENTE = '{!! $estudiante->id !!}';
        let CLIENTE = {!! $estudiante->persona!!};
        var IDNOTA = 0;
        var STATES = [];
        var CURSOS = [];
        //$(document).ready(function () {
        // Instrucciones a ejecutar al terminar la carga
        $("#error").hide();
        console.log(CLIENTE);

        cargarMunicipios()
        cargarDatos();
        //matriculas();
        var TABLA = $('#tablaCursos').DataTable({
            "ajax": {
                "url": "/get-cursos-matricula",
                "type": "POST",
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content')
                },
                "dataSrc": function (data) {
                    console.log(data);
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Curso: data.msg[item].nombre + " " + data.msg[item].nivel+ " - " +
                                data.msg[item].fecha_inicio_curso+ " a "+ data.msg[item].fecha_fin_curso,
                            Cupos: data.msg[item].matriculados + "/" + data.msg[item].cupos,
                            Opciones: opciones(data.msg[item].id, data.msg[item].nombre + " " + data.msg[item].nivel)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            autoWidth: true,
            order: [[0, "desc"]],
            columns: [
                {data: "Id"},
                {data: "Curso"},
                {data: "Cupos"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            },
        });

        function opciones(id, nombreCurso) {
            var opciones = '';

                opciones += '<button type="button" class="btn btn-primary waves-effect waves-light Inscribir" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Inscribir" data-original-title="Inscribir"' +
                    '           onclick="guardar(`' + nombreCurso + '`)">' +
                    '           <i class="icofont icofont-ui-edit"></i>' +
                    ' </button>';

            return opciones;
        }

        function cargarDatos() {
            $('#tipo_documento').val(CLIENTE.tipo_documento)
            $('#sexo').val(CLIENTE.sexo)
            $('#select-municipio').val(CLIENTE.city.state_id).trigger('change');
            $('#select-ciudad').select2();


            $.get(
                "/getCities", {'state': CLIENTE.city.state_id}
            ).done(function (data) {
                var cities = [];
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    //dropdownParent: $("#modal-actualizar"),
                    allowClear: true,
                    data: cities
                });
                $("#select-ciudad").val(CLIENTE.city_id).trigger('change');
            });

        }

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargarMunicipios() {

            $.ajax({
                url: '/getStates',
                type: 'GET',
                async: false,
            }).done(function (data) {
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    STATES.push(itemSelect2)
                }
                $('#select-municipio').select2({
                    //dropdownParent: $("#modal-1"),
                    data: STATES
                });

                //return response;
            }).fail(function (error) {

                console.log(error)

            });


        }

        $('#select-municipio').on('change', function (e) {

            var cities = [];
            $("#select-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#select-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    //dropdownParent: $("#modal-1"),
                    allowClear: true,
                    data: cities
                });

            });
        });

        $("#editar").click(function () {
            $("#avatar").click();
        });

        $("#eliminar").click(function () {

            $.post(
                "/quitar-avatar", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#error").html(data.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + data.imagen);
                    notify(response.msg, 'success');

                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        $('#avatar').change(function () {
            var data = new FormData();
            jQuery.each(jQuery('#avatar')[0].files, function (i, file) {
                data.append('avatar', file);
            });
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: '/cambiar-avatar',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + response.imagen);
                    notify(response.msg, 'success');

                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        function actualizarUsuario() {

            $.ajax({
                    url: '/actualizar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDCLIENTE,
                        nombres: $('#nombres').val().toUpperCase(),
                        apellidos: $('#apellidos').val().toUpperCase(),
                        tipo_documento: $('#tipo_documento').val(),
                        documento: $('#documento').val(),
                        ocupacion: $('#ocupacion').val().toUpperCase(),
                        telefono: $('#telefono').val(),
                        email: $('#email').val(),
                        fecha_nacimiento: $('#fecha_nacimiento').val(),
                        sexo: $('#sexo').val(),
                        tipo_sangre: $('#tipo_sangre').val(),
                        city_id: $('#select-ciudad').val(),
                        empresa_id: $('#select-empresa').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    notify('Usuario Actualizado Correctamente', 'info')
                    location.reload();

                    $("#error").hide();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();

                });
            });
        }

        function descargar(id) {
            window.open("/descargar_certificado/" + id);
        }

        function verCurso(id) {
            location.href = "/dashboard/curso/" + id;
        }

        function cambiarPass() {
            if ($("#pass").val() != $("#pass2").val()) {
                $("#Perror").html("Las Contraseña No Son Iguales");
                $("#Perror").show();
                return;
            }
            $("#Perror").hide();

            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    passOld: $("#passOld").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function guardar(curso) {
            $.ajax({
                url: '/crear-preinscripcion',
                type: 'POST',
                data: {
                    nombres: CLIENTE.nombres,
                    apellidos: CLIENTE.apellidos,
                    tipo_documento: CLIENTE.tipo_documento,
                    documento: CLIENTE.documento,
                    cargo: CLIENTE.ocupacion,
                    telefono: CLIENTE.telefono,
                    email: '{{$estudiante->email}}',
                    curso: curso,
                    nombre_empresa: 'ninguna',
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    notify(response.msg,'error');

                    //$("#error").html(response.msg);
                    //$("#error").show();
                } else {
                    notify('Inscripcion completada','success');
                    TABLA.ajax.reload();
                    //$("#error").hide();
                }
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    notify(value[0],'error');

                    //$("#error").html(value[0]);
                    //$("#error").show();
                });

            });

        }


    </script>


@endsection
