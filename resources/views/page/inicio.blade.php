@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">

        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Dashboard</h4>
                </div>
            </div>
            <!-- 4-blocks row start -->
            <div class="row m-b-30 dashboard-header">

                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Usuarios</span>
                        <h2 class="dashboard-total-products counter">{{$usuarios->count()}}</h2>
                        <span class="label label-info">Registrados</span>En El Sistema
                        <div class="side-box bg-info">
                            <i class="icofont icofont-ui-user"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Estuiantes</span>
                        <h2 class="dashboard-total-products counter">{{$usuarios->where('rol_id','=','5')->count()}}</h2>
                        <span class="label label-primary">Registrados</span> Actualmente
                        <div class="side-box bg-primary">
                            <i class="icofont icofont-people"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Cursos</span>
                        <h2 class="dashboard-total-products counter">{{$cursos->count()}}</h2>
                        <span class="label label-success">Creados</span> Actualmente
                        <div class="side-box bg-success">
                            <i class="icofont icofont-book"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Certificados</span>
                        <h2 class="dashboard-total-products counter">{{$certificados->count()}}</h2>
                        <span class="label label-danger">Generados</span> Actualmente
                        <div class="side-box bg-danger">
                            <i class="icofont icofont-certificate"></i>
                        </div>
                    </div>
                </div>

            </div>
            <!-- 4-blocks row end -->

            <!-- 2-1 block start -->
            <div class="row">
                <div class="col-xl-8 col-lg-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table m-b-0 photo-table">
                                    <thead>
                                    <tr class="text-uppercase">
                                        <th>Foto</th>
                                        <th>Nombre</th>
                                        <th>Ultimo Ingreso</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                   @foreach($usuarios->sortByDesc('last_login')->take(5) as $usuario)
                                        @if($usuario->last_login!=null)
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/perfil/{{$usuario->imagen}}"
                                                 alt="User">
                                        </th>
                                        @if($usuario->persona!=null)
                                            <td>{{$usuario->persona->nombres}} {{$usuario->persona->apellidos}}
                                                <p><i class="icofont icofont-star"></i>Rol: {{$usuario->rol->nombre}}</p>
                                            </td>
                                        @else
                                            <td>{{$usuario->empresa->nombre}}
                                                <p><i class="icofont icofont-star"></i>Rol: {{$usuario->rol->nombre}}</p>
                                            </td>
                                        @endif
                                        <td>{{ \Carbon\Carbon::parse($usuario->last_login)->diffForHumans()}}</td>
                                    </tr>
                                    @endif
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="user-block-2">
                            <img class="img-fluid" src="/images/perfil/{{auth()->user()->imagen}}" alt="user-header">
                            <h5>{{auth()->user()->persona->nombres}} {{auth()->user()->persona->apellidos}}</h5>
                            <h6>{{auth()->user()->rol->nombre}}</h6>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 2-1 block end -->
        </div>

    </div>

@endsection