@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Header Starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Empresas</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('empresas')}}">Empresas</a>
                            </li>
                            <li class=" breadcrumb-item">Empresas Del Sistema
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Header end -->

            <!-- Row start -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Gestion de Empresas</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    data-toggle="modal"
                                    data-target="#modal-1">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block tooltip-btn">
                                <div class="table-responsive">
                                    <table class="table" id="tablaEmpresas" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Sector</th>
                                            <th>Contacto</th>
                                            <th>Telefono</th>
                                            <th>Correo</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                        </thead>
                                        <tbody id="usuario">

                                        </tbody>
                                    </table>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Row end -->
        </div>

    </div>
    @include('modals.empresa')
@endsection
@section('js')

    <script>
        var IDEMPRESA = 0;
        var STATES = [];
        $('#select-ciudad').select2();
        $('#select-municipio').select2();
        var TABLA = $('#tablaEmpresas').DataTable({
            "ajax": {
                "url": "/get-empresas",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].nombre,
                            Sector: data.msg[item].sector,
                            Contacto: data.msg[item].contacto,
                            Telefono: data.msg[item].telefono,
                            Correo: data.msg[item].email,
                            Estado: estado,
                            Opciones: opciones(data.msg[item].id,data.msg[item].estado)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Sector"},
                {data: "Contacto"},
                {data: "Telefono"},
                {data: "Correo"},
                {data: "Estado"},
                {data: "Opciones"}
            ],
            createdRow: function ( row, data, index ) {
                $(row).attr("id","us_"+data.Id);
            }
        });

        function opciones(id,estado){
            var opciones = '' +
                '<button type="button" class="btn btn-primary waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="actualizarModal(' + id + ')">\n' +
                '           <i class="icofont icofont-ui-edit"></i>\n' +
                ' </button>';
            if(estado){

                opciones+=''+
                    '<button type="button" class="btn btn-danger waves-effect waves-light actualizar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Des verificar" data-original-title="Edit"' +
                    '           onclick="verificar(' + id + ')">\n' +
                    '           <i class="icofont icofont-close"></i>\n' +
                    ' </button>';
            }else{
                opciones+=''+
                    '<button type="button" class="btn btn-success waves-effect waves-light actualizar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Verificar" data-original-title="Edit"' +
                    '           onclick="verificar(' + id + ')">\n' +
                    '           <i class="icofont icofont-check"></i>\n' +
                    ' </button>';
            }
            opciones+=''+
                '<button type="button" class="btn btn-success waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver Empresa" data-original-title="Edit"' +
                '           onclick="verEmpresa(' + id + ')">\n' +
                '           <i class="icofont icofont-eye"></i>\n' +
                ' </button>';
            opciones+=''+
                '<button type="button" class="btn btn-info waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Cambiar Contraseña" data-original-title="Edit"' +
                '           onclick="passModal(' + id + ')">\n' +
                '           <i class="icofont icofont-key"></i>\n' +
                ' </button>';

            return opciones;
        }


        function guardar() {

            $.ajax({
                url: '/crear-empresa',
                type: 'POST',
                data: {
                    nombre: $('#empresa').val().toUpperCase(),
                    nit: $('#nit').val().toUpperCase(),
                    contacto: $('#contacto').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    sector: $('#sector').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if(response.status=="Error"){
                    $("#error").html(response.msg);
                    $("#error").show();
                }else{
                    TABLA.ajax.reload();
                    $('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });

        }

        function actualizar() {
            console.log(IDEMPRESA);
            $.ajax({
                    url: '/actualizar-empresa',
                    type: 'POST',
                    data: {
                        id: IDEMPRESA,
                        nombre: $('#Aempresa').val().toUpperCase(),
                        nit: $('#Anit').val().toUpperCase(),
                        contacto: $('#Acontacto').val().toUpperCase(),
                        telefono: $('#Atelefono').val(),
                        email: $('#Aemail').val(),
                        sector: $('#Asector').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if(response.status=="Error"){
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                }else{
                    TABLA.ajax.reload();
                    $('#modal-actualizar').modal('hide');
                    $("#actualizarUsuario")[0].reset();
                    $("#Aerror").hide();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
            });
        }

        function verificar(empresa) {
            $.ajax({
                    url: '/verificar-empresa',
                    type: 'POST',
                    data: {
                        id: empresa,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if(response.status=="Error"){
                    //error
                }else{
                    TABLA.ajax.reload();
                }
                //return response;
            }).fail(function (error) {

                console.log(error);

            });
        }

        function actualizarModal(id) {

            IDEMPRESA = id;
            console.log(IDEMPRESA);

            $.ajax({
                    url: '/buscar-empresa',
                    type: 'POST',
                    data: {
                        id: IDEMPRESA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                    $('#Aempresa').val(response.msg.nombre);
                    $('#Anit').val(response.msg.nit);
                    $('#Acontacto').val(response.msg.contacto);
                    $('#Atelefono').val(response.msg.telefono);
                    $('#Aemail').val(response.msg.email);
                    $('#Asector').val(response.msg.sector);
                    $('#estado').val(response.msg.estado);

                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }

        function verEmpresa(id) {
            location.href="/dashboard/perfil/" + id;
        }

        function passModal(id) {

            IDUSUARIO = id;
            $('#modal-pass').modal();
        }

        function cambiarPass() {
            $.post(
                "/cambiar-pass",{pass:$("#pass").val(),id:IDUSUARIO,_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                console.log(data);
                if(data.status=="Error"){
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                }else{

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }
    </script>


@endsection