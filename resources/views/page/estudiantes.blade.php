@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Header Starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Estudiantes</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item">
                                <a href="/">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('estudiantes')}}">Estudiantes</a>
                            </li>
                            <li class=" breadcrumb-item">Estudiantes Registrados
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Header end -->

            <!-- Row start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Gestion de Estudiantes</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    data-toggle="modal"
                                    data-target="#modal-1">
                                <i class="fa fa-plus-circle"></i> &nbsp;Registrar
                            </button>
                        </div>

                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaEstudiantes" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Cedula</th>
                                        <th>Correo</th>
                                        <th>Rol</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody id="usuario">

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Row end -->
        </div>

    </div>
    @include('modals.estudiantes')
@endsection
@section('js')

    <script>
        var IDUSUARIO = 0;
        var STATES = [];
        var EMPRESAS = [{
            id: 0,
            text: 'Ninguna'
        }];
        $('#select-ciudad').select2();
        $('#select-municipio').select2();

        $('#select-empresa').select2();
        $('#Aselect-empresa').select2();


        var TABLA = $('#tablaEstudiantes').DataTable({
            "ajax": {
                "url": "/get-usuarios-by-rol",
                "type": "POST",
                "data": {
                    rol: 'Estudiante',
                    _token: $('meta[name="csrf-token"]').attr('content'),
                },
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombres: data.msg[item].nombre,
                            Cedula: data.msg[item].cedula,
                            Correo: data.msg[item].email,
                            Rol: data.msg[item].rol,
                            Telefono: data.msg[item].telefono,
                            Estado: estado,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombres"},
                {data: "Cedula"},
                {data: "Correo"},
                {data: "Rol"},
                {data: "Telefono"},
                {data: "Estado"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            }
        });

        $.get(
            "/get-empresas",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id_empresa,
                    text: data.msg[item].nombre
                };
                EMPRESAS.push(itemSelect2)
            }

            $('#select-empresa').select2({
                dropdownParent: $("#modal-1"),
                minimumResultsForSearch: 5,
                allowClear: true,
                placeholder: "Bucar Empresa",
                data: EMPRESAS
            });

            $('#Aselect-empresa').select2({
                dropdownParent: $("#modal-actualizar"),
                minimumResultsForSearch: 5,
                allowClear: true,
                placeholder: "Bucar Empresa",
                data: EMPRESAS
            });

            $("#select-empresa").val(0).trigger('change');
            $("#Aselect-empresa").val(0).trigger('change');

        });
        $.get(
            "/getStates",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                STATES.push(itemSelect2)
            }
            $('#select-municipio').select2({
                dropdownParent: $("#modal-1"),
                data: STATES
            });
            $('#Aselect-municipio').select2({
                dropdownParent: $("#modal-actualizar"),
                data: STATES
            });

        });

        $('#select-municipio').on('change', function (e) {
            var cities = [];
            $("#select-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#select-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    dropdownParent: $("#modal-1"),
                    allowClear: true,
                    data: cities
                });

            });
        });

        $('#Aselect-municipio').on('change', function (e) {
            var cities = [];
            $("#Aselect-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#Aselect-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#Aselect-ciudad').select2({
                    dropdownParent: $("#modal-actualizar"),
                    allowClear: true,
                    data: cities
                });

            });
        });

        function opciones(id) {
            var opciones = '' +
                '<button type="button" class="btn btn-primary waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="actualizarModal(' + id + ')">\n' +
                '           <i class="icofont icofont-ui-edit"></i>\n' +
                ' </button>' +
                '<button type="button" class="btn btn-info waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="verPerfil(' + id + ')">\n' +
                '           <i class="icofont icofont-eye-alt"></i>\n' +
                '</button>' +
                '<button type="button" class="btn btn-success waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Cambiar Contraseña" data-original-title="Edit"' +
                '           onclick="passModal(' + id + ')">\n' +
                '           <i class="icofont icofont-key"></i>\n' +
                ' </button>';
            return opciones;
        }

        function guardar() {

            $.ajax({
                url: '/crear-usuarios',
                type: 'POST',
                data: {
                    nombres: $('#nombres').val().toUpperCase(),
                    apellidos: $('#apellidos').val().toUpperCase(),
                    tipo_documento: $('#tipo_documento').val(),
                    documento: $('#documento').val(),
                    ocupacion: $('#ocupacion').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    fecha_nacimiento: $('#fecha_nacimiento').val(),
                    sexo: $('#sexo').val(),
                    tipo_sangre: $('#tipo_sangre').val(),
                    city_id: $('#select-ciudad').val(),
                    empresa_id: $('#select-empresa').val(),
                    rol_id: 5,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {

                    TABLA.ajax.reload();
                    $('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();

                }
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });

        }

        function actualizar() {
            console.log(IDUSUARIO);
            $.ajax({
                    url: '/actualizar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        nombres: $('#Anombres').val().toUpperCase(),
                        apellidos: $('#Aapellidos').val().toUpperCase(),
                        tipo_documento: $('#Atipo_documento').val(),
                        documento: $('#Adocumento').val(),
                        ocupacion: $('#Aocupacion').val().toUpperCase(),
                        telefono: $('#Atelefono').val(),
                        email: $('#Aemail').val(),
                        fecha_nacimiento: $('#Afecha_nacimiento').val(),
                        sexo: $('#Asexo').val(),
                        tipo_sangre: $('#Atipo_sangre').val(),
                        city_id: $('#Aselect-ciudad').val(),
                        empresa_id: $('#Aselect-empresa').val(),
                        rol_id: 5,
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Estudiante Actualizado', 'success');

                    //usuario(response.msg, 'remplazar');
                    $('#modal-actualizar').modal('hide');
                    $("#actualizarUsuario")[0].reset();
                    $("#Aerror").hide();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
            });
        }

        function actualizarModal(id) {

            IDUSUARIO = id;
            console.log(IDUSUARIO);

            $.ajax({
                    url: '/buscar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombres').val(response.msg.persona.nombres);
                $('#Aapellidos').val(response.msg.persona.apellidos);
                $('#Atipo_documento').val(response.msg.persona.tipo_documento);
                $('#Adocumento').val(response.msg.persona.documento);
                $('#Aocupacion').val(response.msg.persona.ocupacion);
                $('#Atelefono').val(response.msg.persona.telefono);
                $('#Aemail').val(response.msg.email);
                $('#Apassword').val(response.msg.password);
                $('#Afecha_nacimiento').val(response.msg.persona.fecha_nacimiento);
                $('#Asexo').val(response.msg.persona.sexo);
                $('#Atipo_sangre').val(response.msg.persona.tipo_sangre);
                $("#Aselect-municipio").val(response.msg.municipio).trigger('change');
                $('#Arol').val(response.msg.rol_id);
                $('#estado').val(response.msg.estado);
                $("#Aselect-empresa").val(response.msg.persona.empresa_id == null ? 0 : response.msg.persona.empresa_id).trigger('change');
                $('#estado').val(response.msg.estado);

                $.get(
                    "/getCities", {'state': response.msg.municipio}
                ).done(function (data) {
                    console.log(data);
                    var cities = [];
                    for (var item in data.msg) {
                        var itemSelect2 = {
                            id: data.msg[item].id,
                            text: data.msg[item].name
                        };
                        cities.push(itemSelect2)
                    }
                    $('#Aselect-ciudad').select2({
                        dropdownParent: $("#modal-actualizar"),
                        allowClear: true,
                        data: cities
                    });
                    $("#Aselect-ciudad").val(response.msg.persona.city_id).trigger('change');
                });

                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }

        function verPerfil(id) {
            window.location.href = "/dashboard/perfil/" + id;
        }

        function passModal(id) {
            IDUSUARIO = id;
            $('#modal-pass').modal();
            $("#form-password")[0].reset();
            $("#Perror").hide();

        }

        function cambiarPass() {
            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    id: IDUSUARIO,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

    </script>


@endsection