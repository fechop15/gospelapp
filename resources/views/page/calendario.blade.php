@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Matriculacion</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Matriculas</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Cursos Disponibles</h5>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Curso</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th>Lugar</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody id="produccion">

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                        <div class="card-block">
                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                       id="select_dia">Dia</a>
                                    <div class="slide"></div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                       id="select_mes">Mes</a>
                                    <div class="slide"></div>
                                </li>
                                {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                {{--<div class="slide"></div>--}}
                                {{--</li>--}}
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="home3" role="tabpanel">
                                    <div style="overflow:hidden;">
                                        <div class="form-group">
                                            <div id="fechaDia"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="profile3" role="tabpanel">
                                    <div style="overflow:hidden;">
                                        <div class="form-group">
                                            <div id="fechaMes"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="messages3" role="tabpanel">
                                    <div class="container">
                                        <br>
                                        <div class='col-md-12'>
                                            <div class="form-group">
                                                <label for="">Desde</label>
                                                <div class="input-group date input-group-date-custom">
                                                    <input type="text" class="form-control">
                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-md-12'>
                                            <div class="form-group">
                                                <label for="">Hasta</label>
                                                <div class="input-group date input-group-date-custom">
                                                    <input type="text" class="form-control">
                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-block waves-effect"
                                            data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title=".btn-success .btn-block">Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

@endsection

@section('js')

    <script>

        var opciones = [];
        var REGISTROS = [];
        var COMISION = 0;

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });
            //rango//
            $('#datetimepicker6').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true
            });
            $('#datetimepicker7').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                gerRegistrosMes($("#fechaMes").data("date"));
            });

            gerRegistros($("#fechaDia").data("date"));

            function gerRegistros(fecha) {
                $('#produccion').html("");
                $('#total').html("0")
                REGISTROS = [];
                $.get(
                    "/get-produccion-fecha", {
                        fecha: fecha,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#produccion').html("");
                    for (var item in data.msg) {
                        produccion(data.msg[item], 'agregar');
                    }
                    calcularToral();

                });
            }

            function gerRegistrosMes(fecha) {
                $('#produccion').html("");
                $('#total').html("0")
                REGISTROS = [];
                $.get(
                    "/get-produccion-mes", {
                        fecha: fecha,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#produccion').html("");
                    for (var item in data.msg) {
                        produccion(data.msg[item], 'agregar');
                    }
                    calcularToral();
                    //console.log(opciones)
                });
            }

            function calcularToral() {
                var total = 0;

                for (var item in REGISTROS) {
                    total += parseInt(REGISTROS[item].precio);
                }

                total = total * (COMISION / 100);
                $('#total').html(total.toLocaleString())

            }

            $("#select_dia").click(function () {
                gerRegistros($("#fechaDia").data("date"));
            });

            $("#select_mes").click(function () {
                gerRegistrosMes($("#fechaMes").data("date"));
            });
        });

        function produccion(data, op) {
            var ganancia = data.precio * (COMISION / 100);
            var html = ' <tr id="prod_' + data.id + '">' +
                '        <td class="text-nowrap">' + data.nombre + '</td>' +
                '        <td class="text-nowrap">' + data.servicio + '</td>' +
                '        <td>$' + data.precio.toLocaleString() + '</td>' +
                '        <td class="text-nowrap">$' + ganancia.toLocaleString() + '</td>' +
                '        <td class="text-nowrap">' + data.fecha + '</td>' +
                '        </tr>';

            if (op == "agregar") {
                $('#produccion').prepend(html);
                REGISTROS.push(data);
            } else if (op == "remplazar") {
                $('#prod_' + data.id).replaceWith(html);
            }

        }

    </script>


@endsection