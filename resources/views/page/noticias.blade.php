@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Header Starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Noticias</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item">
                                <a href="/">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('empleados')}}">Noticias</a>
                            </li>
                            <li class=" breadcrumb-item">Historial de noticias
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Header end -->

            <!-- Row start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card" id="vistaNoticia">
                        <div class="card-header">
                            <h5 class="card-header-text">Historial de noticias</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="modalGuardar()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaNoticias" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Titulo</th>
                                        <th>Tipo</th>
                                        <th>Autor</th>
                                        <th>Fecha</th>
                                        <th>Leidos</th>
                                        <th>Gustados</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody id="usuario">

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="card" id="noticia" style="display: none">
                        <div class="card-header">
                            <h5 class="card-header-text">Registro De Noticia</h5>
                        </div>
                        <div class="card-block" style="padding-top: 0px;">
                            <form id="guardarUsuario">
                                <input type="hidden" id="idNoticia">

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Titulo:</label>
                                            <input type="text" class="form-control" id="titulo">
                                        </div>
                                        <div class="col-md-3">
                                            <label class="col-form-label">Tipo:</label>
                                            <select class="form-control" id="tipoNoticia">
                                                <option value="Predica">Predica</option>
                                                <option value="Evento">Evento</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="col-form-label">Estado:</label>
                                            <select class="form-control" id="estado">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="editor">

                                        <div class="col-md-12" style="padding-top: 10px;">
                                            <textarea id='edit' style="margin-top: 30px;"
                                                      placeholder="Contenido de la noticia"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="alert alert-danger" id="error" style="display:flex">
                                        </div>
                                    </div>
                                </div>
                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row end -->
        </div>


    </div>
@endsection
@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>

    <script>

        var IDUSUARIO = 0;
        var evento = "guardar";
        var TABLA = $('#tablaNoticias').DataTable({
            "ajax": {
                "url": "/get-noticias",
                "type": "GET",
                "dataSrc": function (data) {
                    console.log(data.noticias);
                    var json = [];
                    for (var item in data.noticias) {
                        var estado = (data.noticias[item].estado === 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var itemJson = {
                            Id: data.noticias[item].id,
                            Titulo: data.noticias[item].titulo,
                            Tipo: data.noticias[item].tipo,
                            Autor: data.noticias[item].autor.nombres,
                            Fecha: data.noticias[item].fecha,
                            Leidos: data.noticias[item].vistos,
                            Gustados: data.noticias[item].like,
                            Contenido: data.noticias[item].contenido,
                            Estado: estado,
                            estado: data.noticias[item].estado,
                            Opciones: opciones()
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Titulo"},
                {data: "Tipo"},
                {data: "Autor"},
                {data: "Fecha"},
                {data: "Leidos"},
                {data: "Gustados"},
                {data: "Estado"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            }
        });
        $("#error").hide();

        function opciones() {
            return '' +
                '<button type="button" class="btn btn-primary waves-effect waves-light editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit">' +
                '           <i class="icofont icofont-ui-edit"></i>\n' +
                ' </button>';
        }

        function modalGuardar() {
            evento = 'guardar';
            $('#guardarUsuario')[0].reset();
            $('#actualizar').hide();
            $('#guardar').show();
            //$("#modal-1").modal('show');
            $('#noticia').show();
            $('#vistaNoticia').hide();
            $('#edit').summernote('destroy');
            $('#edit').html('');
            $('#edit').summernote({height: 100});


        }

        $('#guardar').on('click',function () {
           console.log('evento');
            $.ajax({
                url: '/crear-noticia',
                type: 'POST',
                data: {
                    titulo: $('#titulo').val().toUpperCase(),
                    tipo: $('#tipoNoticia').val(),
                    contenido: $('#edit').summernote('code')
                    ,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                //console.log(response);
                if (!response.success) {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    cancelar();
                    notify('Noticia Registrada', 'success');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        $('#actualizar').on('click',function () {

            console.log("actualizando..");
            $.ajax({
                    url: '/actualizar-noticia',
                    type: 'POST',
                    data: {
                        id: $('#idNoticia').val(),
                        titulo: $('#titulo').val().toUpperCase(),
                        tipo: $('#tipoNoticia').val(),
                        estado: $('#estado').val(),
                        contenido: $('#edit').summernote('code'),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                }
            ).done(function (response) {
                //console.log(response);
                if (!response.success) {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Noticia Actualizada', 'success');
                    cancelar();
                    //$('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                }

                //return response;
            }).fail(function (error) {

                //console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();

                });
            });
        });

        function cancelar() {
            $('#noticia').hide();
            $('#vistaNoticia').show();
        }

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        TABLA.on('click', '.editar', function () {
            evento = 'actualizar';
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            console.log(data);
            $("#error").hide();

            //$("#modal-1").modal('show');
            $('#guardarUsuario')[0].reset();

            $('#edit').summernote('destroy');

            $('#actualizar').show();
            $('#guardar').hide();

            $('#noticia').show();
            $('#vistaNoticia').hide();

            $("#idNoticia").val(data.Id);
            $("#titulo").val(data.Titulo);
            $("#tipoNoticia").val(data.Tipo);
            $("#edit").val(data.Contenido);
            $("#estado").val(data.estado);

            $('#edit').summernote({
                focus: true, height: 300, dialogsInBody: true
            });


        });
    </script>


@endsection