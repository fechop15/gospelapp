@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Matriculacion</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Matriculas</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Matriculas</h5>
                            <button type="button" class="btn btn-primary" style="float: right" data-toggle="modal"
                                    data-target="#modal-1">
                                <i class="fa fa-plus-circle"></i> &nbsp; Nueva
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaInscripciones" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cedula</th>
                                        <th>Ref Curso</th>
                                        <th>Curso</th>
                                        <th>Pagado</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>
    @include('modals.matricula')

@endsection

@section('js')

    <script>

        var USUARIOS = [];
        var CURSOS = [];
        var IDINSCRIPCION = 0;

        var TABLA = $('#tablaInscripciones').DataTable({
            "ajax": {
                "url": "/get-inscripciones",
                "type": "GET",
                "dataSrc": function (data) {
                    console.log(data);
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var pago = (data.msg[item].pago == 0 ? '<span class="label label-danger">Pendiente</span>' : '<span class="label label-success">Pago</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Cedula: data.msg[item].persona.documento,
                            RefCurso: data.msg[item].curso.id,
                            Curso: data.msg[item].curso.nombre + " " + data.msg[item].curso.nivel + "- " +
                                data.msg[item].curso.fecha_inicio_curso + " a " + data.msg[item].curso.fecha_fin_curso,
                            Pagado: pago,
                            Estado: estado,
                            Fecha: data.msg[item].fecha,
                            Opciones: opciones(data.msg[item].id, data.msg[item].estado, data.msg[item].curso.id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            order: [[0, "desc"]],
            columns: [
                {data: "Id"},
                {data: "Cedula"},
                {data: "RefCurso"},
                {data: "Curso"},
                {data: "Pagado"},
                {data: "Estado"},
                {data: "Fecha"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            }
        });

        function opciones(id, estado, curso_id) {
            var opciones = '';
            if ('{!! auth()->user()->rol->nombre !!}' == "Administrador") {
                opciones += '<button type="button" class="btn btn-primary waves-effect waves-light actualizar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                    '           onclick="actualizarModal(' + id + ')">\n' +
                    '           <i class="icofont icofont-ui-edit"></i>\n' +
                    ' </button>';
            }
            opciones += '<button type="button" class="btn btn-success waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver curso" data-original-title="Edit"' +
                '           onclick="verCurso(' + curso_id + ')">\n' +
                '           <i class="icofont icofont-eye-alt"></i>\n' +
                '</button>';

            return opciones;
        }

        cursos();


        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Estudiante'}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].persona.id,
                        text: data.msg[item].cedula + " - " + data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-persona').select2({
                    dropdownParent: $("#modal-1"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Estudiante",
                    data: USUARIOS
                });
                $("#select-persona").val("").trigger('change');
            });


        });

        function cursos() {
            CURSOS = [];
            $.post(
                "/get-cursos-matricula", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].matriculados + "/" + data.msg[item].cupos +
                            " - " + data.msg[item].nombre + " " + data.msg[item].nivel + " - " +
                            data.msg[item].fecha_inicio_curso + " a " + data.msg[item].fecha_fin_curso,
                    };
                    CURSOS.push(itemSelect2)
                }
                $('#select-curso').select2({
                    dropdownParent: $("#modal-1"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Cursos Disponibles",
                    data: CURSOS
                });
                $("#select-curso").val("").trigger('change');
            });
        }

        function guardar() {
            //        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
            //        'fecha_inicio_curso','fecha_fin_curso','estado','entrenador_id'
            $.ajax({
                url: '/crear-inscripcion',
                type: 'POST',
                data: {
                    documentado: $('#documentado').val(),
                    pago: $('#pago').val(),
                    persona_id: $('#select-persona').val(),
                    curso_id: $('#select-curso').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    $('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#Aerror").hide();
                    cursos();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
            });

        }

        function actualizarModal(id) {

            IDINSCRIPCION = id;
            $.ajax({
                    url: '/buscar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Adocumento').val(response.msg.documento);
                $('#Atelefono').val(response.msg.telefono);
                $('#Acorreo').val(response.msg.correo);
                $('#AfechaInicio').val(response.msg.fecha_inicio);
                $('#AfechaFin').val(response.msg.fecha_fin);
                $('#Acurso').val(response.msg.curso);
                $('#Adocumentado').val(response.msg.documentado);
                $('#Apago').val(response.msg.pago);
                $('#estado').val(response.msg.estado);

                if (response.msg.documentado == 1) {
                    $("#Adocumentado").attr("disabled", true);
                } else {
                    $("#Adocumentado").attr("disabled", false);
                }

                if (response.msg.pago == 1) {
                    $("#Apago").attr("disabled", true);
                } else {
                    $("#Apago").attr("disabled", false);
                }

                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }

        function actualizar() {
            $.ajax({
                    url: '/actualizar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        documentado: $('#Adocumentado').val(),
                        pago: $('#Apago').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                TABLA.ajax.reload();
                $('#modal-actualizar').modal('hide');
                $("#actualizarUsuario")[0].reset();
                cursos();
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        function informacionModal(id) {

            $.ajax({
                    url: '/buscar-inscripcion',
                    type: 'POST',
                    data: {
                        id: id,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Inombre').val(response.msg.nombre);
                $('#Idocumento').val(response.msg.documento);
                $('#Itelefono').val(response.msg.telefono);
                $('#Icorreo').val(response.msg.correo);
                $('#IfechaInicio').val(response.msg.fecha_inicio);
                $('#IfechaFin').val(response.msg.fecha_fin);
                $('#Icurso').val(response.msg.curso);
                $('#Idocumentado').val(response.msg.documentado);
                $('#Ipago').val(response.msg.pago);
                $('#Iestado').val(response.msg.estado);

                $('#modal-informacion').modal();
                return response;

            }).fail(function (error) {
                console.log(error);
            });

        }

        function verCurso(id) {
            window.location.href = "/dashboard/curso/" + id;
        }
    </script>


@endsection