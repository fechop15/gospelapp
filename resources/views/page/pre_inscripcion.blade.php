@extends('layout.publica')
@section('page')
    <div class="common-img-bg">

    <section class="login p-fixed d-flex text-center common-img-bg" style="position: inherit;">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <div class="text-center">
                            <img src="/images/logo-bajo.png" alt="logo" width="100%">
                        </div>

                        <h3 class="text-center txt-primary">
                            Pre Inscripción
                        </h3>

                        <div class="form-group" id="confirmacion" style="display:none;">
                            <div class="col-md-12">
                                <h6>Preinscripción completada. <br>
                                    FELICITACIONES , YA ESTAS PREINSCRITO. PRONTO TE CONTACTAREMOS.</h6>
                            </div>
                        </div>

                        <form class="md-float-material" id="form-preinscripcion">

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Nombres:</label>
                                    <input type="text" class="form-control" id="nombres" autocomplete="off">
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">Apellidos:</label>
                                    <input type="text" class="form-control" id="apellidos" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-6">
                                    <label class="col-form-label">Tipo Documento:</label>
                                    <select class="form-control" id="tipo_documento" style="width: 100%;">
                                        <option value="CC">CC</option>
                                        <option value="TI">TI</option>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label class="col-form-label">Documento:</label>
                                    <input type="number" class="form-control" id="documento" autocomplete="off" style="padding-top: 12px">
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-sm-6">
                                    <label class="col-form-label">Cargo/Ocupacion:</label>
                                    <input type="text" class="form-control" id="cargo" autocomplete="off">
                                </div>

                                <div class="col-sm-6">
                                    <label class="col-form-label">Empresa:</label>
                                    <input type="text" class="form-control" id="empresa" autocomplete="off">
                                </div>


                            </div>

                            <div class="form-group">

                                <div class="col-sm-6">
                                    <label class="col-form-label">Correo:</label>
                                    <input type="email" class="form-control" id="email" autocomplete="off">
                                </div>

                                <div class="col-sm-6">
                                    <label class="col-form-label">Telefono:</label>
                                    <input type="number" class="form-control" id="telefono" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <label class="col-form-label">Curso:</label>
                                <select class="form-control" id="curso" style="width: 100%;">
                                    <option value="Sendas Antiguas – Bendiciendo Generaciones">Sendas Antiguas – Bendiciendo Generaciones</option>
                                    <option value="Impartiendo la bendición a los hijos">Impartiendo la bendición a los hijos
                                    </option>
                                    <option value="Escuela para Padres">Escuela para Padres
                                    </option>
                                    <option value="Salvaje de Corazón">Salvaje de Corazón
                                    </option>
                                    <option value="Band of Brothers">Band of Brothers</option>
                                    <option value="Instituto Bíblico Capacitación Transforma Discipulado Básico">Instituto Bíblico Capacitación Transforma Discipulado Básico</option>
                                    <option value="Primeros pasos para nuevos creyentes">Primeros pasos para nuevos creyentes</option>
                                    <option value="Diplomado Mi Experiencia con Dios">Diplomado Mi Experiencia con Dios</option>
                                    <option value="Cautivante">Cautivante</option>
                                    <option value="La verdad de ser mujer">La verdad de ser mujer</option>
                                    <option value="El matrimonio sí importa">El matrimonio sí importa</option>
                                    <option value="Matrimonios para toda la vida">Matrimonios para toda la vida</option>
                                    <option value="Otro">Otro</option>
                                </select>
                                <input type="text" placeholder="Cual??" class="form-control" id="cual" autocomplete="off" style="display: none">

                            </div>

                            <div class="col-sm-12">
                                <div class="alert alert-danger" id="error" style="display: none">
                                </div>
                            </div>
                            <label class="has-error"></label>
                            <!-- <div class="card-footer"> -->
                            <!-- </div> -->
                        </form>
                        <div class="row">
                            <div class="col-xs-10 offset-xs-1">
                                <br>

                                <button id="btn-registrar" type="button" onclick="guardar()"
                                        class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                    Registrar
                                </button>

                            </div>
                        </div>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>

    </div>

@endsection
@section('js')

    <script>
        $('#curso').change(function () {
            if ($('#curso').val()=='Otro'){
                $('#cual').show();
            } else{
                $('#cual').hide();
            }
        });

        function guardar() {
            var curso=$('#curso').val();
            if ($('#curso').val()=='Otro'){
                curso=$('#cual').val();
            }
            $.ajax({
                url: '/crear-preinscripcion',
                type: 'POST',
                data: {
                    nombres: $('#nombres').val().toUpperCase(),
                    apellidos: $('#apellidos').val().toUpperCase(),
                    tipo_documento: $('#tipo_documento').val(),
                    documento: $('#documento').val(),
                    cargo: $('#cargo').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    curso: curso,
                    nombre_empresa: $('#empresa').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {

                    $("#form-preinscripcion").hide();
                    $("#confirmacion").show();
                    $("#btn-registrar").hide();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });

        }

    </script>


@endsection