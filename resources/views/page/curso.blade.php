@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>{{$curso->id}} - {{$curso->nombre}} - {{$curso->nivel}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('cursos')}}">Cursos</a>
                        </li>
                        <li class="breadcrumb-item">Perfil
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Header end -->
            <div class="row">
                <!-- start col-lg-9 -->
                <div class="col-xl-12 col-lg-12">
                    <!-- Nav tabs -->
                    <div class="tab-header">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Informacion
                                    Del Curso</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#project" role="tab">Matriculados</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#questions" role="tab">****</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#members" role="tab">****</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Sobre El Curso # {{$curso->id}}</h5>
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombre</th>
                                                                    <td>{{$curso->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nivel</th>
                                                                    <td>{{$curso->nivel}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Intensidad</th>
                                                                    <td>{{$curso->intensidad}} Horas</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Cupos</th>
                                                                    <td>{{$curso->matriculados->where("estado","=",true)->count()}}
                                                                        /{{$curso->cupos}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Entrenador</th>
                                                                    <td>{{$curso->entrenador->nombres }} {{$curso->entrenador->apellidos }}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Dias</th>
                                                                    <td>{{$curso->dias}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Fecha Fin Inscripcion</th>
                                                                    <td>{{$curso->fecha_fin_inscripciones}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Inicio del Curso</th>
                                                                    <td> {{$curso->fecha_inicio_curso}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Fin del Curso</th>
                                                                    <td>{{$curso->fecha_fin_curso}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Estado</th>
                                                                    <td>{{($curso->estado ?"Activo":"Inactivo") }}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Estudiantes</h5>
                                </div>
                                <!-- end of card-header  -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="project-table">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>

                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Nombres</th>
                                                        <th class="text-center txt-primary pro-pic">Documento</th>
                                                        <th class="text-center txt-primary">Telefono</th>
                                                        <th class="text-center txt-primary">Nota</th>
                                                        <th class="text-center txt-primary">Pago</th>
                                                        <th class="text-center txt-primary">Documentado</th>
                                                        <th class="text-center txt-primary">Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="">

                                                    @foreach($curso->matriculados->reverse() as $matricula)
                                                        @if($matricula->estado)
                                                            <tr>
                                                                <td>{{$matricula->persona->nombres}} {{$matricula->persona->apellidos}}</td>
                                                                <td class="text-center">{{$matricula->persona->documento}}</td>
                                                                <td class="text-center">{{$matricula->persona->telefono}}</td>
                                                                <td class="text-center">{{($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6)}}</td>
                                                                <td class="text-center" id="pagos_{{$matricula->id}}">
                                                                    @if($matricula->pago)
                                                                        <span class="label label-success m-t-20">Pagado</span>
                                                                    @else
                                                                        <span class="label label-danger m-t-20">Pendiente</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center"
                                                                    id="documentos_{{$matricula->id}}">
                                                                    @if($matricula->documentado)
                                                                        <span class="label label-success m-t-20">Si</span>
                                                                    @else
                                                                        <span class="label label-danger m-t-20">Pendiente</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center" id="op_button">
                                                                    @if($matricula->asistencia_validada==0 && (auth()->user()->rol_id<=3))
                                                                        <button id="asistencia_{{$matricula->id}}"
                                                                                type="button"
                                                                                class="btn btn-success waves-effect waves-light"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Confirmar Asistencia"
                                                                                data-original-title="Edit"
                                                                                onclick="asistencia('{{$matricula->id}}')">
                                                                            <i class="icofont icofont-tasks"></i>
                                                                        </button>
                                                                    @endif
                                                                    @if(($matricula->nota_teorica==null || $matricula->nota_practica==null) && (auth()->user()->rol_id<=3))
                                                                        <button id="calificacion_{{$matricula->id}}"
                                                                                type="button"
                                                                                class="btn btn-info waves-effect waves-light"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Ingresar Calificaciones"
                                                                                data-original-title="Edit"
                                                                                onclick="notaCertificado('{{$matricula->id}}')">
                                                                            <i class="icofont icofont-tick-boxed"></i>
                                                                        </button>
                                                                    @endif
                                                                    @if(
                                                                    (($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6))>=70 &&
                                                                    ($matricula->asistencia_validada!=0)
                                                                    )
                                                                        @if($matricula->fecha_certificacion!=null)
                                                                            <button type="button"
                                                                                    class="btn btn-info waves-effect waves-light"
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="Descargar Certificados"
                                                                                    data-original-title="Edit"
                                                                                    onclick="descargarCertificado('{{$matricula->id}}')">
                                                                                <i class="icofont icofont-download"></i>
                                                                            </button>
                                                                            @if($matricula->fecha_reporte_ministerio==null)
                                                                                <button type="button"
                                                                                        class="btn btn-danger waves-effect waves-light"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="top"
                                                                                        title="Reportar Al Ministerio"
                                                                                        data-original-title="Edit"
                                                                                        onclick="reporarMinisterio('{{$matricula->id}}')">
                                                                                    <i class="icofont icofont-certificate"></i>
                                                                                </button>
                                                                            @endif
                                                                        @else
                                                                            @if(auth()->user()->rol_id==1)
                                                                                <button id="reportar_{{$matricula->id}}"
                                                                                        type="button"
                                                                                        class="btn btn-warning waves-effect waves-light"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="top"
                                                                                        title="Generar Certificado"
                                                                                        data-original-title="Edit"
                                                                                        onclick="generarCertificado('{{$matricula->id}}')">
                                                                                    <i class="icofont icofont-page"></i>
                                                                                </button>
                                                                            @endif
                                                                        @endif

                                                                    @endif

                                                                    @if(
                                                                    (($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6))<70 &&
                                                                    ($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6)!=0)

                                                                        <span class="label label-danger m-t-20">Curso No Superado</span>

                                                                        <button id="calificacion_{{$matricula->id}}"
                                                                                type="button"
                                                                                class="btn btn-info waves-effect waves-light"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Modificar Nota"
                                                                                data-original-title="Edit"
                                                                                onclick="actualizarNotas('{{$matricula->id}}','{{$matricula->nota_teorica}}', '{{$matricula->nota_practica}}')">
                                                                            <i class="icofont icofont-tick-boxed"></i>
                                                                        </button>

                                                                    @endif
                                                                    @if(auth()->user()->rol_id==1||auth()->user()->rol_id==2)

                                                                        <button type="button"
                                                                                class="btn btn-primary waves-effect waves-light actualizar"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Editar Estudiante"
                                                                                data-original-title="Edit"
                                                                                onclick="actualizarModal('{{$matricula->id}}')">
                                                                            <i class="icofont icofont-ui-edit"></i>
                                                                        </button>



                                                                    @endif

                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                            <!-- end of table responsive -->
                                        </div>
                                        <!-- end of project table -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">
                            <section class="panels-wells">

                                {{--  <div class="card">
                                      <div class="card-header">
                                          <h5 class="card-header-text">Notas del Paciente</h5>
                                          <button type="button"
                                                  class="btn btn-primary waves-effect waves-light f-right"
                                                  onclick="openModal()">
                                              + Nueva Nota
                                          </button>
                                      </div>
                                      <div class="card-block">
                                          <div class="row" id="notas">


                                          </div>
                                      </div>
                                  </div>--}}

                            </section>
                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>

        </div>
        <!-- Container-fluid ends -->
    </div>


    @include('modals.curso_gestion')

@endsection

@section('js')

    <script>
        var IDCURSO = '{!! $curso->id !!}';
        var IDMATRICULA = 0;

        /---------------------------------/

        function generarCertificado(id) {
            IDMATRICULA = id;
            $('#modal-certificado').modal();
        }

        function reporarMinisterio(id) {
            IDMATRICULA = id;
            $('#modal-reportar').modal();
        }

        function notaCertificado(id) {
            $('#modal-calificacion').modal();
            $("#calificacion")[0].reset();
            $('#notaFinal').html("0");
            IDMATRICULA = id;
        }

        function actualizarNotas(id, teorica, practica) {


            $('#modal-calificacion').modal();
            $("#calificacion")[0].reset();
            $('#notaFinal').html("0");
            $('#nota_teorica').val(teorica).trigger('keyup');
            $('#nota_practica').val(practica).trigger('keyup');
            IDMATRICULA = id;
        }

        function asistencia(id) {
            $('#modal-asistencia').modal();
            $("#asistencia")[0].reset();
            IDMATRICULA = id;
        }

        function actualizarModal(id) {

            IDINSCRIPCION = id;
            $.ajax({
                    url: '/buscar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Adocumento').val(response.msg.documento);
                $('#Atelefono').val(response.msg.telefono);
                $('#Acorreo').val(response.msg.correo);
                $('#AfechaInicio').val(response.msg.fecha_inicio);
                $('#AfechaFin').val(response.msg.fecha_fin);
                $('#Acurso').val(response.msg.curso);
                $('#Adocumentado').val(response.msg.documentado);
                $('#Apago').val(response.msg.pago);
                $('#estado').val(response.msg.estado);

                if (response.msg.documentado == 1) {
                    $("#Adocumentado").attr("disabled", true);
                } else {
                    $("#Adocumentado").attr("disabled", false);
                }

                if (response.msg.pago == 1) {
                    $("#Apago").attr("disabled", true);
                } else {
                    $("#Apago").attr("disabled", false);
                }

                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }


        /---------------------------------/

        function calcularNota() {
            var nota1 = (parseInt($('#nota_teorica').val()) * 0.4);
            var nota2 = (parseInt($('#nota_practica').val()) * 0.6);
            $('#notaFinal').html(nota1 + nota2);
        }

        function calificar() {
            $.ajax({
                    url: '/calificar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        nota_practica: $('#nota_practica').val(),
                        nota_teorica: $('#nota_teorica').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Cerror").html(response.msg);
                    $("#Cerror").show();
                } else {
                    $("#calificacion_" + IDMATRICULA).remove();
                    $('#modal-calificacion').modal('hide');
                    $("#Cerror").hide();
                }

//return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Cerror").html(value[0]);
                    $("#Cerror").show();

                });
            });
        }

        function certificar() {
            $.ajax({
                    url: '/certificar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Ceerror").html(response.msg);
                    $("#Ceerror").show();
                } else {
                    $("#certificado_" + IDMATRICULA).remove();
                    $("#op_button").html("<span class='label label-success m-t-20'>Completado</span>");
                    $('#modal-certificado').modal('hide');
                    $("#Ceerror").hide();
                }

//return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Ceerror").html(value[0]);
                    $("#Ceerror").show();

                });
            });
        }

        function descargarCertificado(id) {

            window.open("/descargar_certificado/" + id);

        }

        function confirmarAsistencia() {
            $.ajax({
                    url: '/confirmarAsistencia',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        asistencia_validada: $('#asistio').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    $("#asistencia_" + IDMATRICULA).remove();
                    $('#modal-asistencia').modal('hide');
                    $("#Aerror").hide();
                }

//return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
            });
        }

        function reportar() {
            $.ajax({
                    url: '/reportar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Merror").html(response.msg);
                    $("#Merror").show();
                } else {
                    $("#reportar_" + IDMATRICULA).remove();
                    $('#modal-reportar').modal('hide');
                    $("#Merror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Merror").html(value[0]);
                    $("#Merror").show();

                });
            });
        }

        function actualizar() {
            $.ajax({
                    url: '/actualizar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        documentado: $('#Adocumentado').val(),
                        pago: $('#Apago').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                //TABLA.ajax.reload();
                $('#modal-actualizar').modal('hide');
                $("#actualizarUsuario")[0].reset();
                if ($('#Apago').val() == 1) {
                    $("#pago_" + IDINSCRIPCION).html('<span class="label label-success m-t-20">Pagado</span>');
                } else {
                    $("#pago_" + IDINSCRIPCION).html('<span class="label label-danger m-t-20">Pendiente</span>\n');
                }

                if ($('#Adocumentado').val() == 1) {
                    $("#documentos_" + IDINSCRIPCION).html('<span class="label label-success m-t-20">Si</span>');
                } else {
                    $("#documentos_" + IDINSCRIPCION).html('<span class="label label-danger m-t-20">Pendiente</span>\n');
                }
                //cursos();
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

    </script>


@endsection
