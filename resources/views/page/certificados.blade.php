@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Certificados</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Certificados</a>
                            </li>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Certificados Generados</h5>
                        </div>
                        <div class="card-block tooltip-btn">
                            <div class="btn-group">

                                <button type="button" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown" style="margin-right: 5px;margin-bottom: 5px;">
                                    <i class="icofont icofont-gears"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu" id="lista">
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="0">Reg</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="1">Nombre</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="2">Cedula</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="3">Empresa</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="4">Curso</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="5">Fecha</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="6">Fecha Cer.</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="7">Fecha Rep.</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked value="8">Opciones</label></li>
                                </ul>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tablaCertificados" width="100%">
                                    <thead>

                                    <tr>
                                        <th class="text-center txt-primary pro-pic">Reg</th>
                                        <th class="text-center txt-primary pro-pic">Nombre</th>
                                        <th class="text-center txt-primary pro-pic">Cedula</th>
                                        <th class="text-center txt-primary pro-pic">Empresa</th>
                                        <th class="text-center txt-primary pro-pic">Curso</th>
                                        <th class="text-center txt-primary">Fecha</th>
                                        <th class="text-center txt-primary">Fecha Cer.</th>
                                        <th class="text-center txt-primary">Fecha Rep.</th>
                                        <th class="text-center txt-primary">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center">
                                    @foreach($matriculas as $matricula)
                                            <tr>
                                                <td>IND{{str_pad($matricula->certificado->id, 4, '0', STR_PAD_LEFT)}}</td>
                                                <td>{{$matricula->persona->nombres}} {{$matricula->persona->apellidos}}</td>
                                                <td>{{$matricula->persona->documento}}</td>
                                                <td>{{$matricula->persona->empresa==null?'Ninguna':$matricula->persona->empresa->nombre}}</td>
                                                <td>{{$matricula->curso->nombre}} {{$matricula->curso->nivel}}</td>
                                                <td>{{$matricula->curso->fecha_inicio_curso}}
                                                    a {{$matricula->curso->fecha_fin_curso}}
                                                </td>
                                                <td>{{$matricula->fecha_certificacion}}</td>
                                                <td id="reporte_{{$matricula->id}}">{{$matricula->fecha_reporte_ministerio}}</td>
                                                <td class="text-center">
                                                    @if($matricula->fecha_certificacion!=null)
                                                        <button type="button"
                                                                class="btn btn-success waves-effect waves-light"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Descargar Certificado"
                                                                data-original-title="Edit"
                                                                onclick="descargar('{{$matricula->id}}')">
                                                            <i class="icofont icofont-download"></i>
                                                        </button>
                                                        @if($matricula->fecha_reporte_ministerio==null)
                                                            <button id="reportar_{{$matricula->id}}" type="button"
                                                                    class="btn btn-danger waves-effect waves-light"
                                                                    data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    title="Reportar Al Ministerio"
                                                                    data-original-title="Edit"
                                                                    onclick="reporarMinisterio('{{$matricula->id}}')">
                                                                <i class="icofont icofont-certificate"></i>
                                                            </button>
                                                        @endif
                                                    @else
                                                        <span class="label label-info m-t-20">En Proceso ..</span>
                                                    @endif
                                                </td>
                                            </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Reg</th>
                                        <th>Nombre</th>
                                        <th>Cedula</th>
                                        <th>Empresa</th>
                                        <th>Curso</th>
                                        <th>Fecha</th>
                                        <th>Fecha Cer.</th>
                                        <th>Fecha Rep.</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <!-- end of table -->
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

    @include('modals.curso_gestion')

@endsection

@section('js')

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>


    <script>
        var IDMATRICULA = 0;
        $('#tablaCertificados tfoot th').each( function () {
            var title = $(this).text();
            if (title!='Opciones'){
                $(this).html( '<input class="form-control input-sm" type="text" placeholder="'+title+'" style="width: 100%"/>' );
            }else {
                $(this).html('<input disabled class="form-control input-sm" type="text" placeholder="' + title + '" style="width: 100%"/>');
            }
        } );

        //var TABLA = $('#tablaCertificados').DataTable({});
        $(document).ready(function() {
            var TABLA = $('#tablaCertificados').DataTable({
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel',
                        className: 'btn btn-primary exportExcel',
                        filename: 'Export excel',
                        exportOptions: {
                            columns: ':visible'
                        }
                    }
                    ]
            });

            TABLA.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $(document).on('change','input[type="checkbox"]' ,function(e) {
                TABLA.column( this.value).visible( this.checked );
            });

        });





        /---------------------------------/

        function reporarMinisterio(id) {
            IDMATRICULA = id;
            $('#modal-reportar').modal();
        }

        /---------------------------------/

        function descargar(id) {

            window.open("/descargar_certificado/" + id);

        }

        function reportar() {
            $.ajax({
                    url: '/reportar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Merror").html(response.msg);
                    $("#Merror").show();
                } else {
                    $("#reportar_" + IDMATRICULA).remove();
                    $("#reporte_" + IDMATRICULA).html(response.msg.fecha_reporte_ministerio);
                    $('#modal-reportar').modal('hide');
                    $("#Merror").hide();

                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Merror").html(value[0]);
                    $("#Merror").show();

                });
            });
        }

        function exportTableToExcel(tableID, filename = ''){
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

            // Specify file name
            var fecha = new Date();
            var date="_"+fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
            date+="_Hora_"+fecha.getHours()+"_"+fecha.getMinutes();
            filename = filename?filename+date+'.xls':'excel_data.xls';

            // Create download link element
            downloadLink = document.createElement("a");

            document.body.appendChild(downloadLink);

            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                // Setting the file name
                downloadLink.download = filename;

                //triggering the function
                downloadLink.click();
            }
        }
    </script>


@endsection