@extends('layout.publica')
@section('page')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <div class="text-center">
                            <img src="/images/logo-bajo.png" alt="logo" width="100%">
                        </div>

                        <h3 class="text-center txt-primary">
                            Consulta de certificados
                        </h3>
                        @if(isset($inscripcion))
                            <div class="form-group" id="confirmacion">
                                <h6 id="nombre">Nombre: {{$inscripcion->persona->nombres}} {{$inscripcion->persona->apellidos}}
                                    <br>
                                    Documento: {{$inscripcion->persona->documento}}
                                </h6>
                                <div style="color:black">
                                    <table class="table table-striped">
                                        <thead class="alert-default">
                                        <tr>
                                            <th scope="col">Curso</th>
                                            <th scope="col">Expedicion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$inscripcion->curso->nombre}} {{$inscripcion->curso->nivel}}</td>
                                                <td>{{$inscripcion->fecha_certificacion}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <form action="/consulta_externa">
                                <input type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"
                                 value="Regresar" />
                            </form>
                        @else
                            <div class="form-group" id="confirmacion" style="display:none;">
                                <h6 id="nombre"></h6>
                                <div style="color:black">
                                    <table class="table table-striped">
                                        <thead class="alert-default">
                                        <tr>
                                            <th scope="col">Curso</th>
                                            <th scope="col">Expedicion</th>
                                            <th scope="col">Cod</th>
                                        </tr>
                                        </thead>
                                        <tbody id="cursos">
                                        <tr>
                                            <th>Ninguno</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" onclick="regresar()"
                                        class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                    Regresar
                                </button>
                            </div>
                            <form class="md-float-material" id="form-preinscripcion">

                                <div class="form-group">
                                    <label class="col-form-label">Numero Documento:</label>
                                    <input type="number" class="form-control" id="documento" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <div class="alert alert-danger" id="error" style="display: none">
                                    </div>
                                </div>

                                <!-- <div class="card-footer"> -->
                                <!-- </div> -->
                            </form>
                            <div class="row">
                                <div class="col-xs-10 offset-xs-1">
                                    <h6>Se encuentran disponibles los certificados emitidos a partir del 22 de Marzo de 2019</h6>

                                    <br>

                                    <button id="btn-registrar" type="button" onclick="consultar()"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                        Consultar
                                    </button>

                                </div>
                            </div>
                            <!-- end of form -->
                        @endif

                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection
@section('js')

    <script>

        function consultar() {

            $("#error").hide();
            $("#confirmacion").hide();

            $.ajax({
                url: '/consultar',
                type: 'POST',
                data: {
                    documento: $('#documento').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#form-preinscripcion").hide();
                    $("#confirmacion").show();
                    $('#cursos').html("");

                    $("#nombre").html("Nombre: " + response.nombre + "<br>Documento: " + $('#documento').val());
                    if (response.msg==''){
                        $('#cursos').append('' +
                            '<tr>' +
                            '      <td colspan="3" valign="middle">Ningun Certificado</td>' +
                            '</tr>');
                    }
                     else{
                    jQuery.each(response.msg, function (i, val) {
                        $('#cursos').append('' +
                            '<tr>' +
                            '      <td>' + val.curso + '</td>' +
                            '      <td>' + val.fecha_certificacion + '</td>' +
                            '      <td>' + val.cod + '</td>' +
                            '</tr>');
                    });
                    }

                    $("#btn-registrar").hide();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });

        }

        function regresar() {
            $("#form-preinscripcion").show();
            $("#confirmacion").hide();
            $("#error").hide();
            $("#btn-registrar").show();

        }
    </script>


@endsection