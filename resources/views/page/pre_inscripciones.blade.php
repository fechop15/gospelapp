@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Pre Inscripciones</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Pre Inscripciones</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Pre Inscripciones</h5>

                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaPreinscripciones" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Identificacion</th>
                                        <th>Correo</th>
                                        <th>Curso</th>
                                        <th>Empresa</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

    @include('modals.pre_inscripcion')

@endsection

@section('js')

    <script>
        var PREMATRICULA = 0;
        var EMPRESA = null;
        var PERSONA = 0;
        var STATES = [];
        var CURSOS = [];
        var EMPRESAS = [{
            id: 0,
            text: 'Ninguna'
        }];
        $('#select-ciudad').select2();
        $('#select-municipio').select2();

        $('#select-curso').select2();
        $('#Rselect-curso').select2();

        $('#select-empresa').select2();
        $('#Rselect-empresa').select2();

        var TABLA = $('#tablaPreinscripciones').DataTable({
            "ajax": {
                "url": "/get-preinscripcion",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombres: data.msg[item].nombre,
                            Telefono: data.msg[item].telefono,
                            Identificacion: data.msg[item].documento,
                            Correo: data.msg[item].email,
                            Empresa: data.msg[item].nombre_empresa,
                            curso: data.msg[item].curso,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            order: [[ 0, "desc" ]],
            columns: [
                {data: "Id"},
                {data: "Nombres"},
                {data: "Telefono"},
                {data: "Identificacion"},
                {data: "Correo"},
                {data: "curso"},
                {data: "Empresa"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            }
        });

        $.get(
            "/get-empresas",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id_empresa,
                    text: data.msg[item].nombre
                };
                EMPRESAS.push(itemSelect2)
            }

            $('#select-empresa').select2({
                dropdownParent: $("#modal-1"),
                minimumResultsForSearch: 5,
                allowClear: true,
                placeholder: "Bucar Empresa",
                data: EMPRESAS
            });

            $('#Rselect-empresa').select2({
                dropdownParent: $("#modal-actualizar"),
                minimumResultsForSearch: 5,
                allowClear: true,
                placeholder: "Bucar Empresa",
                data: EMPRESAS
            });

            $("#select-empresa").val(0).trigger('change');
            $("#Rselect-empresa").val(0).trigger('change');

        });

        $.get(
            "/getStates",
        ).done(function (data) {
            //console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                STATES.push(itemSelect2)
            }
            $('#select-municipio').select2({
                dropdownParent: $("#modal-1"),
                data: STATES
            });
        });

        $('#select-municipio').on('change', function (e) {
            var cities = [];
            $("#select-ciudad").html('').select2();
            $.get(
                "/getCities", {'state': $("#select-municipio").val()}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#select-ciudad').select2({
                    dropdownParent: $("#modal-1"),
                    allowClear: true,
                    data: cities
                });
            });
        });

        function matriculas() {
            $.post(
                "/get-cursos-matricula", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].matriculados + "/" + data.msg[item].cupos +
                            " - " + data.msg[item].nombre + " " + data.msg[item].nivel+ " - " +
                            data.msg[item].fecha_inicio_curso+ " a "+ data.msg[item].fecha_fin_curso,
                    };
                    CURSOS.push(itemSelect2)
                }
                $('#select-curso').select2({
                    dropdownParent: $("#modal-1"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Cursos Disponibles",
                    data: CURSOS
                });
                $('#Rselect-curso').select2({
                    dropdownParent: $("#modal-2"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Cursos Disponibles",
                    data: CURSOS
                });
                $("#select-curso").val("").trigger('change');
                $("#Rselect-curso").val("").trigger('change');
            });
        }

        function eliminar(id) {
            PREMATRICULA = id;
            $('#modal-3').modal();

        }

        function opciones(id) {
            var opciones = '' +
                '<button type="button" class="btn btn-primary waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="matricular(' + id + ')">\n' +
                '           <i class="icofont icofont-ui-edit"></i>\n' +
                ' </button>' +
                '<button type="button" class="btn btn-danger waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="eliminar(' + id + ')">\n' +
                '           <i class="icofont icofont-trash"></i>\n' +
                ' </button>';
            return opciones;
        }


        function matricular(id) {
            matriculas();
            PREMATRICULA = id;
            $.ajax({
                    url: '/buscar-preinscripcion',
                    type: 'POST',
                    data: {
                        id: PREMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);
                if (response.msg.persona == null) {
                    $('#nombres').val(response.msg.nombres);
                    $('#apellidos').val(response.msg.apellidos);
                    $('#documento').val(response.msg.documento);

                    $('#ocupacion').val(response.msg.ocupacion);
                    $('#telefono').val(response.msg.telefono);

                    $('#email').val(response.msg.email);
                    $('#cursoInteresado').html("Esta interesado en :" + response.msg.curso);
                    $("#select-empresa").val(response.msg.empresa==null?0:response.msg.empresa.id).trigger('change');
                    $('#modal-1').modal();
                    PERSONA = 0;
                } else {
                    $('#Rnombres').val(response.msg.nombres);
                    $('#Rapellidos').val(response.msg.apellidos);
                    $('#Rdocumento').val(response.msg.documento);
                    $('#RcursoInteresado').html("Esta interesado en :" + response.msg.curso);
                    $("#Rselect-empresa").val(response.msg.empresa==null?0:response.msg.empresa.id).trigger('change');
                    $('#modal-2').modal();
                    PERSONA = response.msg.persona.id;
                }
                //EMPRESA=response.msg.empresa.id;

                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }

        function guardar() {

            if (PERSONA == 0) {
                crearUsuario();
            } else {
                crearInscripcion(PERSONA, $('#Rdocumentado').val(), $('#Rpago').val(), $('#Rselect-curso').val(),$('#Rselect-empresa').val());
            }

        }

        function crearUsuario() {

            $.ajax({
                url: '/crear-usuarios',
                type: 'POST',
                data: {
                    nombres: $('#nombres').val().toUpperCase(),
                    apellidos: $('#apellidos').val().toUpperCase(),
                    tipo_documento: "CC",
                    documento: $('#documento').val(),
                    ocupacion: $('#ocupacion').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    password: $('#password').val(),
                    fecha_nacimiento: $('#fecha_nacimiento').val(),
                    sexo: $('#sexo').val(),
                    city_id: $('#select-ciudad').val(),
                    empresa_id: $('#select-empresa').val(),
                    rol_id: 5,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    crearInscripcion(response.msg.persona.id, $('#documentado').val(), $('#pago').val(), $('#select-curso').val(),$('#select-empresa').val());
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });

        }

        function crearInscripcion(persona_id, documentado, pago, curso,empresa) {
            //        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
            //        'fecha_inicio_curso','fecha_fin_curso','estado','entrenador_id'
            $.ajax({
                url: '/crear-inscripcion',
                type: 'POST',
                data: {
                    documentado: documentado,
                    pago: pago,
                    persona_id: persona_id,
                    curso_id: curso,
                    empresa_id: empresa,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                    $("#Rerror").html(response.msg);
                    $("#Rerror").show();
                } else {

                    //TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    //$("#guardarUsuario")[0].reset();
                    //$("#Aerror").hide();
                    terminarPrematricula();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();

                    $("#Rerror").html(value[0]);
                    $("#Rerror").show();

                });
            });

        }

        function terminarPrematricula() {
            $.post(
                "/terminar-preinscripcion", {id: PREMATRICULA, _token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                TABLA.ajax.reload();
                $("#guardarMatricula")[0].reset();
                $("#guardarMatriculaR")[0].reset();
                $('#modal-1').modal('hide');
                $('#modal-2').modal('hide');
                $('#modal-3').modal('hide');
                $("#error").hide();
                $("#Rerror").hide();
                EMPRESA=null
            });

        }

    </script>


@endsection