@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Servicios</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">Cursos</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Cursos Disponibles
                            </h5>

                            @if(auth()->user()->rol_id==1)
                                <button type="button" class="btn btn-primary" style="float: right" data-toggle="modal"
                                        data-target="#modal-1">
                                    <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                                </button>
                            @endif


                        </div>
                        <div class="card-block">

                            <div class="btn-group">

                                <button type="button" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown">
                                    <i class="icofont icofont-gears"></i>
                                </button>

                                <ul class="dropdown-menu " role="menu">
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="0">Cod</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="1">Nombre</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="2">Nivel</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="3">Cupos</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="4">Incripciones</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="5">Fecha Inicio</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="6">Entrenador</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="7">Supervisor</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="8">Ciudad</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="9">Estado</label></li>
                                    <li class="dropdown-item"><label><input autocomplete="off" type="checkbox" checked
                                                                            value="10">Acciones</label></li>
                                </ul>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaCursos" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Cod</th>
                                        <th>Nombre</th>
                                        <th>Nivel</th>
                                        <th>Cupos</th>
                                        <th>Incripciones</th>
                                        <th>Fecha Inicio</th>
                                        <th>Entrenador</th>
                                        <th>Supervisor</th>
                                        <th>Ciudad</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

    @include('modals.curso')

@endsection

@section('js')

    <script>
        var IDCURSO = 0;
        var USUARIOS = [];
        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Entrenador'}
            ).done(function (data) {
                //console.log(data);

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].persona.id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-entrenador').select2({
                    dropdownParent: $("#modal-1"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Entrenador",
                    data: USUARIOS
                });
                $('#Aselect-entrenador').select2({
                    dropdownParent: $("#modal-actualizar"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Entrenador",
                    data: USUARIOS
                });
                $("#select-entrenador").val("").trigger('change');
                $("#Aselect-entrenador").val("").trigger('change');

            });

            function diasEntreFechas(desde, hasta) {
                var dia_actual = moment(desde);
                var fechas = [];
                while (dia_actual.isSameOrBefore(moment(hasta))) {
                    fechas.push(dia_actual.format('DD'));
                    dia_actual.add(1, 'days');
                }
                return fechas;
            }

            $("#fechaInicio").change(function () {
                if ($("#fechaInicio").val() != null && $("#fechaFin").val() != null) {
                    var results = diasEntreFechas($("#fechaInicio").val(), $("#fechaFin").val());
                    $('#dias').val(results.join(', '));
                }
            });
            $("#fechaFin").change(function () {
                if ($("#fechaInicio").val() != null && $("#fechaFin").val() != null) {
                    var results = diasEntreFechas($("#fechaInicio").val(), $("#fechaFin").val());
                    $('#dias').val(results.join(', '));
                }
            });

            $("#AfechaInicio").change(function () {
                if ($("#AfechaInicio").val() != null && $("#AfechaFin").val() != null) {
                    var results = diasEntreFechas($("#AfechaInicio").val(), $("#AfechaFin").val());
                    $('#Adias').val(results.join(', '));
                }
            });
            $("#AfechaFin").change(function () {
                if ($("#AfechaInicio").val() != null && $("#AfechaFin").val() != null) {
                    var results = diasEntreFechas($("#AfechaInicio").val(), $("#AfechaFin").val());
                    $('#Adias').val(results.join(', '));
                }
            });

        });
        var TABLA = $('#tablaCursos').DataTable({
            "ajax": {
                "url": "/get-cursos",
                "type": "GET",
                "dataSrc": function (data) {
                    console.log(data);
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].nombre,
                            Nivel: data.msg[item].nivel,
                            Incripciones: data.msg[item].fecha_inicio_inscripciones + " a " + data.msg[item].fecha_fin_inscripciones,
                            Fechas: data.msg[item].fecha_inicio_curso + " a " + data.msg[item].fecha_fin_curso,
                            Cupos: data.msg[item].matriculados + "/" + data.msg[item].cupos,
                            Entrenador: data.msg[item].entrenador.nombres,
                            Supervisor: data.msg[item].supervisor,
                            Ciudad: data.msg[item].ciudad,
                            Estado: estado,
                            Opciones: opciones(data.msg[item].id, data.msg[item].estado)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            autoWidth: true,
            order: [[0, "desc"]],
            columns: [
                {data: "Id"},
                {data: "Nombre"},
                {data: "Nivel"},
                {data: "Cupos"},
                {data: "Incripciones"},
                {data: "Fechas"},
                {data: "Entrenador"},
                {data: "Supervisor"},
                {data: "Ciudad"},
                {data: "Estado"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            },
        });

        $(document).on('change', 'input[type="checkbox"]', function (e) {
            TABLA.column(this.value).visible(this.checked);

        });

        function opciones(id, estado) {
            var opciones = '';
            if ('{!! auth()->user()->rol->nombre !!}' == "Administrador") {
                opciones += '<button type="button" class="btn btn-primary waves-effect waves-light actualizar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                    '           onclick="actualizarModal(' + id + ')">\n' +
                    '           <i class="icofont icofont-ui-edit"></i>\n' +
                    ' </button>';
            }
            opciones += '<button type="button" class="btn btn-success waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver curso" data-original-title="Edit"' +
                '           onclick="verPerfil(' + id + ')">\n' +
                '           <i class="icofont icofont-eye-alt"></i>\n' +
                '</button>';

            return opciones;
        }

        function verPerfil(id) {
            window.location.href = "/dashboard/curso/" + id;
        }

        function guardar() {
            //        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
            //        'fecha_inicio_curso','fecha_fin_curso','estado','entrenador_id'
            $("#guardar").attr("disabled", true);
            $("#guardar").html("Guardando..");

            var data = new FormData();
            jQuery.each(jQuery('#certificado')[0].files, function (i, file) {
                data.append('certificado', file);
            });
            data.append('nombre', $('#nombre').val().toUpperCase());
            data.append('nivel', $('#nivel').val().toUpperCase());
            data.append('supervisor', $('#supervisor').val().toUpperCase());
            data.append('intensidad', $('#intensidad').val().toUpperCase());
            data.append('cupos', $('#cupos').val());
            data.append('fecha_inicio_inscripciones', $('#inscripcionFechaInicio').val());
            data.append('fecha_fin_inscripciones', $('#inscripcionFechaFin').val());
            data.append('fecha_inicio_curso', $('#fechaInicio').val());
            data.append('fecha_fin_curso', $('#fechaFin').val());
            data.append('entrenador_id', $('#select-entrenador').val());
            data.append('dias', $('#dias').val());
            data.append('ciudad', $('#ciudad').val());
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                url: '/crear-curso',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    $('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            }).always(function (data) {
                $("#guardar").attr("disabled", false);
                $("#guardar").html("Guardar");

            });

        }

        function actualizar() {
            var data = new FormData();
            jQuery.each(jQuery('#Acertificado')[0].files, function (i, file) {
                data.append('Acertificado', file);
            });
            data.append('id', IDCURSO);
            data.append('nombre', $('#Anombre').val().toUpperCase());
            data.append('nivel', $('#Anivel').val().toUpperCase());
            data.append('supervisor', $('#Asupervisor').val().toUpperCase());
            data.append('intensidad', $('#Aintensidad').val().toUpperCase());
            data.append('cupos', $('#Acupos').val());
            data.append('fecha_inicio_inscripciones', $('#AinscripcionFechaInicio').val());
            data.append('fecha_fin_inscripciones', $('#AinscripcionFechaFin').val());
            data.append('fecha_inicio_curso', $('#AfechaInicio').val());
            data.append('fecha_fin_curso', $('#AfechaFin').val());
            data.append('entrenador_id', $('#Aselect-entrenador').val());
            data.append('estado', $('#estado').val());
            data.append('dias', $('#Adias').val());
            data.append('ciudad', $('#Aciudad').val());
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                    url: '/actualizar-curso',
                    type: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false
                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    $('#modal-actualizar').modal('hide');
                    $("#actualizarUsuario")[0].reset();
                    $("#Aerror").hide();
                }

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
            });
        }

        function actualizarModal(id) {
            IDCURSO = id;
            $.ajax({
                    url: '/buscar-curso',
                    type: 'POST',
                    data: {
                        id: IDCURSO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Anivel').val(response.msg.nivel);
                $('#Aintensidad').val(response.msg.intensidad);
                $('#Acupos').val(response.msg.cupos);
                $('#AinscripcionFechaInicio').val(response.msg.fecha_inicio_inscripciones);
                $('#AinscripcionFechaFin').val(response.msg.fecha_fin_inscripciones);
                $('#AfechaInicio').val(response.msg.fecha_inicio_curso);
                $('#AfechaFin').val(response.msg.fecha_fin_curso);
                $('#Aselect-entrenador').val(response.msg.entrenador.id).trigger('change');
                $('#estado').val(response.msg.estado);
                $('#Adias').val(response.msg.dias);
                $('#Aciudad').val(response.msg.ciudad);
                $('#Asupervisor').val(response.msg.supervisor);

                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);

            });
        }

    </script>


@endsection