@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>{{$empresa->nombre}} - {{ $empresa->nit }}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('empresas')}}">Empresa</a>
                        </li>
                        <li class="breadcrumb-item">Perfil
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Header end -->
            <div class="row">
                <!-- start col-lg-9 -->
                <div class="col-xl-12 col-lg-12">
                    <!-- Nav tabs -->
                    <div class="tab-header">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Informacion
                                    Personal</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#project" role="tab">Preinscripcion</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#questions" role="tab">Cursos</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#members" role="tab">Cuenta</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Sobre Mi</h5>
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombres:</th>
                                                                    <td>{{$empresa->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nit:</th>
                                                                    <td>{{$empresa->nit}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sector:</th>
                                                                    <td>{{$empresa->sector}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Contacto:</th>
                                                                    <td>{{$empresa->contacto}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Telefono:</th>
                                                                    <td> {{$empresa->telefono}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Correo:</th>
                                                                    <td>{{$empresa->usuario->email}}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">

                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Pre Inscripciones</h5>
                                    @if(auth()->user()->rol_id==4)
                                        <button type="button" class="btn btn-primary waves-effect md-trigger"
                                                style="float: right"
                                                data-toggle="modal"
                                                data-target="#modal-1">
                                            <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                                        </button>
                                    @endif
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="tablaPreinscripciones" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="txt-primary">Nombre</th>
                                                <th class="txt-primary">Telefono</th>
                                                <th class="txt-primary">Identificacion</th>
                                                <th class="txt-primary">Correo</th>
                                                <th class="txt-primary">Curso</th>
                                                <th class="txt-primary">Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">


                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Cursos</h5>
                                </div>
                                <div class="card-block">
                                    <div class="btn-group">

                                        <button type="button" class="btn btn-primary dropdown-toggle"
                                                data-toggle="dropdown" style="margin-right: 5px;margin-bottom: 5px;">
                                            <i class="icofont icofont-gears"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" id="lista">
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="0">Nombre</label>
                                            </li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="1">Documento</label>
                                            </li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="2">Curso</label></li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="3">Fecha</label></li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="4">Dias</label></li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked
                                                                                    value="5">Descargable</label></li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="6">Pago</label></li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="7">Fecha Cer.</label>
                                            </li>
                                            <li class="dropdown-item"><label><input autocomplete="off" type="checkbox"
                                                                                    checked value="8">Opciones</label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="tablaMatriculados" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center txt-primary pro-pic">Nombre</th>
                                                <th class="text-center txt-primary">Documento</th>
                                                <th class="text-center txt-primary">Curso</th>
                                                <th class="text-center txt-primary">Fecha</th>
                                                <th class="text-center txt-primary">Dias</th>
                                                <th class="text-center txt-primary">Descargable</th>
                                                <th class="text-center txt-primary">Pago</th>
                                                <th class="text-center txt-primary">Fecha Cer.</th>
                                                <th class="text-center txt-primary">Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($empresa->personas as $persona)
                                                @foreach($persona->matriculas as $matricula)
                                                    @if($matricula->estado==1)
                                                        <tr>
                                                            <td>{{$persona->nombres}} {{$persona->apellidos}}</td>
                                                            <td>{{$persona->documento}}</td>
                                                            <td>{{$matricula->curso->nombre}} {{$matricula->curso->nivel}}</td>
                                                            <td>{{$matricula->curso->fecha_inicio_curso}}
                                                                a {{$matricula->curso->fecha_fin_curso}}</td>
                                                            <td>{{$matricula->curso->dias}}</td>
                                                            <td id="descargable-td-{{$matricula->id}}">{{$matricula->descargable?'Si':'No'}}</td>
                                                            <td class="text-center">
                                                                @if($matricula->pago)
                                                                    <span class="label label-success m-t-20">Pago</span>
                                                                @else
                                                                    <span class="label label-danger m-t-20">Pendiente</span>
                                                                @endif
                                                            </td>
                                                            <td>{{$matricula->fecha_certificacion}}</td>
                                                            <td class="text-center">
                                                                @if($matricula->fecha_certificacion!=null && $matricula->pago)
                                                                    @if(!$matricula->documentado)
                                                                        <span class="label label-danger m-t-20">Falta Documentacion</span>
                                                                    @else
                                                                    <button type="button"
                                                                            class="btn btn-success waves-effect waves-light"
                                                                            data-toggle="tooltip"
                                                                            data-placement="top"
                                                                            title="Descargar Certificado"
                                                                            data-original-title="Descargar Certificado"
                                                                            onclick="descargar({{$matricula->id}})">
                                                                        <i class="icofont icofont-download"></i>
                                                                    </button>
                                                                    @endif
                                                                @else
                                                                    <span class="label label-info m-t-20">En Proceso ..</span>
                                                                @endif
                                                                <button type="button"
                                                                        class="btn btn-success waves-effect waves-light"
                                                                        data-toggle="tooltip"
                                                                        data-placement="top" title="Permitir Descarga"
                                                                        data-original-title="Permitir Descarga"
                                                                        onclick="descargable({{$matricula->id}})">
                                                                    <i class="icofont icofont-question"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Documento</th>
                                                <th>Curso</th>
                                                <th>Fecha</th>
                                                <th>Dias</th>
                                                <th>Descargable</th>
                                                <th>Pago</th>
                                                <th>Fecha Cer.</th>
                                                <th>Opciones</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                            @if(auth()->user()->id==$empresa->usuario->id)
                                <section class="panels-wells">

                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Cuenta</h5>
                                        </div>
                                        <!-- end of card-header  -->
                                        <div class="card-block">

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h6 class="card-header-text">Cambio de Contraseña</h6>

                                                    <form id="form-password">

                                                        <div class="form-group">
                                                            <label class="col-form-label">Contraseña Actual:</label>
                                                            <input type="text" class="form-control" id="passOld">

                                                            <label class="col-form-label">Contraseña nueva:</label>
                                                            <input type="text" class="form-control" id="pass">

                                                            <label class="col-form-label">Contraseña nueva
                                                                (Confirmacion):</label>
                                                            <input type="text" class="form-control" id="pass2">
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="alert alert-danger" id="Perror"
                                                                 style="display: none">
                                                            </div>
                                                        </div>

                                                    </form>
                                                    <button type="button"
                                                            class="btn btn-primary waves-effect waves-light"
                                                            onclick="cambiarPass()">Confirmar
                                                    </button>

                                                    <!-- end of project table -->
                                                </div>
                                                <div class="col-sm-4">
                                                    <h6>IMAGEN DE PERFIL</h6>
                                                    <div class="card">
                                                        <div class="social-profile">
                                                            <img id="perfil" class="img-fluid width-100"
                                                                 src="/images/perfil/{{auth()->user()->imagen}}" alt="">
                                                            <div class="profile-hvr m-t-15">
                                                                <i class="icofont icofont-ui-edit p-r-10"
                                                                   id="editar"></i>
                                                                <i class="icofont icofont-ui-delete" id="eliminar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <form id="subirImagen">
                                                        <input type="file" id="avatar" name="avatar"
                                                               style="display: none" accept="image/*">
                                                    </form>
                                                    <!-- end of project table -->
                                                </div>
                                                <div class="col-sm-4"></div>
                                                <!-- end of col-lg-12 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>
                                    </div>

                                </section>
                            @endif
                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>

        </div>
        <!-- Container-fluid ends -->
    </div>



    @include('modals.pre_inscripcion_empresa')


@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>

    <script>
        var IDCLIENTE = '{!! $empresa->id !!}';
        var MATRICULA = 0;

        function descargable(id) {
            MATRICULA = id;
            $('#modal-descargable').modal();
        }

        function SendDescargable() {
            $.ajax({
                    url: '/descargable',
                    type: 'POST',
                    data: {
                        id: MATRICULA,
                        estado: $('#descargable').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {

                } else {
                    $('#modal-descargable').modal('hide');
                    $('#descargable-td-' + MATRICULA).html('' + response.msg);
                }
                //return response;
            }).fail(function (error) {

                console.log(error);

            });
        }


        $('#curso').change(function () {
            if ($('#curso').val() == 'Otro') {
                $('#cual').show();
            } else {
                $('#cual').hide();
            }
        });

        var TABLA = $('#tablaPreinscripciones').DataTable({
            "ajax": {
                "url": "/get-preinscripcion",
                {!! auth()->user()->rol_id!=4?'"data": {id:IDCLIENTE},':''!!}
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombres: data.msg[item].nombre,
                            Telefono: data.msg[item].telefono,
                            Identificacion: data.msg[item].documento,
                            Correo: data.msg[item].email,
                            curso: data.msg[item].curso,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            order: [[0, "desc"]],
            columns: [
                {data: "Nombres"},
                {data: "Telefono"},
                {data: "Identificacion"},
                {data: "Correo"},
                {data: "curso"},
                {data: "Opciones"}
            ],
            createdRow: function (row, data, index) {
                $(row).attr("id", "us_" + data.Id);
            }
        });

        var TABLA2 = $('#tablaMatriculados').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    className: 'btn btn-primary exportExcel',
                    filename: 'reporte_empresa',
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ]
        });

        $('#tablaMatriculados tfoot th').each(function () {
            var title = $(this).text();
            if (title != 'Opciones') {
                $(this).html('<input class="form-control input-sm" type="text" placeholder="' + title + '" style="width: 100%"/>');
            } else {
                $(this).html('<input class="form-control input-sm" type="text" placeholder="' + title + '" style="width: 100%"/>');
            }
        });

        TABLA2.columns().every(function () {
            var that = this;

            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

        $(document).on('change', 'input[type="checkbox"]', function (e) {
            TABLA2.column(this.value).visible(this.checked);
        });

        function opciones(id) {
            var opciones = '' +
                '<button type="button" class="btn btn-danger waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="eliminar(' + id + ')">\n' +
                '           <i class="icofont icofont-trash"></i>\n' +
                ' </button>';
            return opciones;
        }


        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        $("#editar").click(function () {
            $("#avatar").click();
        });

        $("#eliminar").click(function () {

            $.post(
                "/quitar-avatar", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#error").html(data.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + data.imagen);
                    notify(response.msg, 'success');

                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        $('#avatar').change(function () {
            var data = new FormData();
            jQuery.each(jQuery('#avatar')[0].files, function (i, file) {
                data.append('avatar', file);
            });
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: '/cambiar-avatar',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + response.imagen);
                    notify(response.msg, 'success');

                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        //});

        function descargar(id) {
            window.open("/descargar_certificado/" + id);
        }


        function eliminar(id) {
            PREMATRICULA = id;
            $('#modal-3').modal();
        }


        function cambiarPass() {
            if ($("#pass").val() != $("#pass2").val()) {
                $("#Perror").html("Las Contraseña No Son Iguales");
                $("#Perror").show();
                return;
            }
            $("#Perror").hide();

            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    passOld: $("#passOld").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function guardarPreinscripcion() {

            var curso = $('#curso').val();

            if ($('#curso').val() == 'Otro') {
                curso = $('#cual').val();
            }
            $.ajax({
                url: '/crear-preinscripcion',
                type: 'POST',
                data: {
                    nombres: $('#nombres').val().toUpperCase(),
                    apellidos: $('#apellidos').val().toUpperCase(),
                    tipo_documento: $('#tipo_documento').val(),
                    documento: $('#documento').val(),
                    cargo: $('#cargo').val().toUpperCase(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    curso: curso,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    $('#modal-1').modal('hide');
                    notify('Preinscripcion Registrada', 'success');
                    $("#form-preinscripcion")[0].reset();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });

        }

    </script>


@endsection
