<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'api\UsuarioController@login');

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {

    Route::post('update', 'api\UsuarioController@updateToken');

    Route::post('getUsuarios', 'api\UsuarioController@getUsuarios');

    Route::post('getInvitados', 'api\InvitadosController@getInvitados');
    Route::post('getMisInvitados', 'api\InvitadosController@misInvitados');
    Route::post('addInvitado', 'api\InvitadosController@crearInvitado');
    Route::post('confirmarInvitado', 'api\InvitadosController@confirmarInvitado');
    Route::post('eliminarInvitado', 'api\InvitadosController@eliminarInvitado');

    Route::post('getNoticias', 'api\NoticiasController@getNoticias');
    Route::post('getUltimaNoticia', 'api\NoticiasController@ultimaNoticia');
    Route::post('visitaNoticia', 'api\NoticiasController@visitaNoticia');
    Route::post('like', 'api\NoticiasController@like');

});