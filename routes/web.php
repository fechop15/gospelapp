<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//rutas
//ruta para todos
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('blank', 'DashboardController@blank')->name('blank');
    Route::get('perfil', 'DashboardController@perfil')->name('perfil');

});

//ruta para los roles 1 2 y 3
Route::group(['prefix' => 'dashboard', 'middleware' => 'empresa'], function () {

    Route::get('curso/{id}', 'DashboardController@curso')->name('infoCurso');
    Route::get('cursos', 'DashboardController@cursos')->name('cursos');

});

//rutas para roles 1 y 2
Route::group(['prefix' => 'dashboard', 'middleware' =>'empleado'], function () {

    Route::get('perfil/{id}', 'DashboardController@perfil')->name('perfilCliente');
    Route::get('estudiantes', 'DashboardController@estudiantes')->name('estudiantes');
    Route::get('empresas', 'DashboardController@empresas')->name('empresas');
    Route::get('pre_inscripciones', 'DashboardController@pre_inscripciones')->name('pre_inscripciones');
    Route::get('matriculas', 'DashboardController@matriculas')->name('matriculas');
    Route::get('noticias', 'DashboardController@noticias')->name('noticias');

});
//rutas para el rol 1
Route::group(['prefix' => 'dashboard', 'middleware' =>  'admin'], function () {

    Route::get('empleados', 'DashboardController@usuarios')->name('empleados');
    Route::get('certificados', 'DashboardController@certificados')->name('certificados');

});

//rutas para el rol 3
Route::group(['prefix' => 'dashboard', 'middleware' => 'entrenador'], function () {

});
//rutas para el rol 4 y 5
Route::group(['prefix' => 'dashboard', 'middleware' => 'users'], function () {


});

Route::get('/', 'Auth\LoginController@showLoginForm')->middleware('guest');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login')->middleware('guest');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('login', 'Auth\LoginController@login')->middleware('guest');

Route::get('/pre_inscripcion', 'DashboardController@pre_inscripcion');
Route::get('/consulta_externa/{id}', 'DashboardController@consulta_externa');
Route::get('/consulta_externa', 'DashboardController@consulta_externa');
Route::post('/consultar', 'InscripcionsController@consultar');
Route::post('crear-preinscripcion', 'Pre_inscripcionController@crearPreinscripcion');


Route::group(['middleware' => ['auth']], function () {

    Route::get('get-noticias', 'NoticiasController@getNoticias');
    Route::post('crear-noticia', 'NoticiasController@crearNoticia'); //admin
    Route::post('actualizar-noticia', 'NoticiasController@actualizarNoticia'); //admin
    Route::post('eliminar-noticia', 'NoticiasController@actualizarNoticia'); //admin


    Route::get('get-preinscripcion', 'Pre_inscripcionController@getPreinscripciones');
    Route::post('buscar-preinscripcion', 'Pre_inscripcionController@buscarPreinscripcion');
    Route::post('terminar-preinscripcion', 'Pre_inscripcionController@terminarPreinscripcion');

//metodos
    Route::get('get-usuarios', 'UsuarioController@getUsuarios');
    Route::post('get-usuarios-by-rol', 'UsuarioController@getUsuariosByRol');
    Route::post('crear-usuarios', 'UsuarioController@crearUsuario'); //admin
    Route::post('actualizar-usuarios', 'UsuarioController@actualizarUsuario'); //admin
    Route::post('buscar-usuarios', 'UsuarioController@buscarUsuario');
    Route::post('cambiar-pass', 'UsuarioController@actualizarPass');
    Route::post('cambiar-avatar', 'UsuarioController@update_profile');
    Route::post('quitar-avatar', 'UsuarioController@remove_profile');
 // cambio de contraseña
//empresas//
    Route::get('get-empresas', 'EmpresasController@getEmpresas');
    Route::post('crear-empresa', 'EmpresasController@crearEmpresa');
    Route::post('actualizar-empresa', 'EmpresasController@actualizarEmpresa');
    Route::post('buscar-empresa', 'EmpresasController@buscarEmpresa');
    Route::post('verificar-empresa', 'EmpresasController@verificarEmpresa'); //admin
//fin empresas//

//cursos//
    Route::get('get-cursos', 'CursosController@getCursos');
    Route::post('crear-curso', 'CursosController@crearCurso'); // admin
    Route::post('actualizar-curso', 'CursosController@actualizarCurso');
    Route::post('buscar-curso', 'CursosController@buscarCurso');
    Route::post('get-cursos-matricula', 'CursosController@getCursosMatricula');
//fin curso//

//incripciones//
    Route::get('get-inscripciones', 'InscripcionsController@getInscripciones');
    Route::post('crear-inscripcion', 'InscripcionsController@crearInscripcion');
    Route::post('actualizar-inscripcion', 'InscripcionsController@actualizarInscripcion');
    Route::post('buscar-inscripcion', 'InscripcionsController@buscarInscripcion');
    Route::post('calificar', 'InscripcionsController@calificacion'); //entrenador
    Route::post('confirmarAsistencia', 'InscripcionsController@asistencia'); //entrenador
    Route::post('certificar', 'InscripcionsController@certificar'); // admin
    Route::post('reportar', 'InscripcionsController@reportar'); // admin

    Route::get('descargar_certificado/{id}', 'InscripcionsController@descargarCertificado');
    Route::post('descargable', 'InscripcionsController@descargable');
//fin inscripciones//

    Route::get('getCities', 'LocalidadController@getCity');
    Route::get('getStates', 'LocalidadController@getStates');
    Route::get('getStatesOfCity', 'LocalidadController@getStatesOfCity');

// fin metodos
});
