<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Noticias_vistas extends Model
{
    use Notifiable;

    protected $fillable = [
        'like', 'noticia_id', 'user_id'
    ];


    public function usuario()
    {
        return $this->belongsTo(User::class,"user_id");
    }

    public function noticia()
    {
        return $this->belongsTo(Noticias::class,"noticia_id");
    }

}
