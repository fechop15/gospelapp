<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_certificados extends Model
{
    //
    protected $fillable = [
        'pdf','curso_id',
    ];

    public function curso()
    {
        return $this->belongsTo(Curso::class,'curso_id');
    }

}
