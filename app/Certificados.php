<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificados extends Model
{
    //
    protected $fillable = [
        'estado','inscripcion_id','detalle_certificado_id','user_id'
    ];

    public function matricula()
    {
        return $this->belongsTo(Inscripcion::class,'inscripcion_id');
    }

    public function detalle()
    {
        return $this->belongsTo(Detalle_certificados::class,"detalle_certificado_id");
    }

    public function usuario()
    {
        return $this->belongsTo(User::class,"user_id");
    }
}
