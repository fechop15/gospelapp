<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    protected $fillable = [
        'tipo_documento', 'documento', 'nombres', 'tipo_sangre',
        'apellidos', 'telefono', 'fecha_nacimiento', 'sexo', 'ocupacion', 'city_id', 'user_id', 'empresa_id'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function city()
    {
        return $this->belongsTo(Cities::class);
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function matriculas()
    {
        return $this->hasMany(Inscripcion::class);
    }

}
