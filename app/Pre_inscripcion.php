<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pre_inscripcion extends Model
{

    protected $fillable = [
        'nombres', 'apellidos', 'tipo_documento', 'documento',
        'telefono', 'cargo', 'email','curso','estado', 'empresa_id','nombre_empresa'
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

}
