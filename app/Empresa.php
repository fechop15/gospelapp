<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    protected $fillable = [
        'nombre','nit','sector', 'contacto', 'telefono', 'estado','user_id'
    ];

    public function personas()
    {
        return $this->hasMany(Persona::class);
    }

    public function pre_inscripciones()
    {
        return $this->hasMany(Pre_inscripcion::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class,"user_id");
    }

}
