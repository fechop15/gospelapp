<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Invitado extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *     protected $fillable = [
    'nombre', 'email', 'password', 'telefono', 'rol_id', 'cedula', 'estado', 'ganancia','sueldo','tipoPago',
    ];
     */

    protected $fillable = [
        'id', 'nombres','apellidos','telefono','direccion','oracion','estado','user_id'
    ];


    public function registrador()
    {
        return $this->belongsTo(User::class,"user_id");
    }

}
