<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Noticias extends Model
{
    use Notifiable;

    protected $fillable = [
        'id', 'titulo', 'imagen','contenido','fecha','estado','tipo','user_id'
    ];


    public function autor()
    {
        return $this->belongsTo(User::class,"user_id");
    }

    public function visitas()
    {
        return $this->hasMany(Noticias_vistas::class,'noticia_id');
    }

}
