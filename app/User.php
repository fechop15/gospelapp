<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *     protected $fillable = [
    'nombre', 'email', 'password', 'telefono', 'rol_id', 'cedula', 'estado', 'ganancia','sueldo','tipoPago',
    ];
     */
    protected $fillable = [
        'email', 'rol_id', 'estado','password','last_login','imagen','api_token','notificacion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol()
    {
        return $this->belongsTo(Rol::class);
    }

    public function persona()
    {
        return $this->hasOne(Persona::class);
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class);
    }
}
