<?php

namespace App\Http\Controllers;

use App\Certificados;
use App\Curso;
use App\Inscripcion;
use App\Noticias;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class DashboardController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        if (auth()->user()->rol_id==4 || auth()->user()->rol_id==5){
            return redirect('/dashboard/perfil');
        }else if(auth()->user()->rol_id==3){
            return redirect('/dashboard/cursos');

        }else {
            $usuarios = User::all();
            $cursos = Curso::all();
            $certificados = Certificados::all();
            return view('nueva.page.inicio',compact('usuarios', 'cursos', 'certificados'));

            //return view('page.inicio', compact('usuarios', 'cursos', 'certificados'));
        }
    }

    public function estudiantes()
    {
        return view('nueva.page.estudiantes');
    }

    public function blank()
    {
        return view('page.blank');
    }

    public function cursos()
    {
        return view('nueva.page.cursos');
    }

    public function curso($id)
    {
        $curso = Curso::findOrFail($id);
        if (auth()->user()->rol_id==3){
            if ($curso->entrenador_id==auth()->user()->id){
                return view('nueva.page.curso',compact('curso'));
            }else{
                return redirect('/');
            }
        }
        return view('nueva.page.curso',compact('curso'));
    }

    public function matriculas()
    {
        return view('nueva.page.matriculas');
    }

    public function pre_inscripciones()
    {
        return view('nueva.page.pre_inscripciones');
    }

    public function perfil($id = null)
    {
        if ($id == null) {
            $estudiante = auth()->user();
            if ($estudiante->empresa==null) {
                return view('nueva.page.cliente', compact('estudiante'));
            }else{
                $empresa=$estudiante->empresa;
                return view('nueva.page.empresa', compact('empresa'));
            }
        } else {
            $estudiante = User::findOrFail($id);
            if ($estudiante->empresa==null){
                return view('nueva.page.cliente', compact('estudiante'));
            }else{
                $empresa=$estudiante->empresa;
                return view('nueva.page.empresa', compact('empresa'));
            }
        }
    }

    public function usuarios()  
    {
        return view('nueva.page.usuarios');
    }

    public function empresas()
    {
        return view('nueva.page.empresas');
    }

    public function pre_inscripcion()
    {
        return view('nueva.page.pre_inscripcion');
    }

    public function certificados()
    {
        $matriculas = Inscripcion::where('fecha_certificacion','!=',null)->get();

        return view('nueva.page.certificados', compact('matriculas'));
    }

    public function consulta_externa($id = null)
    {
        if ($id == null) {
            return view('nueva.page.consultaExterna');
        }else{
            try{
                $decrypted = Crypt::decryptString($id);
                $inscripcion = Inscripcion::find($decrypted);
                return view('nueva.page.consultaExterna',compact('inscripcion'));

            } catch (\Exception $e) {
                return redirect('/consulta_externa/');
            }

        }

    }

    public function noticias(){
        return view('nueva.page.noticias');
    }

}
