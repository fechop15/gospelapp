<?php

namespace App\Http\Controllers;

use App\Certificados;
use App\Detalle_certificados;
use App\Inscripcion;
use App\Persona;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Mpdf\Mpdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InscripcionsController extends Controller
{
    //
    function getInscripciones()
    {
        $inscripciones = Inscripcion::all();
        $array = array();

        foreach ($inscripciones as $inscripcion) {

            array_push($array, $this->datos($inscripcion));

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearInscripcion(Request $request)
    {
        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'
        $this->validate($request, [
            'pago' => 'required|string',
            'curso_id' => 'required|numeric',
            'persona_id' => 'required|numeric',
            'documentado' => 'required|numeric',
        ]);

        $error = null;
        DB::beginTransaction();
        try {
            $inscripciones = Inscripcion::where("curso_id","=",$request->curso_id)
                ->where("persona_id","=",$request->persona_id)->get();
            foreach ($inscripciones as $inscripcion) {

                $error="El Estudiante se encuentra matriculado en el curso seleccionado";
                $success=false;

            }

            if($error==null){
                //descargable si viene con una empresa
                $inscripcion = Inscripcion::create([
                    'pago' => $request->pago,
                    'curso_id' => $request->curso_id,
                    'persona_id' => $request->persona_id,
                    'documentado' => $request->documentado,
                ]);
                DB::commit();
                    if ($inscripcion->persona->empresa_id!=null){
                        $inscripcion->descargable=false;
                        $inscripcion->save();
                    }


                $success = true;
            }

        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error

        return response()->json($response);

    }

    function actualizarInscripcion(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $this->validate($request, [
            'pago' => 'required|numeric',
            'estado' => 'required|numeric',
            'documentado' => 'required|numeric',
        ]);

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->update($request->all());

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function calificacion(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $this->validate($request, [
            'nota_teorica' => 'required|numeric|max:100',
            'nota_practica' => 'required|numeric|max:100',
        ]);

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->update($request->all());

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function asistencia(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $this->validate($request, [
            'asistencia_validada' => 'required|numeric',
        ]);

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->update($request->all());

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function certificar(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->fecha_certificacion=Carbon::now()->format('Y-m-d');
            $detallePDF = Detalle_certificados::where('curso_id', '=', $inscripcion->curso->id)->first();

            //generar certificado en la tabla
            $certificado=Certificados::create([
                'inscripcion_id'=>$inscripcion->id,
                'detalle_certificado_id'=>$detallePDF->id,
                'user_id'=>auth()->user()->id
            ]);

            if($inscripcion->persona->empresa_id==null){
                $inscripcion->descargable=true;
            }else{
                $inscripcion->descargable=false;
            }

            $inscripcion->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function reportar(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->fecha_reporte_ministerio=Carbon::now()->format('Y-m-d');
            $inscripcion->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function descargable(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->descargable=$request->estado;
            $inscripcion->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $inscripcion->descargable?'Si':'No'
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function descargarCertificado(Request $request){


        $inscripcion = Inscripcion::findOrFail($request->id);

        //validacion
        if (auth()->user()->rol_id==5){
            if (auth()->user()->persona->id!=$inscripcion->persona->id ||
                $inscripcion->estado==false ||
                $inscripcion->fecha_certificacion==null){
                return abort(404);
            }
        }


        $rutapdf = $inscripcion->certificado->detalle->pdf; //"plantillas/plantilla1.pdf";
        $filename = "Certificado_".$inscripcion->persona->nombres.".pdf";
        $filename = str_replace(" ", "_", $filename);

        $tempDir = 'temp/';
        $qrfileName = $inscripcion->id.'_codigoqr.png';
        $pngAbsoluteFilePath = $tempDir.$qrfileName;

        // generating
        $url=$request->root();
        if (!file_exists($pngAbsoluteFilePath)) {
            QrCode::generate($url.'/consulta_externa/'.Crypt::encryptString($inscripcion->id), $pngAbsoluteFilePath);
        }

        //$mpdf = new mPDF(['utf-8', [210,310], 'Arial Bold', 3, 3, 3, 3, 9, 9, 'L']);
        $mpdf = new mPDF([
            'format' => [210, 310],
            'orientation' => 'L',
            'default_font_size' => '30',
            'margin_left' => '3',
            'margin_right' => '3',
            'margin_top' => '3',
            'margin_bottom' => '3',
            'margin_header' => '9',
            'margin_footer' => '9',
        ]);

        //$mpdf = new mPDF();

        //datos del certificado//
        $anioCurso = date("Y", strtotime($inscripcion->curso->fecha_fin_curso));
        $mesCurso = date("n", strtotime($inscripcion->curso->fecha_fin_curso));
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $diasTranscurridos=$this->devuelveArrayFechasEntreOtrasDos($inscripcion->curso->fecha_inicio_curso,$inscripcion->curso->fecha_fin_curso);
        $mesesTranscurridos=$this->devuelveArrayMesesEntreOtrasDos($inscripcion->curso->fecha_inicio_curso,$inscripcion->curso->fecha_fin_curso);
        $mesesCurso=array();
        foreach ($mesesTranscurridos as $mes){
            array_push($mesesCurso,$meses[$mes-1]);
        }

        ////

        $mpdf->SetImportUse();

        $pagecount = $mpdf->SetSourceFile($rutapdf);

        $tplId = $mpdf->ImportPage(1);
        $mpdf->UseTemplate($tplId);

        $mpdf->WriteFixedPosHTML("<div style='text-align: center;'>".$inscripcion->persona->nombres." ".$inscripcion->persona->apellidos."</div>", 35, 95, 310, 90, 'auto');
        $mpdf->WriteFixedPosHTML("<div style='text-align: center; font-size: 12pt; font-family: Times, serif;'>".
            "Con número de identificación ".number_format($inscripcion->persona->documento, 0, ',', '.').
            "</div>", 30, (95+14), 310, 90, 'auto');
        $mpdf->Image($pngAbsoluteFilePath, 266, 167, 35, 35, 'png', '');

        $mpdf->WriteFixedPosHTML("<div style='font-size: 20pt; font-family: Times, serif;'>".
            $inscripcion->curso->nivel.
            "</div>", 150, 125, 310, 90, 'auto');

        $mpdf->WriteFixedPosHTML("<div style='text-align: center; font-size: 12pt; font-family: Times, serif;'>".
            "Realizado en ".$inscripcion->curso->ciudad.", los días ".$inscripcion->curso->dias." de ".implode(" y ",$mesesCurso)." de $anioCurso, con una intensidad de ".$inscripcion->curso->intensidad." horas".
            "</div>", 30, 135, 310, 90, 'auto');

        $mpdf->WriteFixedPosHTML("<div style='font-size: 13pt; font-family: Times, serif;'>".
            date("d/m/Y", strtotime($inscripcion->fecha_certificacion)).
            "</div>", 210, 192, 30, 90, 'auto');

        $mpdf->WriteFixedPosHTML("<div style='font-size: 18pt; font-family: Times, serif;'>".
            "Reg. Nº ".str_pad($inscripcion->certificado->id, 4, '0', STR_PAD_LEFT).
            "</div>", 260, 7, 60, 90, 'auto');

        $mpdf->Output($filename,'I');
    }

    function buscarInscripcion(Request $request)
    {
        $inscripcion = Inscripcion::findOrFail($request->id);

        $datos = [
            'id' => $inscripcion->id,
            'nombre' => $inscripcion->persona->nombres." ".$inscripcion->persona->apellidos,
            'documento' => $inscripcion->persona->documento,
            'telefono' => $inscripcion->persona->telefono,
            'correo' => $inscripcion->persona->usuario->email,
            'fecha_inicio' => $inscripcion->curso->fecha_inicio_curso,
            'fecha_fin' => $inscripcion->curso->fecha_fin_curso,
            'curso' => $inscripcion->curso->nombre."-".$inscripcion->curso->nivel ,
            'documentado' => $inscripcion->documentado,
            'pago' => $inscripcion->pago,
            'estado' => $inscripcion->estado,
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);
    }

    function consultar(Request $request){

        $this->validate($request, [
            'documento' => 'required|numeric',
        ]);

        $persona = Persona::where('documento','=',$request->documento)->first();
        if($persona==null){
            $response = array(
                'status' => 'Error',
                'msg' => "Esta Persona No Se encontra Registrada",
            );
        }elseif ($persona->matriculas==null){
            $response = array(
                'status' => 'Error',
                'msg' => "No Se encontraron Cursos Realizados",
            );
        }else{
            $array = array();

            foreach ($persona->matriculas as $inscripcion) {
                if ($inscripcion->estado==1 && $inscripcion->fecha_certificacion!=null && $inscripcion->pago){
                    array_push($array, $this->datosPublicos($inscripcion));
                }

            }

            $response = array(
                'status' => 'success',
                'msg' => $array,
                'nombre' =>$persona->nombres." ".$persona->apellidos,
            );

        }

        return response()->json($response);

    }

    public function datos($inscripcion)
    {
        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'
        $datos = [
            'id' => $inscripcion->id,
            'estado' => $inscripcion->estado,
            'pago' => $inscripcion->pago,
            'documentado' => $inscripcion->documentado,
            'nota_teorica' => $inscripcion->nota_teorica,
            'nota_practica' => $inscripcion->nota_practica,
            'fecha_certificacion' => $inscripcion->fecha_certificacion,
            'fecha_reporte_ministerio' => $inscripcion->fecha_reporte_ministerio,
            'asistencia_validada' => $inscripcion->asistencia_validada,
            'curso' => $inscripcion->curso,
            'persona' => $inscripcion->persona,
            'fecha' => $inscripcion->created_at->format('Y-m-d H:i:s' ),
        ];
        return $datos;
    }

    public function datosPublicos($inscripcion)
    {

        $datos = [
            'fecha_certificacion' => $inscripcion->fecha_certificacion,
            'curso' => $inscripcion->curso->nombre." NIVEL ".$inscripcion->curso->nivel,
            'cod' => "IND".str_pad($inscripcion->certificado->id, 4, '0', STR_PAD_LEFT),
        ];
        return $datos;
    }

    function devuelveArrayFechasEntreOtrasDos($fechaInicio, $fechaFin)
    {
        $arrayFechas=array();
        $fechaMostrar = $fechaInicio;

        while(strtotime($fechaMostrar) <= strtotime($fechaFin)) {
            $arrayFechas[]=date("d",strtotime($fechaMostrar));
            $fechaMostrar = date("d-m-Y", strtotime($fechaMostrar . " + 1 day"));

        }

        return $arrayFechas;


    }

    function devuelveArrayMesesEntreOtrasDos($fechaInicio, $fechaFin)
    {
        $arrayFechas=array();
        $fechaMostrar = $fechaInicio;

        while(strtotime($fechaMostrar) <= strtotime($fechaFin)) {
            $arrayFechas[]=date("m",strtotime($fechaMostrar));
            $fechaMostrar = date("d-m-Y", strtotime($fechaMostrar . " + 1 day"));

        }

        return array_unique($arrayFechas);


    }
}
