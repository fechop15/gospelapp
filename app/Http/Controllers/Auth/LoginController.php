<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('nueva.page.login');
    }

    public function login(Request $request)
    {
        $credenciales = $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $usuario = User::where('email', $request->email)->first();

        if (Auth::attempt($credenciales)) {
            if (($usuario->estado == 1)) {
                $usuario->last_login = new DateTime;
                $usuario->save();
                if ($usuario->rol->id >=4)
                    return redirect()->route('perfil');
                else
                    return redirect()->route('dashboard');

            } else {
                Auth::logout();
                return back()
                    ->withErrors(['loginError' => "Usuario inactivo, pongase en contacto con el administrador"])
                    ->withInput($credenciales);
            }
        }

        return back()
            ->withErrors(['loginError' => trans('auth.failed')])
            ->withInput($credenciales);
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

}
