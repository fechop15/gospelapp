<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Detalle_certificados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CursosController extends Controller
{
    //
    function getCursos()
    {
        if (auth()->user()->rol_id==3){
            $cursos = Curso::where("entrenador_id","=",auth()->user()->id)->get();

        }else{
            $cursos = Curso::all();

        }
        $array = array();

        foreach ($cursos as $curso) {

            array_push($array, $this->datos($curso));

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getCursosMatricula()
    {
        $cursos = Curso::where('estado', '=', '1')->orderBy('id', 'desc')->get();
        $array = array();

        foreach ($cursos as $curso) {
            if ($curso->fecha_inicio_curso >= now()->toDateString() || $curso->matriculados->count() < $curso->cupo) {
                array_push($array, $this->datos($curso));
            }

        }
        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearCurso(Request $request)
    {
        //        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
        //        'fecha_inicio_curso','fecha_fin_curso','estado','entrenador_id'
        $this->validate($request, [
            'nombre' => 'required|string',
            'nivel' => 'required|string',
            'intensidad' => 'required|numeric',
            'cupos' => 'required|numeric',
            'fecha_inicio_inscripciones' => 'required|date',
            'fecha_fin_inscripciones' => 'required|date',
            'fecha_inicio_curso' => 'required|date',
            'fecha_fin_curso' => 'required|date',
            'certificado' => 'required',
            'entrenador_id' => 'required|numeric',
            'supervisor' => 'required|string',
            'ciudad' => 'required|string',
            'dias' => 'required|string',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $curso = Curso::create([
                'nombre' => $request->nombre,
                'nivel' => $request->nivel,
                'supervisor' => $request->supervisor,
                'intensidad' => $request->intensidad,
                'cupos' => $request->cupos,
                'fecha_inicio_inscripciones' => $request->fecha_inicio_inscripciones,
                'fecha_fin_inscripciones' => $request->fecha_fin_inscripciones,
                'fecha_inicio_curso' => $request->fecha_inicio_curso,
                'fecha_fin_curso' => $request->fecha_fin_curso,
                'entrenador_id' => $request->entrenador_id,
                'ciudad' => $request->ciudad,
                'dias' => $request->dias,
            ]);

            $name = $curso->id . '.pdf';
            $path = "plantillas/";

            move_uploaded_file($request->file('certificado'),$path.$name);

            //$request->file('certificado')->storeAs($path, $name);

            $detallePDF = Detalle_certificados::create([
                'pdf' => $path.$name,
                'curso_id' => $curso->id
            ]);


            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($curso),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function actualizarCurso(Request $request)
    {

        $curso = Curso::findOrFail($request->id);
        $this->validate($request, [
            'nombre' => 'required|string',
            'nivel' => 'required|string',
            'supervisor' => 'required|string',
            'intensidad' => 'required|numeric',
            'cupos' => 'required|numeric',
            'fecha_inicio_inscripciones' => 'required|date',
            'fecha_fin_inscripciones' => 'required|date',
            'fecha_inicio_curso' => 'required|date|',
            'fecha_fin_curso' => 'required|date',
            'entrenador_id' => 'required|numeric',
            'estado' => 'required|numeric',
            'ciudad' => 'required|string',
            'dias' => 'required|string',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $curso->update($request->all());

            if($request->file('Acertificado')){
                $name = $curso->id . '.pdf';
                $path = "plantillas/";

                move_uploaded_file($request->file('Acertificado'),$path.$name);

                //$request->file('Acertificado')->storeAs($path, $name);

                $detallePDF = Detalle_certificados::where('curso_id', '=', $curso->id)->first();

                $detallePDF->pdf=$path.$name;

                $detallePDF->save();

            }

            DB::commit();
            $success = true;


        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($curso),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
                'pat' => $detallePDF,
            );
        }//error


        return response()->json($response);

    }

    function buscarCurso(Request $request)
    {
        $curso = Curso::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($curso),
        );

        return response()->json($response);
    }

    public function datos($curso)
    {
        //        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
        //        'fecha_inicio_curso','fecha_fin_curso','estado','entrenador_id'

        $datos = [
            'id' => $curso->id,
            'nombre' => $curso->nombre,
            'nivel' => $curso->nivel,
            'supervisor' => $curso->supervisor,
            'ciudad' => $curso->ciudad,
            'dias' => $curso->dias,
            'mes' => $curso->mes,
            'intensidad' => $curso->intensidad,
            'cupos' => $curso->cupos,
            'matriculados' => $curso->matriculados->where("estado","=",true)->count(),
            'fecha_inicio_inscripciones' => $curso->fecha_inicio_inscripciones,
            'fecha_fin_inscripciones' => $curso->fecha_fin_inscripciones,
            'fecha_inicio_curso' => $curso->fecha_inicio_curso,
            'fecha_fin_curso' => $curso->fecha_fin_curso,
            'estado' => $curso->estado,
            'entrenador' => $curso->entrenador,
        ];
        return $datos;
    }
}
