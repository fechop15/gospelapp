<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Pre_inscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Pre_inscripcionController extends Controller
{
    //
    function getPreinscripciones(Request $request)
    {
        if (auth()->user()->empresa==null){
            if ($request->id){
                $pre_inscripciones = Pre_inscripcion::where('estado', '=', true)->
                where('empresa_id', '=', $request->id)->get();
            }else{
                $pre_inscripciones = Pre_inscripcion::where('estado', '=', true)->get();

            }

        }else{
            $pre_inscripciones = Pre_inscripcion::where('estado', '=', true)->
            where('empresa_id', '=', auth()->user()->empresa->id)->get();

        }

        $array = array();

        foreach ($pre_inscripciones as $pre_inscripcion) {

            array_push($array, $this->datos($pre_inscripcion));

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearPreinscripcion(Request $request)
    {
        //        'nombres', 'apellidos', 'tipo_documento', 'documento',
        //        'telefono', 'cargo', 'email','curso','estado', 'empresa_id'

        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'tipo_documento' => 'required|string',
            'documento' => 'required|numeric',
            'telefono' => 'required|numeric',
            'email' => 'required|string|email',
            'curso' => 'required|string',
            'cargo' => 'required|string',
        ]);
        $error = null;
        DB::beginTransaction();
        try {
        if(auth()->user()==null){

            $pre_inscripcion = Pre_inscripcion::create([
                'nombres' => $request->nombres,
                'apellidos' => $request->apellidos,
                'tipo_documento' => $request->tipo_documento,
                'documento' => $request->documento,
                'cargo' => $request->cargo,
                'telefono' => $request->telefono,
                'email' => $request->email,
                'curso' => $request->curso,
                'empresa_id' => $request->empresa_id,
            ]);

        }else if (auth()->user()->empresa == null) {
                $pre_inscripcion = Pre_inscripcion::create([
                    'nombres' => $request->nombres,
                    'apellidos' => $request->apellidos,
                    'tipo_documento' => $request->tipo_documento,
                    'documento' => $request->documento,
                    'cargo' => $request->cargo,
                    'telefono' => $request->telefono,
                    'email' => $request->email,
                    'curso' => $request->curso,
                    'nombre_empresa' => "",
                ]);
            } else if ($request->empresa_id!=null){
                    $pre_inscripcion = Pre_inscripcion::create([
                        'nombres' => $request->nombres,
                        'apellidos' => $request->apellidos,
                        'tipo_documento' => $request->tipo_documento,
                        'documento' => $request->documento,
                        'cargo' => $request->cargo,
                        'telefono' => $request->telefono,
                        'email' => $request->email,
                        'curso' => $request->curso,
                        'empresa_id' => $request->empresa_id,
                    ]);
                }else{
                    $pre_inscripcion = Pre_inscripcion::create([
                        'nombres' => $request->nombres,
                        'apellidos' => $request->apellidos,
                        'tipo_documento' => $request->tipo_documento,
                        'documento' => $request->documento,
                        'cargo' => $request->cargo,
                        'telefono' => $request->telefono,
                        'email' => $request->email,
                        'curso' => $request->curso,
                        'empresa_id' => auth()->user()->empresa->id,
                    ]);
                }

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($pre_inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function buscarPreinscripcion(Request $request)
    {
        $pre_inscripcion = Pre_inscripcion::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($pre_inscripcion),
        );

        return response()->json($response);
    }

    function terminarPreinscripcion(Request $request)
    {
        $pre_inscripcion = Pre_inscripcion::findOrFail($request->id);
        $pre_inscripcion->estado = false;
        $pre_inscripcion->save();
        $response = array(
            'status' => 'success',
            'msg' => $this->datos($pre_inscripcion),
        );

        return response()->json($response);
    }

    public function datos($pre_inscripcion)
    {
        //        'nombres', 'apellidos', 'tipo_documento', 'documento',
        //        'telefono', 'cargo', 'email','curso','estado', 'empresa_id'
        $persona = Persona::where('documento', '=', $pre_inscripcion->documento)->first();

        $datos = [
            'id' => $pre_inscripcion->id,
            'nombres' => $pre_inscripcion->nombres,
            'apellidos' => $pre_inscripcion->apellidos,
            'nombre' => $pre_inscripcion->nombres . " " . $pre_inscripcion->apellidos,
            'tipo_documento' => $pre_inscripcion->tipo_documento,
            'documento' => $pre_inscripcion->documento,
            'telefono' => $pre_inscripcion->telefono,
            'ocupacion' => $pre_inscripcion->cargo,
            'email' => $pre_inscripcion->email,
            'curso' => $pre_inscripcion->curso,
            'nombre_empresa' => $pre_inscripcion->empresa==null?$pre_inscripcion->nombre_empresa:$pre_inscripcion->empresa->nombre,
            'empresa' => $pre_inscripcion->empresa,
            'persona' => $persona,
        ];
        return $datos;
    }

}
