<?php

namespace App\Http\Controllers;

use App\Cities;
use App\States;
use Illuminate\Http\Request;

class LocalidadController extends Controller
{
    //

    function getStates(){
        $state = States::where('country_id','=','47')->get();

        $response = array(
            'status' => 'success',
            'msg' => $state,
        );

        return response()->json($response);
    }

    function getCity(Request $request){

        $cities = Cities::where('state_id','=',$request->state)->get();
        $response = array(
            'status' => 'success',
            'msg' => $cities,
        );

        return response()->json($response);
    }

    function getStatesOfCity(Request $request){
        $city = Cities::where('id','=',$request->city)->get();

        $response = array(
            'status' => 'success',
            'msg' => $city->state,
        );

        return response()->json($response);
    }
}
