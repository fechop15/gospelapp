<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Invitado;
use App\Noticias;
use App\Noticias_vistas;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

class NoticiasController extends Controller
{

    function getNoticias(Request $request)
    {
        $noticias = Noticias::all();
        $array = array();

        foreach ($noticias as $noticia) {

            array_push($array, $this->datos($noticia, $request->user()->id));

        }

        $response = array(
            'success' => true,
            'noticias' => $array,
        );

        return response()->json($response);
    }

    function crearNoticia(Request $request)
    {

        $this->validate($request, [
            'titulo' => 'required|string',
            'contenido' => 'required',
            'tipo' => 'required|string',
        ]);
        $noticia = null;
        $error = null;
        DB::beginTransaction();
        try {

            $noticia = Noticias::create([
                'titulo' => $request->titulo,
                //'imagen' => $request->imagen,
                'contenido' => $request->contenido,
                //'fecha' => $request->fecha,
                'tipo' => $request->tipo,
                'user_id' => $request->user()->id,
            ]);

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            //https://github.com/ixudra/curl
            $usuarios = User::whereNotNull('notificacion')->get();
            $array = array();
            foreach ($usuarios as $usuario) {
                $datos = [
                    "to" => $usuario->notificacion,
                    "title" => $request->tipo.' GospelApp',
                    "body" => $noticia->titulo
                ];
                array_push($array, $datos);
            }
            $respuesta_notificacion='';
            if (count($array)>0){
            $respuesta_notificacion = Curl::to('https://exp.host/--/api/v2/push/send')
                ->withData($array)
                ->asJson(true)
                ->post();
            }

            $response = array(
                'success' => true,
                'message' => 'Noticia creada con exito.',
                'message2' => $respuesta_notificacion,
            );
        } else {

            $response = array(
                'success' => false,
                'message' => $error,
            );
        }//error


        return response()->json($response);

    }

    function actualizarNoticia(Request $request)
    {
        $noticia = Noticias::findOrFail($request->id);

        $this->validate($request, [
            'titulo' => 'required|string',
            'contenido' => 'required',
            'tipo' => 'required|string',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $noticia->update($request->all());

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'success' => true,
                'message' => 'Noticia Actualizada con exito',
            );
        } else {

            $response = array(
                'success' => false,
                'message' => $error,
            );
        }//error


        return response()->json($response);

    }

    function eliminarNoticia(Request $request)
    {

        $noticia = Noticias::findOrFail($request->id);
        foreach ($noticia->visitas as $visita) {
            $visita->delete();
        }
        $noticia->delete();
        $response = array(
            'success' => true,
            'message' => 'Noticia eliminada con exito.',
        );
        return response()->json($response);

    }

    public function datos($noticia, $idUsuario)
    {
        $noticiaVisitada = Noticias_vistas::where('noticia_id', '=', $noticia->id)
            ->where('user_id', '=', $idUsuario)->get();
        $datos = [
            'id' => $noticia->id,
            'titulo' => $noticia->titulo,
            'imagen' => $noticia->imagen,
            'contenido' => $noticia->contenido,
            'fecha' => $noticia->fecha == null ? $noticia->created_at->format('Y-m-d') : $noticia->fecha,
            'estado' => $noticia->estado,
            'tipo' => $noticia->tipo,
            'autor' => $noticia->autor->persona,
            'visto' => $noticiaVisitada == null ? false : true,
            'leGusta' => $noticiaVisitada ? false : $noticiaVisitada->like,
            'vistos' => $noticia->visitas ? $noticia->visitas->count() : 0,
            'like' => $noticia->visitas ? $noticia->visitas->where('like', true)->count() : 0,
        ];
        return $datos;

    }
}
