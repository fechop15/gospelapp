<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Invitado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvitadosController extends Controller
{

    function getInvitados(Request $request)
    {
        $invitados = Invitado::where('estado',$request->estado)->orderBy('id', 'desc')->get();
        $array = array();

        foreach ($invitados as $invitado) {

            array_push($array, $this->datos($invitado));

        }

        $response = array(
            'success' => true,
            'invitados' => $array,
        );

        return response()->json($response);
    }

    function misInvitados(Request $request){
        $invitados = Invitado::where('user_id',$request->user()->id)->get();
        $array = array();

        foreach ($invitados as $invitado) {

            array_push($array, $this->datos($invitado));

        }

        $response = array(
            'success' => true,
            'invitados' => $array,
        );

        return response()->json($response);
    }

    function crearInvitado(Request $request)
    {

        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $invitado = Invitado::create([
                'nombres' => $request->nombres,
                'apellidos' => $request->apellidos,
                'telefono' => $request->telefono!=''?$request->telefono:null,
                'direccion' => $request->direccion!=''?$request->direccion:null,
                'oracion' => $request->oracion!=''?$request->oracion:null,
                'user_id' => $request->user()->id,
            ]);

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'success' => true,
                'message' => 'Invitado registrado con exito.',
            );
        } else {

            $response = array(
                'success' => false,
                'message' => $error,
            );
        }//error


        return response()->json($response);

    }

    function confirmarInvitado(Request $request)
    {

        $invitado = Invitado::findOrFail($request->id);

        $error = null;
        DB::beginTransaction();
        try {
            $invitado->estado=true;
            $invitado->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'success' => true,
                'message' => 'Invitado confirmado con exito.',
            );
        } else {

            $response = array(
                'status' => false,
                'message' => $error,
            );
        }//error


        return response()->json($response);

    }

    function eliminarInvitado(Request $request){

        $invitado = Invitado::findOrFail($request->id);
        $invitado->delete();
        $response = array(
            'success' => true,
            'message' => 'Invitado eliminado con exito.',
        );
        return response()->json($response);

    }

    public function datos($invitado)
    {

        $datos = [
            'id' => $invitado->id,
            'nombres' => $invitado->nombres,
            'apellidos' => $invitado->apellidos,
            'telefono' => $invitado->telefono,
            'direccion' => $invitado->direccion,
            'oracion' => $invitado->oracion,
            'estado' => $invitado->estado,
            'invitador' => $invitado->registrador->persona->nombres.' '.$invitado->registrador->persona->apellidos,
            'imagen' => $invitado->registrador->imagen,
            'fecha_registro' => $invitado->created_at->format('Y-m-d H:i:s' ),

        ];
        return $datos;

    }
}
