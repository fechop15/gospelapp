<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{

    public function login(Request $request)
    {
        $credenciales = $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $usuario = User::where('email', $request->email)->first();

        if (Auth::attempt($credenciales)) {
            if (($usuario->estado == 1)) {
                $usuario->last_login = new DateTime;
                $usuario->save();

                $token = Str::random(60);
                $usuario->forceFill([
                    'api_token' => $token,
                ])->save();

                //retorno
                $response = array(
                    'success' => true,
                    'userData' => $this->datos($usuario),
                );

                return response()->json($response);

            } else {
                $usuario->api_token=null;
                $usuario->save();
                //Auth::logout();
                $response = array(
                    'success' => false,
                    'message' => "Usuario inactivo, pongase en contacto con el administrador",
                );
                return response()->json($response);
            }
        }

        $response = array(
            'success' => false,
            'message' => "Usuario y contraseña incorrectos",
        );
        return response()->json($response);
    }

    public function updateToken(Request $request)
    {
        $token = Str::random(60);

        $request->user()->forceFill([
            'api_token' => $token,
            'notificacion' => $request->notificacion==null?null:$request->notificacion,
        ])->save();

        $response = array(
            'success' => true,
            'userData' =>  $this->datos($request->user()),
        );

        return response()->json($response);
    }

    function getUsuarios()
    {
        $usuarios = User::all();
        $array = array();

        foreach ($usuarios as $usuario) {
            array_push($array, $this->datos($usuario));
        }

        $response = array(
            'success' => true,
            'usuarios' => $array,
        );

        return response()->json($response);
    }

    function actualizarUsuario(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'tipo_documento' => 'required|string',
            'documento' => 'required|numeric|unique:personas,documento,' . $usuario->persona->id,
            'ocupacion' => 'required|string',
            'telefono' => 'required|numeric',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'fecha_nacimiento' => 'required|date',
            'sexo' => 'required|string',
            'rol_id' => 'required|numeric',
            'city_id' => 'required|numeric',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $usuario->update($request->all());
            $usuario->persona->update($request->all());

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'success' => true,
                'msg' => $this->datos($usuario),
            );
        } else {

            $response = array(
                'success' => false,
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */

    function actualizarPass(Request $request)
    {

        if ($request->id) {
            $usuario = User::findOrFail($request->id);
            $this->validate($request, [
                'pass' => 'required|string|min:6',
            ]);

            if (auth()->user()->rol_id != 1 && ($usuario->rol_id == 1 || $usuario->rol_id == 2 || $usuario->rol_id == 3)) {
                $response = array(
                    'status' => 'Error',
                    'msg' => "Error, Falta de privilegios",
                );
                return response()->json($response);

            } else {
                $usuario->password = bcrypt($request->pass);
                $usuario->save();
                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );
                return response()->json($response);

            }

        } else {

            $usuario = User::findOrFail(auth()->user()->id);
            $this->validate($request, [
                'passOld' => 'required|string',
                'pass' => 'required|string|min:6',
            ]);

            if (Hash::check($request->passOld, $usuario->password)) {

                $usuario->password = bcrypt($request->pass);
                $usuario->save();

                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );

            } else {
                $response = array(
                    'status' => 'Error',
                    'status2' => Hash::check($request->passOld, $usuario->password),
                    'msg' => "la Contraseña actual no es correcta",
                );
            }

            return response()->json($response);

        }

    }

    function update_profile(Request $request)
    {

        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $path = "images/perfil/";

        $filename = Auth::id() . '_' . time() . '.' . $request->avatar->getClientOriginalExtension();
        move_uploaded_file($request->file('avatar'), $path . $filename);

        $user = Auth::user();
        $user->imagen = $filename;
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $filename,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }

    function remove_profile()
    {

        $user = Auth::user();
        $user->imagen = 'avatar-1.png';
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $user->imagen,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }

    public function datos($usuario)
    {
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        if($usuario->persona!=null){
            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->persona->nombres . " " . $usuario->persona->apellidos,
                'cedula' => $usuario->persona->documento,
                'email' => $usuario->email,
                'rol' => $usuario->rol->nombre,
                'rol_id' => $usuario->rol_id,
                'telefono' => $usuario->persona->telefono,
                'tipo_sangre' => $usuario->persona->tipo_sangre,
                'estado' => $usuario->estado,
                'municipio' => $usuario->persona->city->state_id,
                'imagen' => $usuario->imagen,
                'persona' => $usuario->persona,
                'cargo' => $usuario->cargo,
                'api_token' => $usuario->api_token,
                'fecha_nacimiento'=>date("d", strtotime($usuario->persona->fecha_nacimiento)).' de '.$meses[date("n", strtotime($usuario->persona->fecha_nacimiento))-1]
            ];
        }else{
            $datos = [
                'id' => $usuario->id,
                'nombre' => '',
                'cedula' => '',
                'email' => $usuario->email,
                'rol' => '',
                'rol_id' => '',
                'telefono' => '',
                'tipo_sangre' => '',
                'estado' => '',
                'municipio' => '',
                'imagen' => $usuario->imagen,
                'persona' => '',
                'cargo' => $usuario->cargo,
                'api_token' => $usuario->api_token,
                'fecha_nacimiento'=>''
            ];
        }
        return $datos;
    }

}
