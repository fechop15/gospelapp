<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Noticias;
use App\Noticias_vistas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoticiasController extends Controller
{

    function getNoticias(Request $request)
    {
        $noticias = Noticias::where('tipo', $request->tipo)->where('estado', true)->orderBy('id', 'desc')->get();
        $array = array();

        foreach ($noticias as $noticia) {

            array_push($array, $this->datos($noticia,$request->user()->id));

        }

        $response = array(
            'success' => true,
            'noticias' => $array,
        );

        return response()->json($response);
    }

    function ultimaNoticia(Request $request){
        $noticia = Noticias::where('tipo', 'predica')->where('estado', true)->orderBy('id', 'desc')->first();
        $array = array();
        array_push($array, $this->datos($noticia,$request->user()->id));
        $response = array(
            'success' => true,
            'noticia' => $array,
        );

        return response()->json($response);
    }

    function visitaNoticia(Request $request)
    {
        //recibe idNoticia y like opcional
        $mensaje="NADA";
        $error = null;
        DB::beginTransaction();
        //buscar si ya visito
        $visitaNoticia = Noticias_vistas::where('noticia_id', '=', $request->idNoticia)->
        where('user_id', '=', $request->user()->id)->first();
        try {

            if (!$visitaNoticia) {
                Noticias_vistas::create([
                    'noticia_id' => $request->idNoticia,
                    'user_id' => $request->user()->id,
                ]);
                $mensaje="GUARDADO";
            }

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'success' => true,
                'message' => $mensaje,
            );
        } else {

            $response = array(
                'success' => false,
                'message' => $error,
            );
        }//error


        return response()->json($response);

    }

    function like(Request $request){
        Noticias_vistas::where('noticia_id', $request->idNoticia)
            ->where('user_id', $request->user()->id)
            ->update(['like' => $request->like]);
        $response = array(
            'success' => true,
            'noticia' => "Cambiado a ".$request->like,
        );

        return response()->json($response);
    }

    public function datos($noticia,$idUsuario)
    {
        $noticiaVisitada=Noticias_vistas::where('noticia_id', '=', $noticia->id)
            ->where('user_id', '=', $idUsuario)->first();
        $datos = [
            'id' => $noticia->id,
            'titulo' => $noticia->titulo,
            'contenido' => $noticia->contenido,
            'fecha' => Carbon::parse($noticia->created_at)->diffForHumans(),
            'estado' => $noticia->estado,
            'tipo' => $noticia->tipo,
            'autor' => $noticia->autor->persona,
            'imagen' => $noticia->autor->imagen,
            'like' => $noticiaVisitada==null?false:$noticiaVisitada->like,
            'vistos' => $noticia->visitas?$noticia->visitas->count():0,
            'likes' => $noticia->visitas?$noticia->visitas->where('like','=',true)->count():0,

        ];
        return $datos;

    }
}
