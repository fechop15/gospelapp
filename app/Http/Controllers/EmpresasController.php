<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpresasController extends Controller
{
    //
    function getEmpresas()
    {
        $empresas = Empresa::all();
        $array = array();

        foreach ($empresas as $empresa) {

            array_push($array, $this->datos($empresa));

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearEmpresa(Request $request)
    {

        $this->validate($request, [
            'nombre' => 'required|string',
            'sector' => 'required|string',
            'contacto' => 'required|string',
            'nit' => 'required|string|unique:empresas,nit',
            'telefono' => 'required|numeric',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $usuario = User::create([
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'rol_id' => 4,
            ]);

            $empresa= Empresa::create([
                'nombre' => $request->nombre,
                'nit' => $request->nit,
                'sector' => $request->sector,
                'contacto' => $request->contacto,
                'telefono' => $request->telefono,
                'user_id' => $usuario->id,
            ]);

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($empresa),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function actualizarEmpresa(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $this->validate($request, [
            'nombre' => 'required|string',
            'sector' => 'required|string',
            'contacto' => 'required|string',
            'nit' => 'required|string|unique:empresas,nit,'. $usuario->empresa->id,
            'telefono' => 'required|numeric',
            'email' => 'required|string|email|unique:users,email,'. $request->id,
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $usuario->update($request->all());
            $usuario->empresa->update($request->all());
            $usuario->estado=$request->estado;
            $usuario->save();
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($usuario->empresa),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function buscarEmpresa(Request $request)
    {
        $usuario = User::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($usuario->empresa),
        );

        return response()->json($response);
    }

    function verificarEmpresa(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $error = null;
        DB::beginTransaction();
        try {

            $usuario->empresa->estado=!$usuario->empresa->estado;
            $usuario->estado=$usuario->empresa->estado;
            $usuario->empresa->save();
            $usuario->save();
            DB::commit();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($usuario->empresa),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    public function datos($empresa)
    {
        //'nombre','sector', 'contacto', 'telefono', 'estado','user_id'
        $datos = [
            'id' => $empresa->usuario->id,
            'id_empresa' => $empresa->id,
            'nombre' => $empresa->nombre,
            'nit' => $empresa->nit,
            'sector' => $empresa->sector,
            'contacto' => $empresa->contacto,
            'telefono' => $empresa->telefono,
            'email' => $empresa->usuario->email,
            'estado' => $empresa->estado,
            'usuario' => $empresa->usuario,
        ];
        return $datos;
    }

}
