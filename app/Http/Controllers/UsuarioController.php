<?php

namespace App\Http\Controllers;

use App\Persona;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    function getUsuarios()
    {
        $usuarios = User::where('rol_id', '!=', '4')->
        where('rol_id', '!=', '5')->get();
        $array = array();

        foreach ($usuarios as $usuario) {

            array_push($array, $this->datos($usuario));

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearUsuario(Request $request)
    {


        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'tipo_documento' => 'required|string',
            'documento' => 'required|numeric|unique:personas,documento',
            'ocupacion' => 'required|string',
            'telefono' => 'required|numeric',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6',
            'fecha_nacimiento' => 'required|date',
            'sexo' => 'required|string',
            'rol_id' => 'required|numeric',
            'city_id' => 'required|numeric',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $usuario = user::create([
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'rol_id' => $request->rol_id,
            ]);

            if ($request->empresa_id == null || $request->empresa_id == 0) {
                $persona = Persona::create([
                    'tipo_documento' => $request->tipo_documento,
                    'documento' => $request->documento,
                    'nombres' => $request->nombres,
                    'apellidos' => $request->apellidos,
                    'telefono' => $request->telefono,
                    'fecha_nacimiento' => $request->fecha_nacimiento,
                    'sexo' => $request->sexo,
                    'ocupacion' => $request->ocupacion,
                    'city_id' => $request->city_id,
                    'user_id' => $usuario->id,
                ]);
            } else {
                $persona = Persona::create([
                    'tipo_documento' => $request->tipo_documento,
                    'documento' => $request->documento,
                    'nombres' => $request->nombres,
                    'apellidos' => $request->apellidos,
                    'telefono' => $request->telefono,
                    'fecha_nacimiento' => $request->fecha_nacimiento,
                    'sexo' => $request->sexo,
                    'ocupacion' => $request->ocupacion,
                    'city_id' => $request->city_id,
                    'user_id' => $usuario->id,
                    'empresa_id' => $request->empresa_id,
                ]);
            }


            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($usuario),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function actualizarUsuario(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'tipo_documento' => 'required|string',
            'documento' => 'required|numeric|unique:personas,documento,' . $usuario->persona->id,
            'ocupacion' => 'required|string',
            'telefono' => 'required|numeric',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'fecha_nacimiento' => 'required|date',
            'sexo' => 'required|string',
            'city_id' => 'required|numeric',
        ]);
        $error = null;
        DB::beginTransaction();
        try {

            $usuario->update($request->all());
            $usuario->persona->update($request->all());

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($usuario),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    function buscarUsuario(Request $request)
    {
        $usuario = User::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($usuario),
        );

        return response()->json($response);
    }

    function getUsuariosByRol(Request $request)
    {

        $usuarios = User::all();
        $array = array();
        foreach ($usuarios as $usuario) {
            if ($usuario->rol->nombre == $request->rol) {
                if ($usuario->persona != null)
                    array_push($array, $this->datos($usuario));
            }
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    function actualizarPass(Request $request)
    {

        if ($request->id) {
            $usuario = User::findOrFail($request->id);
            $this->validate($request, [
                'pass' => 'required|string|min:6',
            ]);

            if (auth()->user()->rol_id != 1 && ($usuario->rol_id == 1 || $usuario->rol_id == 2 || $usuario->rol_id == 3)) {
                $response = array(
                    'status' => 'Error',
                    'msg' => "Error, Falta de privilegios",
                );
                return response()->json($response);

            } else {
                $usuario->password = bcrypt($request->pass);
                $usuario->save();
                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );
                return response()->json($response);

            }

        } else {

            $usuario = User::findOrFail(auth()->user()->id);
            $this->validate($request, [
                'passOld' => 'required|string',
                'pass' => 'required|string|min:6',
            ]);

            if (Hash::check($request->passOld, $usuario->password)) {

                $usuario->password = bcrypt($request->pass);
                $usuario->save();

                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );

            } else {
                $response = array(
                    'status' => 'Error',
                    'status2' => Hash::check($request->passOld, $usuario->password),
                    'msg' => "la Contraseña actual no es correcta",
                );
            }

            return response()->json($response);

        }

    }

    function update_profile(Request $request)
    {

        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $path = "images/perfil/";

        $filename = Auth::id() . '_' . time() . '.' . $request->avatar->getClientOriginalExtension();
        move_uploaded_file($request->file('avatar'), $path . $filename);

        $user = Auth::user();
        $user->imagen = $filename;
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $filename,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }

    function remove_profile()
    {

        $user = Auth::user();
        $user->imagen = 'avatar-1.png';
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $user->imagen,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }

    public function datos($usuario)
    {
        $datos = [
            'id' => $usuario->id,
            'nombre' => $usuario->persona->nombres . " " . $usuario->persona->apellidos,
            'cedula' => $usuario->persona->documento,
            'email' => $usuario->email,
            'rol' => $usuario->rol->nombre,
            'rol_id' => $usuario->rol_id,
            'telefono' => $usuario->persona->telefono,
            'estado' => $usuario->estado,
            'municipio' => $usuario->persona->city->state_id,
            'imagen' => $usuario->imagen,
            'persona' => $usuario->persona,
        ];
        return $datos;
    }
}
