<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    //
    protected $fillable = [
        'nombre','nivel','intensidad', 'cupos', 'fecha_inicio_inscripciones', 'fecha_fin_inscripciones',
        'fecha_inicio_curso','fecha_fin_curso','estado','supervisor','entrenador_id','ciudad','dias',
    ];

    public function entrenador()
    {
        return $this->belongsTo(Persona::class,'entrenador_id');
    }

    public function matriculados()
    {
        return $this->hasMany(Inscripcion::class);
    }
}
