<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    //
    protected $fillable = [
        'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        'curso_id','persona_id','asistencia_validada','fecha_reporte_ministerio','descargable'
    ];

    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }

    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    public function certificado()
    {
        return $this->hasOne(Certificados::class);
    }

}
